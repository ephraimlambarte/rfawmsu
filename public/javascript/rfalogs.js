$(document).ready(function(){
  loader("#loadrfalogshere");
  var thisdata = new FormData();
  var rowid = "";
  var parthreeid = "";
  var controlno = "";
  var date = "";
  var status = "";
  var edithtml = "";
  var viewhtml = "";
  var rfahtml = "";
  var mypage = 0;
  /*getAjaxCallback('../controller/rfalogs/loadrfalogs2.php', 'POST',thisdata, '#loadrfalogshere', function(data){
    bindElements();
  });*/
  getAjaxValueAsync('../controller/rfalogs/gettotalrows.php', 'POST', thisdata, function(totalrows){
    if(totalrows==0){
      $('#loadrfalogshere').html("<h4>No RFAs yet.</h4>");
      return;
    }
    var visiblepages;
    var totalpages;
    if(totalrows<5){
      visiblepages = 1;
      totalpages = 1;
    }else{
      if(totalrows%5==0){
        totalpages  = totalrows/5;      
      }else{
        totalpages = ((totalrows - (totalrows%5))/5)+1;
      }
      if(totalpages>10){
        visiblepages = 10;
      }else{
        visiblepages = totalpages
      }
    }
    var userdata = new FormData();
    $('#pagination').twbsPagination({
      totalPages:totalpages,
      visiblePages:visiblepages,
      onPageClick: function (event, page) {
          mypage =page - 1;
          if(mypage > 0){
            mypage = mypage*5;
          }
          userdata.append('page', mypage);
          getAjaxCallback('../controller/rfalogs/loadrfalogs2.php', 'POST',userdata, '#loadrfalogshere', function(data){
            bindElements();
          });
      }
    });
  });
  var myresult = loadModalNotifAjax('../controller/rfalogs/loadmodalnotif.php', 'POST',thisdata);
  if(myresult!="No modal"){
    rowid = myresult.split(",")[0];
    if(myresult.split(",")[1]=="part2"){
      var mydata = new FormData();
      mydata.append("rfaid", rowid);
      $("#rfaparttwomodal").modal("show");
      getAjaxDropDown('../controller/rfalogs/loadrfapart2modal.php', 'POST',mydata, '#partTwoContainer', function(){
        $("#preventiveActionText").dropdown();
        $("#implementedByText").dropdown();
      });
    }else if (myresult.split(",")[1]=="part3") {
      $("#rfapartthreemodal").modal("show");
      var mydata = new FormData();
      mydata.append("rfaid", rowid);
      getAjax('../controller/rfalogs/loadrfapart3modal.php', 'POST',mydata, '#tableRfaPartThree');
    }else{
      $("#rfapartfourmodal").modal("show");
      var mydata = new FormData();
      mydata.append("rfaid", rowid);
      getAjax('../controller/rfalogs/loadrfapartfourmodal.php', 'POST',mydata, '#partFourContainer');
    }
  }
  $("#loadrfalogshere").on('click', '.viewRFA',function(){
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    var thisid = $(this).prop("id");
    var id = thisid.split("-")[1];
    var mydata= new FormData();
    mydata.append("id", id);
    ajaxOpenWindow('../controller/rfalogs/instantiaterfaid.php', 'POST', mydata);
  });
  $('#loadrfalogshere').on('click', '.editRFAAdmin', function(){
    var thisid = $(this).prop("id");
    var id = thisid.split("-")[1];
    rowid =id;
    $("#rfapartfourmodal").modal('setting', 'closable', true).modal("show");
  });
  $('#loadrfalogshere').on('click', '.editRFAAuditor', function(){
    var thisid = $(this).prop("id");
    var id = thisid.split("-")[1];
    rowid =id;
    $("#rfapartthreemodal").modal('setting', 'closable', true).modal("show");
  });
  $('#loadrfalogshere').on('click', '.editRFAAuditee', function(){
    var thisid = $(this).prop("id");
    var id = thisid.split("-")[1];
    rowid =id;
    $("#rfaparttwomodal").modal('setting', 'closable', true).modal("show");
  });
  $("#rfaparttwomodal").modal({
    onVisible:function(){
      loader("#partTwoContainer");
      var mydata = new FormData();
      mydata.append("rfaid", rowid);
      getAjaxDropDown('../controller/rfalogs/loadrfapart2modal.php', 'POST',mydata, '#partTwoContainer', function(){
        $("#preventiveActionText").dropdown();
        $("#implementedByText").dropdown();
      });
      getAjaxValueAsync('../controller/rfalogs/checkifrfapart2processed.php', 'POST',mydata,function(data){  
        if(data=="Processed"){
          $("#processRFAPartTwo").html("Update<i class='checkmark icon'></i>");
        }else{
          $("#processRFAPartTwo").html("Process<i class='checkmark icon'></i>");
        }
      });
    },
    onHide:function(){
      rowid = "";
    }
  });
  $("#saveRFAPartTwo").on('click', function(){
    /* useless code */
    if($("#editable").val()=="false"){
      $("#notification").html("<h3>This RFA not yet been processed for Action Plan! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal('show');
      return;
    }
    if($("#rootCausesTextArea").prop('disabled')){
      $("#notification").html("<h3>This RFA have already been processed at Verification! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal('show');
      return;
    }
    var rootcauses = $("#rootCausesTextArea").val();
    var immediateaction = $("#immedateActionTextArea").val();
    var implementedby =  $("#implementedByText").dropdown("get value");
    var date1 = $("#implementedByDate").val();
    var preventiveaction = $("#preventiveActionTextArea").val();
    var implementedby2 = $("#preventiveActionText").dropdown("get value");
    var date2 = $("#preventiveActionDate").val();


    if((rootcauses=="")||(immediateaction=="")||(implementedby=="")||(date1=="")||(preventiveaction=="")||(implementedby2=="")||(date2=="")){
      $("#notification").html("<h3>Please fill up all the necessary information! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal('show');
      return;
    }
    var mydata = new FormData();
    mydata.append("rootcauses", rootcauses);
    mydata.append("immediateaction", immediateaction);
    mydata.append("implementedby", implementedby)
    mydata.append("date1", date1);
    mydata.append("preventiveaction", preventiveaction);
    mydata.append("implementedby2", implementedby2);
    mydata.append("date2", date2);
    mydata.append("rfaid", rowid);
    loader("#notification");
    $("#notificationmodal").modal('show');
    getAjax('../controller/rfalogs/updaterfapart2.php', 'POST',mydata, '#notification');
    $("#rootCausesTextArea").val("");
    $("#immedateActionTextArea").val("");
    $("#preventiveActionTextArea").val("");
  });
  $("#processRFAPartTwo").on('click', function(){
    if($("#processRFAPartTwo").text() == "Update"){
      if($("#editable").val()=="false"){
        $("#notification").html("<h3>This RFA not yet been processed for Action Plan! <i class='warning sign icon'></i></h3>");
        $("#notificationmodal").modal('show');
        return;
      }
      if($("#rootCausesTextArea").prop('disabled')){
        $("#notification").html("<h3>This RFA have already been processed at Verification! <i class='warning sign icon'></i></h3>");
        $("#notificationmodal").modal('show');
        return;
      }
      var rootcauses = $("#rootCausesTextArea").val();
      var immediateaction = $("#immedateActionTextArea").val();
      var implementedby =  $("#implementedByText").dropdown("get value");
      var date1 = $("#implementedByDate").val();
      var preventiveaction = $("#preventiveActionTextArea").val();
      var implementedby2 = $("#preventiveActionText").dropdown("get value");
      var date2 = $("#preventiveActionDate").val();
  
  
      if((rootcauses=="")||(immediateaction=="")||(implementedby=="")||(date1=="")||(preventiveaction=="")||(implementedby2=="")||(date2=="")){
        $("#notification").html("<h3>Please fill up all the necessary information! <i class='warning sign icon'></i></h3>");
        $("#notificationmodal").modal('show');
        return;
      }
      var mydata = new FormData();
      mydata.append("rootcauses", rootcauses);
      mydata.append("immediateaction", immediateaction);
      mydata.append("implementedby", implementedby)
      mydata.append("date1", date1);
      mydata.append("preventiveaction", preventiveaction);
      mydata.append("implementedby2", implementedby2);
      mydata.append("date2", date2);
      mydata.append("rfaid", rowid);
      loader("#notification");
      $("#notificationmodal").modal('show');
      getAjax('../controller/rfalogs/updaterfapart2.php', 'POST',mydata, '#notification');
      $("#rootCausesTextArea").val("");
      $("#immedateActionTextArea").val("");
      $("#preventiveActionTextArea").val("");
    }else{
      if($("#editable").val()=="true"){
        //console.log("Hello");
        $("#notification").html("<h3>This RFA have already been processed for Action Plan. Maybee you can still update it! <i class='warning sign icon'></i></h3>");
        $("#notificationmodal").modal('show');
        return;
      }
      var rootcauses = $("#rootCausesTextArea").val();
      var immediateaction = $("#immedateActionTextArea").val();
      var implementedby =  $("#implementedByText").dropdown("get value");
      var date1 = $("#implementedByDate").val();
      var preventiveaction = $("#preventiveActionTextArea").val();
      var implementedby2 = $("#preventiveActionText").dropdown("get value");
      var date2 = $("#preventiveActionDate").val();
      if((rootcauses=="")||(immediateaction=="")||(implementedby=="")||(date1=="")||(preventiveaction=="")||(implementedby2=="")||(date2=="")){
        $("#notification").html("<h3>Please fill up all the necessary information! <i class='warning sign icon'></i></h3>");
        $("#notificationmodal").modal('show');
        return;
      }
      var mydata = new FormData();
      mydata.append("rootcauses", rootcauses);
      mydata.append("immediateaction", immediateaction);
      mydata.append("implementedby", implementedby)
      mydata.append("date1", date1);
      mydata.append("preventiveaction", preventiveaction);
      mydata.append("implementedby2", implementedby2);
      mydata.append("date2", date2);
      mydata.append("rfaid", rowid);
      loader("#notification");
      $("#notificationmodal").modal('show');
      getAjax('../controller/rfalogs/insertrfapart3.php', 'POST',mydata, '#notification');
      $("#rootCausesTextArea").val("");
      $("#immedateActionTextArea").val("");
      $("#preventiveActionTextArea").val("");
    }
  });
  $("#rfapartthreemodal").modal({
    onVisible:function(){
      loader("#tableRfaPartThree");
      var mydata = new FormData();
      mydata.append("rfaid", rowid);
      $("#notsatisfactory").prop("checked", true);
      getAjax('../controller/rfalogs/loadrfapart3modal.php', 'POST',mydata, '#tableRfaPartThree');
      getAjaxValueAsync('../controller/rfalogs/getverifiedbypart3.php', 'POST',mydata,function(data){  
        $("#verifiedByRFAPartThree").val(data);
      });
      getAjaxValueAsync('../controller/rfalogs/getauditorsdatalist.php', 'POST',mydata,function(data){  
        $("#auditors").html(data);
      });
      getAjaxValueAsync('../controller/rfalogs/checkifrfapart3processed.php', 'POST',mydata,function(data){  
        if(data=="Processed"){
          $("#processRFAPartThree").prop("disabled", true);
          $("#saveRFAPartThree").prop("disabled", true);
        }else{
          $("#processRFAPartThree").prop("disabled", false);
          $("#saveRFAPartThree").prop("disabled", false);
        }
      });
    },
    onHide:function(){
      parthreeid = "";
      rowid = "";
    }
  });
  $("#barGraphModal").modal({
    onVisible:function(){
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var jsonData = $.ajax({
        url: "../controller/rfalogs/getDataLineGraph.php",
        dataType: "json",
        async: false
        }).responseText;
        var obj = JSON.parse(jsonData);

        var data = google.visualization.arrayToDataTable([
          ['Month', 'Open','Closed','Total'],
          ['January',  obj.janclose, obj.janopen, obj.January],
          ['February', obj.febclose, obj.febopen,  obj.February ],
          ['March',  obj.marclose, obj.maropen, obj.March],
          ['April',  obj.aprclose, obj.apropen, obj.April],
          ['May',  obj.mayclose, obj.mayopen, obj.May],
          ['June',  obj.junclose, obj.junopen, obj.June],
          ['July',  obj.julclose, obj.julopen, obj.July],
          ['August',  obj.augclose, obj.augopen, obj.August],
          ['September',  obj.sepclose, obj.sepopen, obj.September],
          ['October',  obj.octclose, obj.octopen, obj.October],
          ['November',  obj.novclose, obj.novopen, obj.November],
          ['December',  obj.decclose, obj.decopen, obj.December]
        ]);
        
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2,
                         { calc: "stringify",
                         sourceColumn: 2,
                         type: "string",
                         role: "annotation" },
                        3,
                        { calc: "stringify",
                        sourceColumn: 3,
                        type: "string",
                        role: "annotation" }]);
        /*var mydata = new google.visualization.DataTable(jsonData);
        console.log(mydata);*/
        var options = {
          title: 'RFA Logs',
          curveType: 'function',
          legend: { position: 'bottom' }
        };
        var chart = new google.visualization.ColumnChart(document.getElementById('curve_chart'));
        var chart_div = document.getElementById('curve_chart');
        // Wait for the chart to finish drawing before calling the getImageURI() method.
        google.visualization.events.addListener(chart, 'ready', function () {
          chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
          console.log(chart_div.innerHTML);
        });
  
        chart.draw(view, options);
  
      }
    }
  });
  $("#tableRfaPartThree").on('click', 'tr', function(){
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    parthreeid = $(this).prop("id").split("-")[1];
    $("#verifiedByDate").val(product[0]);
    $("#detailesRFAParthThreeText").val(product[1]);
    if(product[2]=="*"){
      $("#satisfactory").prop("checked", true);
    }else{
      $("#notsatisfactory").prop("checked", true);
    }
    $("#verifiedByRFAPartThree").val(product[4]);
  });
  $("#saveRFAPartThree").on('click', function(){
    if(parthreeid==""){
      $("#notification").html("<h3>Please select a row to update! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    var details = $("#detailesRFAParthThreeText").val();
    var date = $("#verifiedByDate").val();
    var satisfactory = getSatisfactory();
    var verifiedby = $("#verifiedByRFAPartThree").val();
    if(details==""||date == "" ||verifiedby==""){
      $("#notification").html("<h3>Please fill up all the necessary fields! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }

    var mydata = new FormData();
    mydata.append("details", details);
    mydata.append("date", date);
    mydata.append("satisfactory", satisfactory);
    mydata.append("verifiedby", verifiedby);
    mydata.append("partoneid", rowid);
    mydata.append("partthreeid", parthreeid);
    loader("#notification")
    $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    var result = getValueNotAsync('../controller/rfalogs/updaterfapart3.php', 'POST',mydata);
    if(result=="Something went wrong. Please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else if (result=="This rfa have already been processed in part 4 and cannot be editted!") {
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>This rfa have already been processed in Close Out and cannot be editted! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else if (result=="This rfa is not assigned to you for processing!") {
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully saved! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      $("#parththree-"+parthreeid).html(result);
      $("#detailesRFAParthThreeText").val("");
      $("#verifiedByRFAPartThree").val("");
    }
  
  });
  $("#processRFAPartThree").on('click', function(){

    var details = $("#detailesRFAParthThreeText").val();
    var date = $("#verifiedByDate").val();
    var satisfactory = getSatisfactory();
    var verifiedby = $("#verifiedByRFAPartThree").val();
    if(details==""||date == "" ||verifiedby==""){
      $("#notification").html("<h3>Please fill up all the necessary fields! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    var mydata = new FormData();
    mydata.append("details", details);
    mydata.append("date", date);
    mydata.append("satisfactory", satisfactory);
    mydata.append("verifiedby", verifiedby);
    mydata.append("partoneid", rowid);
    mydata.append("partthreeid", parthreeid);
    loader("#notification")
    $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    var result = getValueNotAsync('../controller/rfalogs/insertrfapartthree.php', 'POST',mydata);
    if(result=="Something went wrong. Please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else if (result=="This rfa have already had 5 max reviews for part 3!") {
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>This rfa have already had 5 max reviews for Verification! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else if (result == "This rfa is not assigned to you for processing!") {
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else if(result == "This RFA is not yet processed for Action Plan!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else if (result == "This have already been approved in for verification!") {
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else if (result == "This rfa have already exceeded due date!") {
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully saved! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      $("#tableRfaPartThree tbody").append(result);
      $("#detailesRFAParthThreeText").val("");
      $("#verifiedByRFAPartThree").val("");
    }
  });
  $("#tableRfaPartThree").on('click', 'table div', function(){
    parthreeid = $(this).prop("id").split("-")[1];
    $("#yesnomodal").modal({allowMultiple:true}).modal("show");
  });
  $("#yesButton").on('click', function(){
    var mydata = new FormData();
    mydata.append("partthreeid", parthreeid);
    mydata.append("partoneid", rowid);
    loader("#notification");
    $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    var result = getValueNotAsync('../controller/rfalogs/deleterfapartthree.php', 'POST',mydata);
    if(result=="Something went wrong. Please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else if (result=="This rfa have already been processed in part 4 and cannot be deleted!") {
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else if (result=="This rfa is not assigned to you for processing!") {
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully deleted! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      $("#parththree-"+parthreeid).remove();
    }
  });
  $("#rfapartfourmodal").modal({
    onVisible: function(){
      loader("#partFourContainer");
      var mydata = new FormData();
      mydata.append("rfaid", rowid);
      getAjax('../controller/rfalogs/loadrfapartfourmodal.php', 'POST',mydata, '#partFourContainer');
      getAjaxValueAsync('../controller/rfalogs/checkifrfapart4processed.php', 'POST',mydata,function(data){  
        if(data=="Processed"){
          $("#processRFAPartFour").html("Update<i class='checkmark icon'></i>");
        }else{
          $("#processRFAPartFour").html("Process<i class='checkmark icon'></i>");
        }
      });
    },
    onHide: function(){
      rowid = "";
    }
  });
  $("#saveRFAPartFour").on('click', function(){
    /* useless code */
    var remarks1 = $("#remarksOnePartFour").val();
    var satisfactory = getSatisfactoryPartFour();
    var remarks2 =  $("#remarksTwoPartFour").val();
    var validatedby = $("#validatedByPartFour").val();
    var verifiedby = $("#verifiedbyPartFour").val();
    var date1 = $("#verifiedByDatePartFour").val();
    var date2 = $("#validatedByDatePartFour").val();

    if($("#rfapartfoureditable").val()!="editable"){
      $("#notification").html("<h3>Please process this rfa first before editting it! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal('show');
      return;
    }
    if(remarks1==""||remarks2==""||validatedby==""||verifiedby==""||date1==""||date2==""){
      $("#notification").html("<h3>Please fill up all the necessary information! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal('show');
      return;
    }
    var mydata = new FormData();
    mydata.append("satisfactory", satisfactory);
    mydata.append("remarks1", remarks1);
    mydata.append("remarks2", remarks2);
    mydata.append("validatedby", validatedby);
    mydata.append("verifiedby", verifiedby);
    mydata.append("date1", date1);
    mydata.append("date2", date2);
    mydata.append("rowid", rowid);
    loader("#notification");
    $("#notificationmodal").modal('show');
    getAjax('../controller/rfalogs/updaterfapartfour.php', 'POST',mydata, '#notification');
    $("#remarksOnePartFour").val("");
    $("#remarksTwoPartFour").val("");
    $("#validatedByPartFour").val("");
    $("#verifiedbyPartFour").val("");
  });
  $("#processRFAPartFour").on('click', function(){
    if($("#processRFAPartFour").text() == "Process"){
       //alert("Hello");
      var remarks1 = $("#remarksOnePartFour").val();
      var satisfactory = getSatisfactoryPartFour();
      var remarks2 =  $("#remarksTwoPartFour").val();
      var validatedby = $("#validatedByPartFour").val();
      var verifiedby = $("#verifiedbyPartFour").val();
      var date1 = $("#verifiedByDatePartFour").val();
      var date2 = $("#validatedByDatePartFour").val();
      //alert($("#remarksOnePartFour").val())
      //alert(remarks1+"-"+satisfactory+"-"+remarks2+"-"+validatedby+"-"+verifiedby+"-"+date1+"-"+date2);
      if($("#rfapartfoureditable").val()=="editable"){
        $("#notificationmodal").modal({allowMultiple:true}).modal("close");
        $("#notification").html("<h3>This rfa have already been processed, you can still edit it! <i class='warning sign icon'></i></h3>");
        $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        return;
      }
      if(remarks1==""||remarks2==""||validatedby==""||verifiedby==""||date1==""||date2==""){
        $("#notificationmodal").modal({allowMultiple:true}).modal("close");
        $("#notification").html("<h3>Please fill up all the necessary information! <i class='warning sign icon'></i></h3>");
        $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        return;
      }
      var mydata = new FormData();
      mydata.append("satisfactory", satisfactory);
      mydata.append("remarks1", remarks1);
      mydata.append("remarks2", remarks2);
      mydata.append("validatedby", validatedby);
      mydata.append("verifiedby", verifiedby);
      mydata.append("date1", date1);
      mydata.append("date2", date2);
      mydata.append("rowid", rowid);
      loader("#notification");
      $("#notificationmodal").modal('show');
      getAjax('../controller/rfalogs/insertrfapartfour.php', 'POST',mydata, '#notification');
      $("#remarksOnePartFour").val("");
      $("#remarksTwoPartFour").val("");
      $("#validatedByPartFour").val("");
      $("#verifiedbyPartFour").val("");
    }else{
      var remarks1 = $("#remarksOnePartFour").val();
      var satisfactory = getSatisfactoryPartFour();
      var remarks2 =  $("#remarksTwoPartFour").val();
      var validatedby = $("#validatedByPartFour").val();
      var verifiedby = $("#verifiedbyPartFour").val();
      var date1 = $("#verifiedByDatePartFour").val();
      var date2 = $("#validatedByDatePartFour").val();
  
      if($("#rfapartfoureditable").val()!="editable"){
        $("#notification").html("<h3>Please process this rfa first before editting it! <i class='warning sign icon'></i></h3>");
        $("#notificationmodal").modal('show');
        return;
      }
      if(remarks1==""||remarks2==""||validatedby==""||verifiedby==""||date1==""||date2==""){
        $("#notification").html("<h3>Please fill up all the necessary information! <i class='warning sign icon'></i></h3>");
        $("#notificationmodal").modal('show');
        return;
      }
      var mydata = new FormData();
      mydata.append("satisfactory", satisfactory);
      mydata.append("remarks1", remarks1);
      mydata.append("remarks2", remarks2);
      mydata.append("validatedby", validatedby);
      mydata.append("verifiedby", verifiedby);
      mydata.append("date1", date1);
      mydata.append("date2", date2);
      mydata.append("rowid", rowid);
      loader("#notification");
      $("#notificationmodal").modal('show');
      getAjax('../controller/rfalogs/updaterfapartfour.php', 'POST',mydata, '#notification');
      $("#remarksOnePartFour").val("");
      $("#remarksTwoPartFour").val("");
      $("#validatedByPartFour").val("");
      $("#verifiedbyPartFour").val("");
    }
   
  });
  $("#notificationusermodalContainer").on('click', '.unread',function(){
    var id = $(this).prop('id');
    var mydata = new FormData();
    id = id.split("-")[1];
    mydata.append("id", id);
    getAjaxNotif('../controller/notification/initializenotificationid.php', 'POST', mydata, "#notificationusermodalContainerss");
  });
  $("#searchRfaLog").on('keyup', function(){
    loader("#loadrfalogshere");
    var mydata = new FormData();
    mydata.append("search", $("#searchRfaLog").val());
    getAjax('../controller/rfalogs/searchRfaLogs.php', 'POST',mydata, '#loadrfalogshere');
  });
  $("#statisticSummaryButton").on('click', function(){
    $("#barGraphModal").modal("show");
  });
  $("#loadrfalogshere").on('click', '.editRfaPartOne', function(){
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    controlno = product[0];
    date = product[1];
    status = product[7];
    edithtml = product[8];
    viewhtml = product[9];
    rfahtml = product[10];
    rowid = $(this).prop("id").split("-")[1];
    $("#editRfaPartOneModal").modal("show");
    var mydata = new FormData();
    mydata.append("rfaid", rowid)
    getAjaxDropDown('../controller/rfalogs/loadrfapartone.php', 'POST',mydata, '#rfaPartOneDetails', function(){
        $("#rfa_cu").dropdown();
        $("#rfa_pu").dropdown();
        $("#rfa_su").dropdown();
        $("#rfa_conformedby").dropdown();
    });
  });
  $("#rfaPartOneDetails").on('change', '#rfa_cu', function(){
    $("#rfa_pu").dropdown('clear');
    $("#rfa_su").dropdown('clear');
    var mydata = new FormData();
    mydata.append("cuid", $("#rfa_cu").dropdown("get value"));
    getAjax('../controller/rfapartone/loadpudropdown.php', 'POST',mydata, '#loadrfa_pu');
  });
  $("#rfaPartOneDetails").on('change', '#rfa_pu', function(){
    //$("#rfa_pu").dropdown('clear');
    $("#rfa_su").dropdown('clear');
    var mydata = new FormData();
    mydata.append("cuid", $("#rfa_pu").dropdown("get value"));
    getAjax('../controller/rfapartone/loadpudropdown.php', 'POST',mydata, '#loadrfa_su');
  });
  $("#saveRfaPartOneButton").on('click', function(){
    if($("#rfaPartOneDetails").html()=="<h4>This RFA is already on process and cannot be editted! <i class='warning sign icon'></i></h4>"){
      $("#notificationmodal").modal("show");
      $("#notification").html("<h4>This rfa cannot be editted anymore because it is on process! <i class='warning sign icon'></i></h4>");
    }else{
        var date1 = new Date( $('#rfa_dateissued').val());
        var date2 = new Date($('#rfa_duedate').val());
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        if(diffDays<30){
          $("#notification").html("<h3>Due Date must atleast be 30 days! <i class='warning sign icon'></i></h3>");
          $("#notificationmodal").modal("show");
          return;
        }
        var coreunit = $("#rfa_cu").dropdown("get value");
        var processunit = $("#rfa_pu").dropdown("get value");
        var subunit  = $("#rfa_su").dropdown("get value");
        var actiontype = $("input[name='Type_Of_Action']:checked").val();
        var requestsource = $("input[name='requestSource']:checked").val();
        var nonconformity = $("#rfa_nonconformity").val();
        var description = $("#rfa_desc").val();
        var conformedby = $("#rfa_conformedby").dropdown("get value");
        var risklevel = $("input[name='riskLevel']:checked").val();
        var requestsource2 = "";
        var rfaDateDue = $('#rfa_duedate').val();
        if(requestsource=="specify"){
          requestsource2 = requestsource;
          requestsource = $("#specifyText").val();
          if (requestsource == ""){
            $("#notification").html("<h4>Request Source specified must not be empty!</h4>");
            $("#notificationmodal").modal({allowMultiple:true}).modal("show");
            return;
          }
        }
        if(coreunit == ""||processunit == ""|| subunit == ""||actiontype == ""||requestsource == ""||
          nonconformity == "" || description == "" || description == "" || conformedby == ""
        || risklevel == ""){
          $("#notification").html("<h4>Please provide all the necessary details!</h4>");
          $("#notificationmodal").modal({allowMultiple:true}).modal("show");
          return;
        }
        var mydata = new FormData();
        mydata.append("coreunit", coreunit);
        mydata.append("processunit", processunit);
        mydata.append("subunit", subunit);
        mydata.append("actiontype", actiontype);
        mydata.append("requestsource", requestsource);
        mydata.append("nonconformity", nonconformity);
        mydata.append("description", description);
        mydata.append("conformedby", conformedby);
        mydata.append("risklevel", risklevel);
        mydata.append("requestsource2", requestsource2);
        mydata.append("rfaid", rowid);
        mydata.append("rfaDateDue",rfaDateDue);
        loader("#notification");
        $("#notificationmodal").modal("show");
        var result = getValueNotAsync('../controller/rfalogs/updaterfapartone.php', 'POST',mydata);
        if(result == "error"){
          $("#notification").html("<h4>Something went wrong! please try again later! <i class='warning sign icon'></i></h4>");
        }else{
          var html = "<td>"+controlno+"</td>";
          html += "<td>"+date+"</td>";
          html +="<td>"+requestsource+"</td>";
          html += "<td>"+nonconformity+"</td>";
          html += "<td></td>";
          html += "<td>"+actiontype+"</td>";
          html += "<td>"+risklevel+"</td>";
          html += "<td>"+status+"</td>";
          html += "<td>"+edithtml+"</td>";
          html += "<td>"+viewhtml+"</td>";
          html += "<td>"+rfahtml+"</td>";
          $("#"+rowid).html(html);
          $("#notification").html("<h4>Successfully updated! <i class='info circle icon'></i></h4>");
          $("#rfa_nonconformity").val("");
          $("#rfa_desc").val("");
          $("#specifyText").val("");
        }
    }
  });
  $("#viewRFALogsButtonPDF").on('click', function(){
    $("#viewPdfModal").modal("show");
  });
  $("#submitPdfModal").on('click', function(){
    var mydata  =  new FormData();
    if($("#startDate").val()== "" || $("#endDate").val() == ""){
      alert("Please select an end date and start date!");
      return;
    }
    mydata.append("startDate", $("#startDate").val());
    mydata.append("endDate", $("#endDate").val());
    mydata.append("filter", $("#statusFilter").val());
    ajaxOpenWindowRfaLofgs('../controller/rfalogs/instantiaterfalogmodal.php', 'POST', mydata);
  });
  $("#viewPdfModal").modal({
    onVisible: function(){
      
      $("#startDate").val((new Date()).getFullYear()+"-"+"01-01");
      $("#endDate").val((new Date()).getFullYear()+"-"+"12-01");
    },
    onHide: function(){
      
    }
  });
  $("#startDate").on('keypress', function(){
    var startdate = $("#startDate").val();
    year = startdate.split("-")[0];
    month = startdate.split("-")[1];
    day =  startdate.split("-")[2];
    if(year.length>4){
      $("#startDate").val(year.substr(0,4)+"-"+month+"-"+day)
    }
  });
  $("#endDate").on('keypress', function(){
    var startdate = $("#endDate").val();
    year = startdate.split("-")[0];
    month = startdate.split("-")[1];
    day =  startdate.split("-")[2];
    if(year.length>4){
      $("#endDate").val(year.substr(0,4)+"-"+month+"-"+day)
    }
  });
});
function getSatisfactory(){
  if($("#satisfactory").prop("checked")){
    return "1";
  }else{
    return "0";
  }
}
function getSatisfactoryPartFour(){
  if($("#satisfactoryPartFour").prop("checked")==true){
    return 1;
  }else{
    return 0;
  }
}
var bindElements = function() {
  $('.button')
  .popup({
    on: 'hover'
  });
};
function float2int (value) {
  return value | 0;
}