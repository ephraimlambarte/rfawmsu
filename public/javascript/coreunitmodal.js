$(document).ready(function(){
  var rowid = "";
  var coreunitname = "";
  tableLoader("#coreunitListTable");
  var thisdata =  new FormData();
  getAjax('../controller/coreunitmodal/loadcoreunitmodal.php', 'POST', thisdata, "#coreunitListTable");
  $("#coreunitListTable").on('click', 'tr', function(){
    var thisid = $(this).attr("id");
    rowid = thisid.split("-")[1];
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    $("tr").removeClass("clickedtr");
    $(this).addClass("clickedtr");
    $("#cu_name").val(product[0]);
  });
  $("#clearCoreunitModalButton").on('click', function(){
    rowid = "";
    $("tr").removeClass("clickedtr");
    $("#cu_name").val("");
  });
  $("#addCoreunitModalButton").on('click', function(){
    if($("#cu_name").val() == ""){
      $("#notification").html("<h3>Please fill up the necessary fields!</h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    var mydata =  new FormData();
    mydata.append("cuname", $("#cu_name").val());
    var result = getValueNotAsync('../controller/coreunitmodal/insertcoreunit.php', 'POST', mydata)
    if (result == "Something went wrong please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#coreunitListTable").append(result);
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully added! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }
    rowid = "";
    $("#"+rowid).removeClass("clickedtr");
  });
  $("#saveCoreunitModalButton").on('click', function(){
    if(rowid ==""){
      //alert("Please select a row to update!");
      $("#notification").html("<h3>Please select a row to update! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    if($("#cu_name").val() == ""){
      //alert("Please fill up all the necessary field!");
      $("#notification").html("<h3>Please fill up the necessary fields!</h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    var mydata = new FormData();
    mydata.append("coreunitname", $("#cu_name").val());
    mydata.append("coreid", rowid);
    loader("#notification");
    $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    var result = getValueNotAsync('../controller/coreunitmodal/updatecoreunit.php', 'POST', mydata);
    if (result == "Something went wrong please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+"</h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#coreunit-"+rowid).html(result);
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully saved!</h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }
    rowid = "";
    $("#coreunit-"+rowid).removeClass("clickedtr");
  });
  $("#coreunitListTable").on('click', "div", function(){
    //alert("Hello");
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    coreunitname = product[0];
    rowid = $(this).attr('id');
    $("#yesnomodalCoreunit").modal({allowMultiple:true}).modal("close");
    $("#questions").html("Are you sure you want to delete coreunit "+coreunitname+"? <i class='warning sign icon'></i>");
    $("#yesnomodalCoreunit").modal({allowMultiple:true}).modal("show");
  });
  $("#yesCoreUnitButton").on('click', function(){
    var mydata = new FormData();
    mydata.append('cuid', rowid);
    var result = getValueNotAsync('../controller/coreunitmodal/deletecoreunit.php', 'POST', mydata);
    if (result == "Something went wrong. please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#coreunit-"+rowid).remove();
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully deleted! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }
    $("#coreunit-"+rowid).removeClass('clickedtr');
    rowid = "";
  });
});
