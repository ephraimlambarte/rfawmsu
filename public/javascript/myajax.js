function getAjax(url, type, formdata,element){
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
			     $(element).html(data);
			}
	}); 
}
function getAjaxCallback(url, type, formdata,element, callback){
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
					 $(element).html(data);
					 callback(data);
			}
	}); 
}
function getAjaxValueAsync(url, type, formdata,callback){
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
			     callback(data);
			}
	}); 
}
function getAjaxDropDown(url, type, formdata,element, callback){
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
			     $(element).html(data);
           callback(data);
			}
	});
}
function getAjaxNotif(url, type, formdata,element){
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,
      async:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
			     if(data=="succcess!"){
             window.location.href = "../views/rfalogs.php";
           }
			}
	});
}
function loadModalNotifAjax(url, type, formdata,element){
  var result;
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,
      async:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
        result=data;
			}

	});
    return result;
}
function getAjaxValue(url, type, formdata,element){
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
			     $(element).val(data);
			}
	});
}
function ajaxOpenWindow(url, type, formdata){
  var hello;
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
      async:false,
			success: function(data)   // A function to be called if request succeeds
			{
			    if(data!==undefined){
            hello = data;
          }
			}
	});
  window.open("../views/rfalogpdf.php");
}
function ajaxOpenWindowRfaLofgs(url, type, formdata){
  var hello;
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
      async:false,
			success: function(data)   // A function to be called if request succeeds
			{
			    if(data!==undefined){
            hello = data;
          }
			}
	});
  window.open("../views/rfalogspdfshow.php");
}
function AjaxNotAsync(url, type, formdata,element){
  var mydata;
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
      async:false,
			success: function(data)   // A function to be called if request succeeds
			{
			    mydata= data;
			}
	});
  return mydata;
}
function getValueNotAsync(url, type, formdata){
  var mydata;
  $.ajax({
      url: url, // Url to which the request is send
      type: type,             // Type of request to be send, called as method
      data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
      contentType: false,       // The content type used when sending data to the server.
      cache: false,             // To unable request pages to be cached
      processData:false,        // To send DOMDocument or non processed data file it is set to false
      async:false,
      success: function(data)   // A function to be called if request succeeds
      {
          mydata= data;
      }
  });
  return mydata;
}
function appendAjax(url, type, formdata,element){
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
			     $(element).append(data);
			}
	});
}
function loginAjax(url, type, formdata,element){
  $.ajax({
			url: url, // Url to which the request is send
			type: type,             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
			     if(data.split(".")[0] == "login"){
             if(data.split(".")[1]=="Admin"){
             window.location.href = "./views/rfalogs.php";
           }else{
             window.location.href = "./views/rfalogs.php";
           }
           }else{
             alert(data);
           }
			}
	});
}
function logout(){
  var formdata = new FormData();
  $.ajax({
			url: '../controller/logout.php', // Url to which the request is send
			type: 'POST',             // Type of request to be send, called as method
			data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
			     if(data == "logout"){
             window.location.href = "../index.php";
           }else{
             alert(data);
           }
			}
	});
}
