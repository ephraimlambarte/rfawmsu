$(document).ready(function(){
  var thisdata = new FormData();
  var rowid = "";
  tableLoader("#subunitListTable", "3");
  getAjax('../controller/subunitmodal/loadsubunit.php', 'POST', thisdata, "#subunitListTable");
  getAjax('../controller/subunitmodal/loadprocessunitdropbox.php', 'POST', thisdata, "#loadpu_dp");
  $("#su_pu").dropdown();
  $("#clearSubUnitButton").on('click', function(){
    rowid = "";
    $("tr").removeClass("clickedtr");
    $('#su_pu').dropdown('restore defaults');
    $("#su_name").val("");
  });
  $("#subunitListTable").on('click', 'tr', function(){
    var thisid = $(this).attr("id");
    rowid = thisid.split("-")[1];
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    $("tr").removeClass("clickedtr");
    $("#subunit-"+rowid).addClass("clickedtr");
    $("#su_name").val(product[0]);
    $("#su_pu").dropdown("set selected", product[1]);
  });
  $("#addSubUnitButton").on('click', function(){
    if($("#su_name").val()== "" || $("#su_pu").dropdown("get value")==""){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Please provide all the necessary details! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    var mydata = new FormData();
    mydata.append("suname", $("#su_name").val());
    mydata.append("puname", $("#su_pu").dropdown("get value"));
    loader("#notification");
    var result = getValueNotAsync('../controller/subunitmodal/insertsubunit.php', 'POST', mydata);
    if (result == "Something went wrong. Please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Something went wrong. Please try again later! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully added! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      $("#subunitListTable").append(result);
    }
  });
  $("#saveSubUnitButton").on('click', function(){
    if($("#su_name").val()== "" || $("#su_pu").dropdown("get value")==""){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Please provide all the necessary details! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    if(rowid == ""){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Please select a row to update! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    var mydata = new FormData();
    mydata.append("suname", $("#su_name").val());
    mydata.append("puname", $("#su_pu").dropdown("get value"));
    mydata.append("rowid", rowid);

    loader("#notification");
    var result = getValueNotAsync('../controller/subunitmodal/updatesubunit.php', 'POST', mydata);
    if (result == "Something went wrong. Please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Something went wrong. Please try again later! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully saved! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      $("#subunit-"+rowid).html(result);
    }
    $("#subunit-"+rowid).removeClass("clickedtr");
    rowid = "";
  });
  $("#subunitListTable").on('click', 'div', function(){
    rowid = $(this).attr("id");
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    var processunitname = product[0];
    $("#yesnomodalSubunit").modal({allowMultiple:true}).modal("close");
    $("#question").html("<h3>Are you sure you want to delete sub unit "+processunitname+"? <i class='warning sign icon'></i></h3>");
    $("#yesnomodalSubunit").modal({allowMultiple:true}).modal("show");
  });
  $("#yesSubUnitButton").on('click', function(){
    loader("#notification");
    $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    var mydata = new FormData();
    mydata.append("rowid", rowid);
    var result = getValueNotAsync('../controller/subunitmodal/deletesubunit.php', 'POST', mydata);
    if(result == "Something went wrong. please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#subunit-"+rowid).remove();
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully deleted! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }
    $("tr").removeClass("clickedtr");
    rowid = "";
  });
});
