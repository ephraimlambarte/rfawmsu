$(document).ready(function(){
    $("#generateQuestion").on('click', function(){
        var formdata = new FormData();
        formdata.append('username', $("#username").val());
        var question = getValueNotAsync('../controller/manageaccounts/getquestion.php', 'POST', formdata);
        if(question!=""){
            $("#securityQuestion").text(question);
        }else{
            alert("no question for this account!")
        }
    });
    $("#submitAnswer").on('click', function(){
        var question = $("#securityQuestion").text();
        if(question == "..."){
            alert("Please enter a username to generate a question!");
            return;
        }
        var formdata = new FormData();
        formdata.append('username', $("#username").val());
        formdata.append('question', question);
        formdata.append('answer', $("#answer").val());
        var checkifquestionmatch = getValueNotAsync('../controller/forgotpassword/getquestion.php', 'POST', formdata);
        if(checkifquestionmatch == "Correct answer!"){
            alert("Sending email.")
            var initusername = getValueNotAsync('../controller/forgotpassword/initusername.php', 'POST', formdata);
            window.location.href = "../views/sendemail.php";
        }else{
            alert(checkifquestionmatch)
        } 
    });
});