$(document).ready(function(){
  var thisdata =  new FormData();
  //getAjax('../controller/rfapartone/getadmin.php', 'POST',thisdata, '#rfa_acknowledgeby');
  getAjaxValue('../controller/rfapartone/getadminid.php', 'POST',thisdata, '#acknowledgeby');
  getAjaxValue('../controller/rfapartone/getadminid.php', 'POST',thisdata, '#issuedby');
  loadCUtoDropBox();
  loadConformedBy();
   loadIssuedBy();
   loadAcknowledgeBy();
  $(".ui.dropdown").dropdown();
  $("#rfa_cu").on('change', function(){
    $("#rfa_pu").dropdown('clear'); 
    $("#rfa_su").dropdown('clear');
    var mydata = new FormData();
    mydata.append("cuid", $("#rfa_cu").dropdown("get value"));
    getAjax('../controller/rfapartone/loadpudropdown.php', 'POST',mydata, '#loadrfa_pu');

  });
  $("#rfa_pu").on('change', function(){
      $("#rfa_su").dropdown('clear');
      var mydata = new FormData();
      mydata.append("puid", $("#rfa_pu").dropdown("get value"));
      getAjax('../controller/rfapartone/loadsudropdown.php', 'POST',mydata, '#loadrfa_su');
  });
  $("#saveRFAPartOneButton").on('click', function(){
    var date1 = new Date( $('#rfa_dateissued').val());
    var date2 = new Date($('#rfa_duedate').val());
    var timeDiff = date2.getTime() - date1.getTime();
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
    if(diffDays<30){
      $("#notification").html("<h3>Due Date must atleast be 30 days! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal("show");
      return;
    }
    var coreunit = $('#rfa_cu').dropdown('get value');
    var processunit = $('#rfa_pu').dropdown('get value');
    var subunit = $('#rfa_su').dropdown('get value');
    var actionType = getActionType();
    var requestSource = getRequestSource();

    var riskLevel = getRiskLevel();
    var nonComformity = $("#rfa_nonconformity").val();
    var description = $("#rfa_desc").val();
    var rfaDateCreated = $('#rfa_dateissued').val();
    var rfaDateDue = $('#rfa_duedate').val();
    var rfaConformedBy = $('#rfa_conformedby').dropdown('get value');
    var rfaAcknowledgeBy = $('#rfa_acknowledgeby').dropdown('get value');
   
    //var issuedby = $("#rfa_issuedby").dropdown('get value');
    var issuedby = $("#issuedby1").val();
    if(issuedby == ""){
      issuedby = $("#issuedby1").dropdown('get value');
    }
    

    var ispecified = false;
    var specifiedRequestSource = $("#rq_s").val();
    if(coreunit == "" || processunit == "" || actionType == "" || requestSource == "" ||
      riskLevel == "" || nonComformity == "" || description == "" || rfaDateCreated == "" || rfaDateCreated == "" ||
      rfaDateDue == "" || rfaConformedBy == "" || rfaAcknowledgeBy == "" || issuedby ==""){
          $("#notification").html("<h3>Please fill up all the necessary fields! <i class='warning sign icon'></i></h3>");
          $("#notificationmodal").modal("show");
          return;
      }
    if(requestSource == "specify"){
      ispecified=true;
    }
    if(ispecified == true){
      if($("#rq_s").val() == ""){
        $("#notification").html("<h3>Please specify your request source! <i class='warning sign icon'></i></h3>");
        $("#notificationmodal").modal("show");
        return;
      }
    }
    var mydata = new FormData();
    mydata.append("coreunit", coreunit);
    mydata.append("processunit", processunit);
    mydata.append("subunit", subunit);
    mydata.append("actionType",actionType);
    mydata.append("requestSource",requestSource);
    mydata.append("riskLevel",riskLevel);
    mydata.append("nonComformity",nonComformity);
    mydata.append("description",description);
    mydata.append("rfaDateCreated",rfaDateCreated);
    mydata.append("rfaDateDue",rfaDateDue);
    mydata.append("rfaConformedBy",rfaConformedBy);
    mydata.append("rfaAcknowledgeBy",rfaAcknowledgeBy);
    mydata.append("issuedby",issuedby);
    mydata.append("ispecified",ispecified);
    mydata.append("specifiedRequestSource", specifiedRequestSource);
    loader("#notification");
    $("#notificationmodal").modal("show");
    getAjax('../controller/rfapartone/insertrfapartone.php', 'POST', mydata, "#notification");
    $("#rfa_nonconformity").val("");
    $("#rfa_desc").val("");
    $("#rq_s").val("");
  });
  $(".checkboxRequestSource").on('click', function(){
    var id = $(this).attr("id");
    //$("#"+id).attr("checked", false);
    if(id!="rq_ia"){
      $("#rq_ia").prop("checked", false);
    }
    if(id!="rq_ea"){
      $("#rq_ea").prop("checked", false);
    }
    if(id!="rq_cf"){
      $("#rq_cf").prop("checked", false)
    }
    if(id!="rq_mr"){
      $("#rq_mr").prop("checked", false)
    }
    if(id!="rq_os"){
      $("#rq_os").prop("checked", false)
    }
  });
  $(".checkBoxActionType").on('click', function(){
    var id  = $(this).attr("id");
    if(id!= "at_pa"){
      $("#at_pa").prop("checked", false);
    }
    if(id!= "at_ca"){
      $("#at_ca").prop("checked", false);
    }
  });
  $(".checkBoxRiskLevel").on('click', function(){
    var id  = $(this).attr("id");
    if(id!= "rfa_hr"){
      $("#rfa_hr").prop("checked", false);
    }
    if(id!= "rfa_mr"){
      $("#rfa_mr").prop("checked", false);
    }
    if(id!= "rfa_lr"){
      $("#rfa_lr").prop("checked", false);
    }
  });
  $("#notificationusermodalContainer").on('click', '.unread',function(){
    var id = $(this).prop('id');
    var mydata = new FormData();
    id = id.split("-")[1];
    mydata.append("id", id);
    getAjaxNotif('../controller/notification/initializenotificationid.php', 'POST', mydata, "#notificationusermodalContainerss");
  });
});
function loadCUtoDropBox(){

  $.ajax({

    url:"../controller/rfapartone/rfapartone.php",
    type:"POST",
    data:{func: 1},
    success: function(result){

      $("#loadrfa_cu").html(result);

    }

  });

}
function getActionType(){
  if($("#at_pa").is(":checked")){
    return "Preventive Action";
  }else if ($("#at_ca").is(":checked")) {
    return "Corrective Action";
  }else{
    return "";
  }
}
function getRequestSource(){
  if($("#rq_ia").is(":checked")){
    return "Internal Audit";
  }else if ($("#rq_ea").is(":checked")) {
    return "External Audit";
  }else if ($("#rq_cf").is(":checked")) {
    return "Client Feedback";
  }else if ($("#rq_mr").is(":checked")) {
    return "Management Review";
  }else if ($("#rq_os").is(":checked")) {
    return "specify";
  }else{
    return "";
  }
}
function getRiskLevel(){
  if($("#rfa_hr").is(":checked")){
    return "High Risk";
  }else if ($("#rfa_mr").is(":checked")) {
    return "Medium Risk"
  }else if ($("#rfa_lr").is(":checked")) {
    return "Low Risk";
  }else {
    return "";
  }
}
function loadConformedBy(){

  $.ajax({

    url:"../controller/rfapartone/rfapartone.php",
    type:"POST",
    data:{func: 5},
    success: function(result){

      $("#loadrfa_conformedby").html(result);

    }

  });

}
function loadIssuedBy(){

    $.ajax({

      url:"../controller/rfapartone/loadIssuedBy.php",
      type:"POST",
      data:{func: 5},
      success: function(result){

        $("#loadrfa_issuedby").html(result);

      }

    });
}
function loadAcknowledgeBy(){
  $.ajax({
    
        url:"../controller/rfapartone/loadrfaacknowledgeby.php",
        type:"POST",
        data:{func: 5},
        success: function(result){
    
          $("#loadrfa_acknowledgeby").html(result);
    
        }
    
  });
}