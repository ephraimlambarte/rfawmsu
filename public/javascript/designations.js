$(document).ready(function(){
    var rowid = "";
    var coreunitname = "";
    tableLoader("#designationListTable", 2);
    var thisdata =  new FormData();
    getAjax('../controller/designation/loadDesignationTable.php', 'POST', thisdata, "#designationListTable");
    $("#addDesignationModalButton").on('click', function(){
        var designationName = $("#designation_name").val();
        if(designationName == ""){
          $("#notification").html("<h3>Please fill up the necessary fields! <i class='warning sign icon'></i></h3>");
          $("#notificationmodal").modal({allowMultiple:true}).modal("show");
          return;
        }
        var mydata =  new FormData();
        mydata.append("designationName", designationName);
        loader("#notification");
        $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        var result = getValueNotAsync('../controller/designation/adddesignation.php', 'POST', mydata);
        if(result == "Something went wrong. please try again later!"){
          $("#notificationmodal").modal({allowMultiple:true}).modal("close");
          $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
          $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        }else{
          $("#designationListTable").append(result);
          $("#notificationmodal").modal({allowMultiple:true}).modal("close");
          $("#notification").html("<h3>Successfully added! <i class='info circle icon'></i></h3>");
          $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        }
    });
    $("#designationListTable").on('click', 'tr', function(){
        var thisid = $(this).attr("id");
        rowid = thisid.split("-")[1];
    
        var i = 0;
        var product = [""];
        $(this).closest('tr').each(function(){
          $(this).find('td').each(function(){
            product[i]=$(this).html();
            i++;
          });
        });
        $("#designation_name").val(product[0]);
        $("tr").removeClass("clickedtr");
        $("#designation-"+rowid).addClass("clickedtr");
    });
    $("#clearDesignationModalButton").on('click', function(){
        rowid = "";
        $("#designation_name").val("");
        $("tr").removeClass("clickedtr");
    });
    $("#saveDesignationModalButton").on('click', function(){
        var designationName = $("#designation_name").val();
        if(designationName == ""){
          $("#notification").html("<h3>Please fill up the necessary fields! <i class='warning sign icon'></i></h3>");
          $("#notificationmodal").modal({allowMultiple:true}).modal("show");
          return;
        }
        var mydata =  new FormData();
        mydata.append("designationName", designationName);
        mydata.append("rowid", rowid);
        if(rowid == ""){
          $("#notification").html("<h3>Please select a row to update!</h3>");
          $("#notificationmodal").modal({allowMultiple:true}).modal("show");
          return;
        }
        loader("#notification");
        $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        var result = getValueNotAsync('../controller/designation/updatedesignation.php', 'POST', mydata);
        if(result == "Something went wrong. please try again later!"){
          $("#notificationmodal").modal({allowMultiple:true}).modal("close");
          $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
          $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        }else{
          $("#designation-"+rowid).html(result);
          $("#notificationmodal").modal({allowMultiple:true}).modal("close");
          $("#notification").html("<h3>Successfully saved! <i class='info circle icon'></i></h3>");
          $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        }
        $("#designation-"+rowid).removeClass("clickedtr");
        rowid = "";
    
    });  
    $("#designationListTable").on('click', 'div', function(){
        //rowid = $(this).attr("id");
        var i = 0;
        var product = [""];
        $(this).closest('tr').each(function(){
            //rowid = $(this).prop("id").split("-")[1];
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        var designationName = product[0];
        $("#yesnomodalDesignation").modal({allowMultiple:true}).modal("close");
        $("#questionss").html("<h3>Are you sure you want to delete "+designationName+"? <i class='warning sign icon'></i></h3>");
        $("#yesnomodalDesignation").modal({allowMultiple:true}).modal("show");
    });
    $("#yesDesignationButton").on('click', function(){
        loader("#notification");
        $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        var mydata = new FormData();
        mydata.append("designationID", rowid);
        var result = getValueNotAsync('../controller/designation/deletedesignation.php', 'POST', mydata);
        if(result == "Something went wrong. please try again later!"){
          $("#notificationmodal").modal({allowMultiple:true}).modal("close");
          $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
          $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        }else{
          $("#designation-"+rowid).remove();
          $("#notificationmodal").modal({allowMultiple:true}).modal("close");
          $("#notification").html("<h3>Successfully deleted! <i class='info circle icon'></i></h3>");
          $("#notificationmodal").modal({allowMultiple:true}).modal("show");
        }
        $("tr").removeClass("clickedtr");
        rowid = "";
    });
});