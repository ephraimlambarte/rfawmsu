
  var ActionType = 0;
  var RequestSource = 0;
  var RFARisk = 0;

  //document.getElementById('rfa_dateissued').addEventListener("change",dateCreatedChange);

  window.onload = loadData();

  function loadData(){

    generateControlNumber();
    loadCUtoDropBox();
    loadConformedBy();

  }

  function showCUModal(){

    $('#coreunitmodal').load('../modals/coreunitmodal.php', function(){

      $('#coreunitmodal').modal('setting','allowMultiple',true).modal('show');

    });

  }

  function showPUModal(){

    $('#processunitmodal').load('../modals/processunitmodal.php', function(){

      $('#processunitmodal').modal('setting','closable',true).modal('show');

    });

  }

  function showSUModal(){

    $('#subunitmodal').load('../modals/subunitmodal.php', function(){

      $('#subunitmodal').modal('setting','closable',true).modal('show');

    });

  }

  function loadCUtoDropBox(){

    $.ajax({

      url:"../controller/rfapartone/rfapartone.php",
      type:"POST",
      data:{func: 1},
      success: function(result){

        $("#loadrfa_cu").html(result);

      }

    });

  }

  function loadConformedBy(){

    $.ajax({

      url:"../controller/rfapartone/rfapartone.php",
      type:"POST",
      data:{func: 5},
      success: function(result){

        $("#loadrfa_conformedby").html(result);

      }

    });

  }

  function generateControlNumber(){

    $.ajax({

      url:"../controller/rfapartone/rfapartone.php",
      type:"POST",
      data:{func: 4},
      success: function(result){

        $("#rfa_controlnum").val(result);

      }

    });

  }

  function saveRFA(){

    var rfaCU = $('#rfa_cu').dropdown('get value');
    var rfaPU = $('#rfa_pu').dropdown('get value');
    var rfaSU = $('#rfa_su').dropdown('get value');
    var rfaDateCreated = $('#rfa_dateissued').val();
    var rfaDateCreated = rfaDateCreated.split("/")[2]+"-"+rfaDateCreated.split("/")[0]+"-"+rfaDateCreated.split("/")[1];
    var rfaDateDue = $('#rfa_duedate').val();
    var rfaDateDue = rfaDateDue.split("/")[2]+"-"+rfaDateDue.split("/")[0]+"-"+rfaDateDue.split("/")[1];
    var rfaRequesSourceSpecified = $('#rq_s').val();
    var rfaNonConformity = $('#rfa_nonconformity').val();
    var rfaDesc = $('#rfa_desc').val();
    var rfaConformedBy = $('#rfa_conformedby').dropdown('get value');
    var rfaAcknowledgeBy = $('#acknowledgeby').val();
    var issuedby = $("#issuedby").val();
                                          "(coureunitid, processunitid," +
                                      +  "    subunitid, rfaTypeOfAction, rfaRequestSource, rfaRequestSourceSpecified," +"
                                            rfaNonComformity, rfaDescription, rfaRiskLevel, rfaDateCreated, rfaDateDue,"+"
                                            rfaIssuedBy, rfaConformedBy, rfaAcknowledgeBy,issued, acknowledged, conformed)"
    $.ajax({

      url:"../controller/rfapartone/rfapartone.php",
      type:"POST",
      data:{func: 7, coreUnit:rfaCU, processUnit:rfaPU, subUnit:rfaSU, typeOfAction:ActionType, },
      success: function(result){
        //alert(result)
        $("#notificationmodal").modal("show");
        loader("#notification");
        if($.trim(result) == 1){
          $("#notification").html("<h3>Saved!</h3>");
          location.reload();
        }else {

          $("#notification").html("<h3>An error occured. please try again later!</h3>");

        }

      }

    });

  }

  function logoutacc(){

    if(confirm("Are you sure you want to logout?")){

      $.ajax({

        url:"../PHP/Controller/account.php",
        type:"POST",
        data: {func: 7},
        success: function(result){

          if($.trim(result) == 1){

            window.location = "../index.php";

          }

        }

      });

    }

  }

  //-----------------------------EVENTS------------------------------------//

  function RiskLevelCheckBox(checkbox){

    var elements = checkbox.split("/");

    for(var i=0;i<2;i++){

      document.getElementById('rfa_'+elements[i]).checked = false;

    }

    if(document.getElementById("rfa_lr").checked == true) {

      RFARisk = 1;

    }

    else if(document.getElementById("rfa_mr").checked == true) {

      RFARisk = 2;

    }

    else if(document.getElementById("rfa_hr").checked == true){

      RFARisk = 3;

    }

  }

  function ActionTypeCheckBox(checkbox){

    var element = document.getElementById(checkbox);

    element.checked = false;

    if(document.getElementById("at_ca").checked == true){

      ActionType = 1;

    }
    else {

      ActionType = 2;

    }

  }

  function RequestSourceCheckBox(checkbox){

    var elements = checkbox.split("/");

    for(var i=0;i<4;i++){

      document.getElementById('rq_'+elements[i]).checked = false;

    }

    if(document.getElementById("rq_ia").checked == true) {

      RequestSource = 1;

    }

    else if(document.getElementById("rq_ea").checked == true) {

      RequestSource = 2;

    }

    else if(document.getElementById("rq_cf").checked == true){

      RequestSource = 3;

    }

    else if(document.getElementById("rq_mr").checked == true) {

      RequestSource = 4;

    }

    else {

      RequestSource = 5;

    }

  }

  /*$('#rfa_cu').change(function(){

    $('#rfa_pu').dropdown('clear');
    $('#rfa_su').dropdown('clear');

    var id = $('#rfa_cu').dropdown('get value');

    $.ajax({

      url:"../controller/rfapartone/rfapartone.php",
      type:"POST",
      data:{func: 2, id:id},
      success: function(result){

        $("#loadrfa_pu").html(result);

      }

    });

  });

  $('#rfa_pu').change(function(){

    $('#rfa_su').dropdown('clear');

    var id = $('#rfa_pu').dropdown('get value');

    $.ajax({

      url:"../controller/rfapartone/rfapartone.php",
      type:"POST",
      data:{func: 3, id:id},
      success: function(result){

        $("#loadrfa_su").html(result);

      }

    });

  });*/

  $(".rfaModalDropDown").dropdown();
