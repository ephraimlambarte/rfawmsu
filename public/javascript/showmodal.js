function showCUModal(){

  $('#coreunitmodal').load('../modals/coreunitmodal.php', function(){

    $('#coreunitmodal').modal('setting','closable',true).modal('show');

  });

}

function showPUModal(){

  $('#processunitmodal').load('../modals/processunitmodal.php', function(){

    $('#processunitmodal').modal('setting','closable',true).modal('show');

  });

}

function showSUModal(){

  $('#subunitmodal').load('../modals/subunitmodal.php', function(){

    $('#subunitmodal').modal('setting','closable',true).modal('show');

  });

}

function showDesignationModal(){
  
    $('#designationmodal').load('../modals/designationsmodal.php', function(){
  
      $('#designationmodal').modal('setting','closable',true).modal('show');
  
    });
  
  }
  
function showNotsModal(){
    //alert("Hello");
    var thisdata =  new FormData();
    $('#notificationusermodal').modal('setting','closable',true).modal('show');
    loader("#notificationusermodalContainer");
    getAjax('../controller/notification/loadnotification.php', 'POST', thisdata, "#notificationusermodalContainer");
}

setInterval(function(){
  var mydata = new FormData();
  $.ajax({
			url: '../controller/checkifinactive.php', // Url to which the request is send
			type: 'POST',             // Type of request to be send, called as method
			data: mydata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
      async:false,
			success: function(data)   // A function to be called if request succeeds
			{
        //console.log(data);
			   if(data == 'logout'){
            logout();
         }else{
           console.log(data);
         }
			}
	});
}, 2000);
