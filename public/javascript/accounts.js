$(document).ready(function(){
  var username;
  var rowid="";
  var name;
  var type;
  var activate;
  var usertype;
  $(".dropdown").dropdown();
  $("#loadaccountshere").html("<td colspan='5'><div id='myloader'><div class='flexdiv'></div><div id = 'loaderdiv'><div class='cssload-loader'></div></div><div class='flexdiv'></div></div></td>");
  var thisdata = new FormData();
  getAjax('../controller/manageaccounts/loadaccounts.php', 'POST',thisdata, '#loadaccountshere');
  $("#registerButton").on('click', function(){

    var name = document.getElementById('new_accname').value;
    var firstname = "";
    var lastname = "";
    if (name.split(" ").length>2){
      fname = name.split(" ").slice(1-name.split(" ").length);
      lastname = name.split(" ")[0];
      for(var i =0; i<fname.length; i++){
          firstname +=fname[i]+" ";
      }
    }else{
      firstname = name.split(" ")[1];
      lastname = name.split(" ")[0];
    }
    if (lastname[lastname.length-1]==","){
      lastname = lastname.slice(0,lastname.length-1);
    }
    var type = $("#new_acctype").dropdown("get value");
    var un = document.getElementById('new_accun').value;
    var pw = document.getElementById('new_accpw1').value;
    var question = $("#securityQuestion").dropdown("get value");
    var answer = $("#questionAnswer").val();
    var email = $("#email").val();
    var designation = $("#designationDropbox").dropdown("get value");
    if ($("#new_accpw1").val() != $("#new_accpw2").val()){
      $("#notification").html("<h3>Password not the same! <i class='warning sign icon'></i></h3>");
      $('#notificationmodal').modal('show');
      return;
    }
    if (designation=="" || firstname =="" || type == "" || email == "" || 
    question == "" || answer == "" || un == "" || pw == "" || lastname == "" 
    || firstname == undefined || lastname == undefined){
      $('#notificationmodal').modal('show');
      $("#notification").html("<h3>Provide all the necessary details! <i class='warning sign icon'></i></h3>");
      return;
    }
    var formdata = new FormData()
    formdata.append("firstname", firstname);
    formdata.append("lastname", lastname);
    formdata.append("type", type);
    formdata.append("designation", designation);
    formdata.append("username", un);
    formdata.append("password", pw);
    formdata.append("question", question);
    formdata.append("answer", answer);
    formdata.append("email", email);
   
    $('#notificationmodal').modal('show');
    loader("#notification");
    var result=AjaxNotAsync('../controller/manageaccounts/addaccount.php', 'POST', formdata, '#loadaccountshere');
    if (result=="Username already exists!") {
      $('#notification').html("<h3>"+result+"</h3>");
    } else if (result=="Something went wrong. try again later!") {
      $('#notification').html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
    } else{
      $('#notification').html("<h3>Success! <i class='info circle icon'></i></h3>");
    }
    username = "";
    rowid = "";
    $("tr").removeClass('clickedtr');
    $("#new_accname").val("");
    $('#new_acctype').dropdown('restore defaults');
    $("#new_accun").val("");
    $("#new_accpw1").val("");
    $("#new_accpw2").val("");
    $("#email").val("")
    $("#questionAnswer").val("");
    var data = result.split("*")[0];
    $("#loadaccountshere").append(data);
  });
  $("#loadaccountshere").on('click', 'tr', function(event){
    rowid = $(this).attr('id');

    $("tr").removeClass('clickedtr');
    $(this).addClass('clickedtr');
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    username = product[1];
    $("#new_accname").val(product[0]);
    $("#new_accun").val(product[1]);
    $("#new_acctype").dropdown('set selected', product[2]);
    usertype = product[2];
    var formdata = new FormData();
    formdata.append('username', username);
    var password = getValueNotAsync('../controller/manageaccounts/getpassword.php', 'POST', formdata);
    var question = getValueNotAsync('../controller/manageaccounts/getquestion.php', 'POST', formdata);
    var answer = getValueNotAsync('../controller/manageaccounts/getanswer.php', 'POST', formdata);
    var email = getValueNotAsync('../controller/manageaccounts/getemail.php', 'POST', formdata);
    var designation = getValueNotAsync('../controller/manageaccounts/getdesignation.php', 'POST', formdata);
    $("#securityQuestion").dropdown('set selected', question);
    $("#questionAnswer").val(answer);
    $("#new_accpw1").val(password);
    $("#new_accpw2").val(password);
    $("#email").val(email);
    $("#designationDropbox").dropdown('set selected',designation);
  });
  $("#clearButton").on('click', function(){
    username = "";
    rowid = "";
    $("tr").removeClass('clickedtr');
    $("#new_accname").val("");
    $('#new_acctype').dropdown('restore defaults');
    $("#new_accun").val("");
    $("#new_accpw1").val("");
    $("#new_accpw2").val("");
    $("#email").val("")
    $("#questionAnswer").val("");
  });
  $("#saveButton").on('click', function(){
    if(rowid==""){
      $("#notification").html("<h3>Please select a row to edit!</h3>");
      $('#notificationmodal').modal('show');
      return;
    }
    var name = document.getElementById('new_accname').value;
    var firstname = "";
    var lastname = "";
    if (name.split(" ").length>2){
      fname = name.split(" ").slice(1-name.split(" ").length);
      lastname = name.split(" ")[0];
      for(var i =0; i<fname.length; i++){
          firstname +=fname[i]+" ";
      }
    }else{
      firstname = name.split(" ")[1];
      lastname = name.split(" ")[0];
    }
    if (lastname[lastname.length-1]==","){
      lastname = lastname.slice(0,lastname.length-1);
    }

    var type = $("#new_acctype").dropdown("get value");
    if(usertype == "Admin"){
      type = "Admin";
    }
    var un = document.getElementById('new_accun').value;
    var pw = document.getElementById('new_accpw1').value;
    var question = $("#securityQuestion").dropdown("get value");
    var answer = $("#questionAnswer").val();
    var email = $("#email").val();
    var designation = $("#designationDropbox").dropdown("get value");
    if ($("#new_accpw1").val() != $("#new_accpw2").val()){
      $("#notification").html("<h3>Password not the same! <i class='warning sign icon'></i></h3>");
      $('#notificationmodal').modal('show');
      return;
    }
    if (designation == "" || firstname =="" || type == "" || email == "" || question == "" || answer == "" || un == "" || pw == "" || lastname == "" || firstname == undefined || lastname == undefined){
      $('#notificationmodal').modal('show');
      $("#notification").html("<h3>Provide all the necessary details! <i class='warning sign icon'></i></h3>");
      return;
    }
    var formdata = new FormData()
    formdata.append("firstname", firstname);
    formdata.append("lastname", lastname);
    formdata.append("type", type);
    formdata.append("username", un);
    formdata.append("password", pw);
    formdata.append("prevusername", username);
    formdata.append("question", question);
    formdata.append("answer", answer);
    formdata.append("email", email);
    formdata.append("designation", designation);
    $('#notificationmodal').modal('show');
    loader("#notification");
    var result =  AjaxNotAsync('../controller/manageaccounts/updateaccount.php', 'POST', formdata, '#loadaccountshere');
    if (result == "Username already exists!") {
      $("#notification").html("<h3>"+result+"</h3>");
    }else if (result == "Something went wrong. please try again later!") {
        $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
    }else {
      $("#"+rowid).html(result.split("*")[0]);
      $("#notification").html("<h3>"+result.split("*")[1]+" <i class='info circle icon'></i></h3>");
    }
    console.log(result);
    username = "";
    rowid = "";
    $("tr").removeClass('clickedtr');
    $("#new_accname").val("");
    $('#new_acctype').dropdown('restore defaults');
    $("#new_accun").val("");
    $("#new_accpw1").val("");
    $("#new_accpw2").val("");
    $("#email").val("")
    $("#questionAnswer").val("");
  });
  $("#loadaccountshere").on('click', 'div', function(event){
    rowid = $(this).attr('id');

    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    username = product[1];

    name = product[0];
    type = product[2];
    var text = "";
    if(product[3]=="Active"){
      activate = "deactivate";
      text = "Are you sure you want to deactivate/delete account of ";
    }else{
      text = "Are you sure you want to activate account of ";
      activate = "activate";
    }
    $("#yesnomodal").modal("show");
    $("#question").html(text+" "+product[0]+"? <i class='warning sign icon'></i>");
  });
  $("#yesButton").on('click', function(event){

    $("#notificationmodal").modal("show");
    loader("#notification");
    var formdata = new FormData();
    formdata.append("id", rowid);
    formdata.append("activate", activate);
    var result = AjaxNotAsync('../controller/manageaccounts/deleteaccount.php', 'POST', formdata, '#loadaccountshere');
    if (result == "deleted!") {
      $("#notification").html("<h3>"+result+"</h3>");
      $("#"+rowid).remove();
    }else if (result == "updated!") {
      var text = "";
      if(activate == "activate"){
        text = "Active";
      }else{
        text = "Deactivated";
      }
      $("#notification").html("<h3>"+result+" <i class='info circle icon'></i></h3>");
      $("#"+rowid).html("<td>"+name+"</td><td>"+username+"</td><td>"+type+
      "</td><td>"+text+"</td><td><div class='ui icon button'><i class='delete icon'></i></div></td>");
    }else{
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
    }
  });
  $("#notificationusermodalContainer").on('click', '.unread',function(){
    var id = $(this).prop('id');
    var mydata = new FormData();
    id = id.split("-")[1];
    mydata.append("id", id);
    getAjaxNotif('../controller/notification/initializenotificationid.php', 'POST', mydata, "#notificationusermodalContainerss");
  });
});
