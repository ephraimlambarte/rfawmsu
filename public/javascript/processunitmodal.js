$(document).ready(function(){
  var rowid = "";
  var thisdata = new FormData();
  getAjax('../controller/processunitmodal/loadprocessunitdropbox.php', 'POST', thisdata, "#mycoreunit3")
  $(".ui.dropdown").dropdown();
  tableLoader("#processunitListTable", "5");
  getAjax('../controller/processunitmodal/loadprocessunit.php', 'POST', thisdata, "#processunitListTable")
  $("#processunitListTable").on('click', 'tr', function(){
    var thisid = $(this).attr("id");
    rowid = thisid.split("-")[1];

    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    $("#pu_name").val(product[0]);
    $("#mycoreunit").dropdown("set selected", product[1]);
    $("tr").removeClass("clickedtr");
    $("#pumodal-"+rowid).addClass("clickedtr");
  });
  $("#processUnitModalClearButton").on('click', function(){
    rowid = "";
    $('#mycoreunit').dropdown('restore defaults');
    $("#pu_name").val("");
    $("tr").removeClass("clickedtr");
  });
  $("#processUnitModalSaveButton").on('click', function(){

    var pu = "";
    var cu = "";
    pu = $("#pu_name").val();
    cu = $("#mycoreunit").dropdown("get value");

    if(rowid == ""){
      $("#notification").html("<h3>Please select a row to update!</h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    if(pu == "" || cu==""){
      $("#notification").html("<h3>Please fill up the necessary fields!</h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    var mydata = new FormData();
    mydata.append("pu", pu);
    mydata.append("cu", cu);
    mydata.append("puid", rowid);
    loader("#notification");
    $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    var result = getValueNotAsync('../controller/processunitmodal/updateprocessunitmodal.php', 'POST', mydata);
    if(result == "Something went wrong. please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#pumodal-"+rowid).html(result);
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully saved! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }
    $("#pumodal-"+rowid).removeClass("clickedtr");
    rowid = "";

  });
  $("#processUnitModalAddButton").on('click', function(){
    var pu = "";
    var cu = "";
    pu = $("#pu_name").val();
    cu = $("#mycoreunit").dropdown("get value");
    if(pu == "" || cu==""){
      $("#notification").html("<h3>Please fill up the necessary fields! <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
      return;
    }
    var mydata =  new FormData();
    mydata.append("pu", pu);
    mydata.append("cu", cu);
    loader("#notification");
    $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    var result = getValueNotAsync('../controller/processunitmodal/addprocessunit.php', 'POST', mydata);
    if(result == "Something went wrong. please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#processunitListTable").append(result);
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully added! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }
  });
  $("#processunitListTable").on('click', 'div', function(){
    //rowid = $(this).attr("id");
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    var processunitname = product[0];
    $("#yesnomodalProcessUnit").modal({allowMultiple:true}).modal("close");
    $("#questionss").html("<h3>Are you sure you want to delete process unit "+processunitname+"? <i class='warning sign icon'></i></h3>");
    $("#yesnomodalProcessUnit").modal({allowMultiple:true}).modal("show");
  });
  $("#yesMyProcessUnitButton").on('click', function(){
    loader("#notification");
    $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    var mydata = new FormData();
    mydata.append("puid", rowid);
    var result = getValueNotAsync('../controller/processunitmodal/deleteprocessunit.php', 'POST', mydata);
    if(result == "Something went wrong. please try again later!"){
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>"+result+" <i class='warning sign icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }else{
      $("#pumodal-"+rowid).remove();
      $("#notificationmodal").modal({allowMultiple:true}).modal("close");
      $("#notification").html("<h3>Successfully deleted! <i class='info circle icon'></i></h3>");
      $("#notificationmodal").modal({allowMultiple:true}).modal("show");
    }
    $("tr").removeClass("clickedtr");
    rowid = "";
  });
});
