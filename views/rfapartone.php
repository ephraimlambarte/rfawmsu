<?php
  session_start();
  include "../model/model.php";
  $model = new model();
  $model->connectDatabase();
  if($_SESSION['user']==""){
    header("Location: ../index.php");
  }
  if(isset($_SESSION['notifrfa'])){
    if($_SESSION['notifrfa']!= "initalized"){
      $model->sendNotifRfa($_SESSION['user']);
      $_SESSION['notifrfa']= "initalized";
    }
  }
 ?>

  <!DOCTYPE html>
  <html>

    <head>

      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

      <title>RFA Monitoring</title>

      <?php include "./includeheader2.php"; ?>
      <link rel = "stylesheet" type = "text/css" href="../public/stylesheet/rfapartone.css">
    </head>

    <body>

      <header>

        <div class="ui menu"> <!-- TOP HEADER -->
          <div class="item top-header">
            <img class="ui middle aligned tiny image" src="../public/images/logo.png">
            <span class="top-header-span"><div>
              Western Mindanao State University
            </div>
            <div>
              Quality Management Office
            </div>
            <div>
              RFA MONITORING SYSTEM
            </div>
            </span>
          </div>
        </div>

        <div class="ui right aligned secondary menu header-item-2"> <!-- BOTTOM HEADER -->

          <div class="left item header-menu">
            <i class="sidebar inverted icon"></i>
          </div>

          <div class="right item">

            <span class="header-right-item"><?php echo $model->getAccNameById($_SESSION['user']); ?>

            </span>
            <span class="header-right-item">|</span>

            <span style="cursor: pointer" class="header-right-item"
                  onclick="logout()">LOGOUT</span>

          </div>

        </div>

      </header>

      <section>

        <div class="ui grid">

          <div class="three wide column">

            <?php include "./sideNavbar.php"; ?>

          </div>

          <div class="twelve wide column">

            <form class="ui fluid form" onsubmit="return false">

              <div class="ui grey inverted top attached segment" style="margin-top: 50px; border-color: grey;">
                <p>Create Request For Action</p>
              </div>

              <div class="ui attached segment"> <!-- FIRST ROW -->

                <div class="ui grid">

                  <div class="row"> <!-- DATE ISSUED -->

                    <div class="twelve wide column">
                    </div>

                    <div class="four wide column">

                      <div class="sixteen wide field" style="float: right">
                        <label>Date:</label>
                        <input type="date" id="rfa_dateissued" value="<?php echo date("Y-m-d");?>" readonly>
                      </div>

                    </div>

                  </div>

                  <div class="row" style="padding-top: 0px">

                    <div class="four wide column"> <!-- RFA CORE UNIT -->

                      <div class="sixteen wide field" id="rfa_cudiv">

                        <label>Core Unit</label>
                        <div class="ui selection dropdown rfaModalDropDown" id="rfa_cu">

                          <input type="hidden">
                          <i class="dropdown icon"></i>
                          <div class="default text">Core Unit</div>

                          <div class="menu" id="loadrfa_cu">

                          </div>

                        </div>

                      </div>

                    </div>

                    <div class="four wide column"> <!-- RFA PROCESS UNIT -->

                      <div class="sixteen wide field" id="rfa_cudiv">

                        <label>Process Unit</label>
                        <div class="ui selection dropdown rfaModalDropDown" id="rfa_pu">

                          <input type="hidden">
                          <i class="dropdown icon"></i>
                          <div class="default text">Process Unit</div>

                          <div class="menu" id="loadrfa_pu">

                          </div>

                        </div>

                      </div>

                    </div>

                    <div class="four wide column"> <!-- RFA SUB UNIT -->

                      <div class="sixteen wide field" id="rfa_sudiv">

                        <label>Sub Unit</label>
                        <div class="ui selection dropdown rfaModalDropDown" id="rfa_su">

                          <input type="hidden">
                          <i class="dropdown icon"></i>
                          <div class="default text">Sub Unit</div>

                          <div class="menu" id="loadrfa_su">

                          </div>

                        </div>

                      </div>

                    </div>

                    <!--div class="four wide column">

                      <div class="sixteen wide field">
                        <label>RFA Control No:</label>
                        <input type="text" placeholder="RFA Control Number" readonly id="rfa_controlnum">
                      </div>

                    </div-->

                  </div>

                </div>

              </div>

              <div class="ui grey inverted attached segment" style="border-color: grey">
                <label><i>Part I: Problem Identification (To be filled-up by the Requesting Source Representative or Auditor)</i><label>
              </div>

              <div class="ui attached segment"> <!-- TYPE OF ACTION -->

                <div class="inline fields">

                  <label>Type of Action:</label>

                  <div class="field">
                    <div class="ui checkbox">
                      <input type="checkbox" name="CorrectiveAction" id="at_ca"
                              value = "Corrective Action" class= "checkBoxActionType">
                      <label>Corrective Action</label>
                    </div>
                  </div>

                  <div class="field">
                    <div class="ui checkbox">
                      <input type="checkbox" name="PreventiveAction" id="at_pa"
                             value = "Preventive Action" class="checkBoxActionType">
                      <label>Preventive Action</label>
                    </div>
                  </div>

                </div>

              </div>

              <div class="ui attached segment"> <!-- Request Source -->

                <div class="fields">

                  <div class="inline fields">

                    <label >Request Source:</label>

                    <div class="field">

                      <div class="grouped fields">

                        <div class="field">
                          <div class="ui checkbox">
                            <input type="checkbox" name="InternalAudit" id="rq_ia"
                            value = "Internal Audit" class = "checkboxRequestSource"
                             >
                            <label>Internal Audit</label>
                          </div>
                        </div>

                        <div class="field">
                          <div class="ui checkbox">
                            <input type="checkbox" name="ExternalAudit" id="rq_ea"
                            value = "External Audit" class = "checkboxRequestSource">
                            <label>External Audit</label>
                          </div>
                        </div>

                      </div>

                    </div>

                    <div class="field">

                      <div class="grouped fields">

                        <div class="field">
                          <div class="ui checkbox">
                            <input type="checkbox" name="ClientFeedbacks" id="rq_cf"
                              value = "Client Feedbacks" class = "checkboxRequestSource">
                            <label>Client Feedback(s)</label>
                          </div>
                        </div>

                        <div class="field">
                          <div class="ui checkbox">
                            <input type="checkbox" name="ManagementReview" id="rq_mr"
                              value = "Management Review" class = "checkboxRequestSource">
                            <label>Management Review</label>
                          </div>
                        </div>

                      </div>

                    </div>

                    <div class="field">

                      <div class="grouped fields">

                        <div class="field">
                          <div class="ui checkbox">
                            <input type="checkbox" name="OthersSpecify" id="rq_os"
                            value = "specify" class = "checkboxRequestSource">
                            <label>Others (Specify)</label>
                          </div>
                        </div>

                      </div>

                    </div>

                  </div>

                  <div class="five wide field">
                    <input type="text" placeholder="Please Specify Here" id="rq_s">
                  </div>

                </div>

              </div>

              <div class="ui attached segment"> <!-- Nonconformance / Potential Nonconformance -->

                <div class="field">
                  <label>Nonconformance / Potential Nonconformance</label>
                  <textarea rows="3" type="text" id="rfa_nonconformity"></textarea>
                </div>

              </div>

              <div class="ui attached segment"> <!-- Description -->

                <div class="field">
                  <label>Description</label>
                  <textarea rows="3" type="text" id="rfa_desc"></textarea>
                </div>

              </div>

              <div class="ui attached segment"> <!-- ISSUED BY / RISK LEVEL / DUE DATE -->

                <div class="ui grid">

                  <div class="eight wide column" style="border-style:none solid none none; border-color: grey">

                    <div class="row">

                      <div class="field">
                        <label>Issued By:</label>
                        <input class="signaturefield" type="text" style="margin-top:19px" placeholder="Signature over printed name" readonly>
                        <!--div class="ui selection dropdown rfaModalDropDown" id="rfa_issuedby">

                          <input type="hidden">
                          <i class="dropdown icon"></i>
                          <div class="default text">Issued By</div>

                          <div class="menu" id="loadrfa_issuedby">

                          </div>

                        </div-->
                      </div>

                    </div>

                    <div class="row" style="margin-top: 10px">
                      <?php
                        if($model->getAdminID() == $_SESSION['user']){

                            echo "<div class='ui selection dropdown rfaModalDropDown' id='issuedby1'>

                              <input type='hidden'>
                              <i class='dropdown icon'></i>
                              <div class='default text'>Issued By</div>

                              <div class='menu' id='loadIssuedBy'>";
                              $sql = "SELECT * FROM tblaccount WHERE (usertype = 'Admin' OR usertype = 'Auditor') AND status = '1'";
                              $query = $model->handler->query($sql);
                              while($row = $query->fetch()){
                                  echo "<div class='item' data-value=\"".$row['accountid']."\">".$row['lastname'].", ".$row['firstname']."</div>";
                              }
                            echo "</div>

                            </div>";
                        }else{
                          echo "<label style='margin-top: 20px; font-weight: 600' id='rfa_issuedby'>".$model->getAccNameById($_SESSION['user'])."</label>";
                          echo "<input type = 'hidden' id = 'issuedby1' value='".$_SESSION['user']."'>";
                        }

                      ?>
                    </div>

                  </div>

                  <div class="four wide column" style="border-style:none solid none none; border-color: grey">

                    <div class="row">
                      <h4 class="ui centered aligned header">RISK LEVEL</h4>
                      <div class="ui clearing divider"></div>
                    </div>

                    <div class="row">

                      <div class="inlinefields" style="margin-top: 25px;">

                        <div class="field">

                          <div align="center">

                            <div class="ui checkbox">
                              <input type="checkbox" name="HighRisk" id="rfa_hr"
                                value = "High" class= "checkBoxRiskLevel">
                              <label style="margin-right:5px"><b>-High</b></label>
                            </div>

                            <div class="ui checkbox">
                              <input type="checkbox" name="MediumRisk" id="rfa_mr"
                                value="Medium" class= "checkBoxRiskLevel">
                              <label style="margin-right:5px"><b>-Medium</b></label>
                            </div>

                            <div class="ui checkbox">
                              <input type="checkbox" name="LowRisk" id="rfa_lr"
                                value="Low" class= "checkBoxRiskLevel">
                              <label style="margin-right:5px"><b>-Low</b></label>
                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                    <div class="row qmc">
                      <label style="margin-left: 20px; color: white; "><i>To: Quality Management Office</i></label>
                    </div>

                  </div>

                  <div class="four wide column">

                    <div class="row">
                      <h4 class="ui centered aligned header">DUE DATE</h4>
                      <div class="ui clearing divider"></div>
                    </div>

                    <div class="row">

                      <div class="field" align="center" style="margin-top: 15px;">
                        <input type="date" id="rfa_duedate" value="<?php echo date('Y-m-d', strtotime("+30 days")); ?>">
                      </div>

                    </div>

                    <div class="row qmc2">
                      <label style="margin-left: 20px; color: white; "></label>
                    </div>

                  </div>

                </div>

              </div>

              <div class="ui attached segment"> <!-- CONFORMED BY / ACKNOWLEGDE BY -->

                <div class="ui grid">

                  <div class="eight wide column" style="border-style:none solid none none; border-color: grey">

                    <div class="row">

                      <div class="field">
                        <label>Conformed By:</label>
                        <input class="signaturefield" type="text" style="margin-top: 5px" placeholder="Signature over printed name" readonly>
                      </div>

                    </div>

                    <div class="row" style="margin-top: 10px">

                      <div class="sixteen wide field" id="rfa_conformedbydiv">

                        <div class="ui selection dropdown rfaModalDropDown" id="rfa_conformedby">

                          <input type="hidden">
                          <i class="dropdown icon"></i>
                          <div class="default text">Conformed By</div>

                            <div class="menu" id="loadrfa_conformedby">

                            </div>

                          </div>

                        </div>
                      </div>

                  </div>

                  <div class="eight wide column">

                    <div class="row">

                      <div class="field">
                        <label >Acknowledge By:</label>
                        <input class="signaturefield" type="text" placeholder="Signature over printed name" readonly>
                      </div>

                    </div>

                    <!--div class="center aligned row" style="margin-top: 10px">
                      <label style="margin-top: 20px; font-weight: 600" id="rfa_acknowledgeby">DR. MARIO R. OBRA - QM OFFICER</label>
                      <input type ="hidden" id = "acknowledgeby">
                    </div-->
                    

                        <div class="ui selection dropdown rfaModalDropDown" id="rfa_acknowledgeby">

                          <input type="hidden">
                          <i class="dropdown icon"></i>
                          <div class="default text">Acknowledged By</div>

                            <div class="menu" id="loadrfa_acknowledgeby">

                            </div>

                          </div>

                        </div>
                  

                </div>

              </div>

            </form>

            <div class="ui attached segment">
              <!--button class="ui grey button" onclick="saveRFA()">Save</button-->
              <button class="ui grey button" id = "saveRFAPartOneButton">Save</button>
            </div>

          </div>

        </div>

      </section>

      <?php include './modalsinclude.php'; ?>
      <?php include '../modals/yesnomodal.php'; ?>
      <?php include '../modals/notificationmodal.php'; ?>
      <?php include '../modals/notificationusermodal.php'; ?>
      <?php include "./includefooter2.php" ?>

      <!--script src="../public/javascript/homemodulescript.js"></script-->
      <script src="../public/javascript/rfapartone.js"></script>
      <script src="../public/javascript/showmodal.js"></script>
    </body>

  </html>
