<?php
  session_start();
  include "../model/model.php";
  $model = new model();
  $model->connectDatabase();
  if($_SESSION['user']==""){
    header("Location: ../index.php");
  }
  if(isset($_SESSION['notifrfa'])){
    if($_SESSION['notifrfa']!= "initalized"){
      $model->sendNotifRfa($_SESSION['user']);
      $_SESSION['notifrfa']= "initalized";
    }
  }
 ?>

<!DOCTYPE html>
<html>

  <head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <title>RFA Monitoring</title>

    <?php include "./includeheader2.php"; ?>
    <link rel = "stylesheet" type = "text/css" href="../public/stylesheet/manage.css">
  </head>

  <body>

    <header>

      <div class="ui menu"> <!-- TOP HEADER -->
        <div class="item top-header">
          <img class="ui middle aligned tiny image" src="../public/images/logo.png">
          <span class="top-header-span">
            <div>
              Western Mindanao State University
            </div>
            <div>
              Quality Management Office
            </div>
            <div>
              RFA MONITORING SYSTEM
            </div>
          </span>
        </div>
      </div>

      <div class="ui right aligned secondary menu header-item-2"> <!-- BOTTOM HEADER -->

        <div class="right item">

          <span class="header-right-item"><?php echo $model->getAccNameById($_SESSION['user']); ?></span>
          <span class="header-right-item">|</span>
          <span style="cursor: pointer" class="header-right-item"
                onclick="logout()">LOGOUT</span>

        </div>

      </div>

    </header>

    <section>

      <div class="ui grid">

        <div class="four wide column">

          <?php include "./sideNavbar.php"; ?>

        </div>

        <div class="ten wide column"><br/><br/><br/>

          <form class="ui fluid form" onsubmit="return false">

            <div class="ui segments">

              <div class="ui segment" style="background-color: grey; color: white;">
                <p>Register Accounts</p>
              </div>

              <div class="ui segment"> <!-- REGISTER NEW ACCOUNT -->

                <div class="fields"> <!-- NAME / ACCOUNT TYPE -->

                  <div class="nine wide required field">
                    <label>Name</label>
                    <input type="text" placeholder="Family Name, Given Name" id="new_accname">
                  </div>

                  <div class="seven wide required field">
                    <label>Account Type</label>
                    <div class="ui fluid selection dropdown" id="new_acctype">
                      <input type="hidden" name="new_dcategory" id="new_dcategory">
                      <i class="dropdown icon"></i>
                      <div class="default text">Account Type</div>
                      <div class="menu">
                        <?php
                          if($model->getUserType($_SESSION['user'])=="Admin"){
                            echo "<div class='item' data-value='SubAdmin'>Sub Admin</div>
                            <div class='item' data-value='Auditor'>Auditor</div>
                            <div class='item' data-value='Auditee'>Auditee</div>";
                          }elseif($model->getUserType($_SESSION['user'])=="SubAdmin"){
                            echo "<div class='item' data-value='Auditor'>Auditor</div>
                            <div class='item' data-value='Auditee'>Auditee</div>";
                          }
                        ?>

                      </div>
                    </div>
                  </div>

                </div>

                <div class="fields"> <!-- USERNAME AND PASSWORD -->

                  <div class="six wide field"> <!-- USERNAME -->
                    <label style="color:#A52A2A; display:none" id="uncount">Username must contain 8 to 16 characters.</label>
                    <label style="color:#A52A2A; display:none" id="unalrdytakenlabel">Username is already taken!</label>
                    <label id="unlabel">Username<span style="color:red;"> *</span></label>
                    <input type="text" placeholder="Username" id="new_accun">
                  </div>

                  <div class="ten wide field">  <!-- Password One / Password Two -->

                    <label style="color:#A52A2A; display:none" id="pwdontmatchlabel">Password don't match!</label>
                    <label id="pwdlabel">Password<span style="color:red;"> *</span></label>

                    <div class="two fields">

                      <div class="field">
                        <input type="password" placeholder="Password" id="new_accpw1">
                      </div>

                      <div class="field">
                        <input type="password" placeholder="Confirm Password" id="new_accpw2">
                      </div>

                    </div>

                  </div>

                </div>
                <div class='fields'>
                  <div class='six wide required field'>
                    <label>Security Question</label>
                    <div class="ui fluid selection dropdown" id="securityQuestion">
                      <input type="hidden" name="new_dcategory" id="mySecurityQuestion">
                      <i class="dropdown icon"></i>
                      <div class="default text">Security Question</div>
                      <div class="menu">
                          <div class='item' data-value='What is the name of your first pet?'>What is the name of your first pet?</div>
                          <div class='item' data-value="What is your mother's maiden name?">What is your mother's maiden name?</div>
                          <div class='item' data-value='What is the name of your favorite teacher?'>What is the name of your favorite teacher?</div>
                          <div class='item' data-value='Who is your first love?'>Who is your first love?</div>
                      </div>
                    </div>
                  </div>
                  <div class="five wide required field">
                    <label>Answer:</label>
                    <input type="text" placeholder="Answer" id="questionAnswer">
                  </div>
                  <div class="five wide required field">
                    <label>Email:</label>
                    <input type="text" placeholder="email" id="email">
                  </div>
                </div>
                <div class='fields'>
                <div class='six wide required field'>
                  <label>Designation</label>
                  <div class="ui fluid selection dropdown" id="designationDropbox">
                    <input type="hidden" name="new_dcategory" id="mydesignationDropbox">
                    <i class="dropdown icon"></i>
                    <div class="default text">Designation</div>
                      <div class="menu">
                        <?php 
                          $model->loadDesignationsOnDropDown();
                        ?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="field">
                  <button class="ui grey button" id = "registerButton">Register</button>
                  <button class="ui green button" id = "saveButton">Save</button>
                  <button class="ui red button" id = "clearButton">Clear</button>
                </div>

              </div>

              <div class="ui segment" style="background-color: grey; color: white;">
                <p>List Of Accounts</p>
              </div>

              <div class="ui segment">

                <table class="ui celled table">

                  <thead>

                    <tr>
                      <th>Name</th>
                      <th>Username</th>
                      <th>Type</th>
                      <th>Status</th>
                      <th>Deactivate/Activate/Delete</th>
                    </tr>

                  </thead>

                  <tbody id="loadaccountshere">


                  </tbody>

                </table>

              </div>

            </div>

          </form>

        </div>

      </div>

    </section>

    <?php include './modalsinclude.php'; ?>
    <?php include '../modals/yesnomodal.php'; ?>
    <?php include '../modals/notificationmodal.php'; ?>
    <?php include '../modals/notificationusermodal.php'; ?>
    <?php include "./includefooter2.php" ?>
    <script src="../public/javascript/accounts.js"></script>
    <script src="../public/javascript/showmodal.js"></script>
  </body>

</html>
