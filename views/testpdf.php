<?php
  session_start();
  include "../model/model.php";
  $model = new model();
  $model->connectDatabase();
  if($_SESSION['user']==""){
    header("Location: ../index.php");
  }
 ?>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel = "stylesheet" type = "text/css" href="../public/Libraries/semantic.css">
    <link rel = "stylesheet" type = "text/css" href="../public/Libraries/icon.css">
    <link rel = "stylesheet" type = "text/css" href="../public/stylesheet/loader.css">
    <link rel = "stylesheet" type = "text/css" href="../public/stylesheet/generic.css">
    <!--script src="./Libraries/ajax-3.1.1.js"></script-->
    <script src="../public/javascript/jquery-3.1.0.js"></script>
    <script src="../public/Libraries/semantic.js"></script>
    <script src="../public/Libraries/semantic.min.js"></script>
    <style>
      .header{
        margin: 0px;
        padding: 0px;
      }
      .mydiv{
        margin-top: 10px;
      }
    </style>
  </head>
  <body>
    <div class='ui container'>
      <!--div class='ui grid'-->
      <div class='ui grid'>
        <div class='row' style = 'margin:5px; padding-bottom:0px;'>
          <div class='four wide column'>
            <h4 class='header'>Western Mindanao State University</h4>
            <h4 class='header'>QUALITY MANAGEMENT OFFICE</h4>
            <h4 class='header'>Zamboanga City</h4>
          </div>
          <div class='eight wide column'>
          </div>
          <div class='four wide column'>
            <h4 class='header'>
              REQUEST FOR ACTION (RFA)
            </h4>
          </div>
        </div>
        <div class='row' style = 'margin:5px; padding:0px;'>
          <div class='four wide column'>
          </div>
          <div class='eight wide column'>
          </div>
          <div class='four wide column'>
            <label>Date:</label>
            <div class="ui input">
              <input type = 'date' value='<?php echo date("Y-m-d");?>' readonly>
            </div>
          </div>
        </div>
        <div class='row' style = 'margin:5px; padding:0px;'>
          <div class='four wide column'>
            <div class='row'>
              <label>Core Unit:</label>
            </div>
            <div class="ui input">
              <input type = 'text' value='<?php echo $model->getRFAPartOneCoreUnit($_SESSION['RFAPartoneID']);?>' readonly>
            </div>
          </div>
          <div class='four wide column'>
            <div class='row'>
              <label>Process Unit:</label>
            </div>
            <div class="ui input">
              <input type = 'text' value='<?php echo $model->getProcessUnitIDByRFAID($_SESSION['RFAPartoneID']);?>' readonly>
            </div>
          </div>
          <div class='four wide column'>
            <div class='row'>
              <label>Sub Unit:</label>
            </div>
            <div class="ui input">
              <input type = 'text' value='<?php echo $model->getSubUnitIDByRFAID($_SESSION['RFAPartoneID']);?>' readonly>
            </div>
          </div>
          <div class='four wide column'>
            <div class='row'>
              <label>RFA Control No:</label>
          </div>
            <div class="ui input">
              <input type = 'text' value='<?php echo $model->getControlNumberRFA($_SESSION['RFAPartoneID']);?>' readonly>
            </div>
          </div>
        </div>
      </div>
      <div class='ui segments sixteen wide column' style = 'padding:5px; margin-bottom:50px;'>
        <div class="ui segment" style = 'padding:5px; background-color:#cccccc; margin:0px;'>
          <h4>Part I: Problem Identification (To be filled-up by the Requesting Source representative or auditor)</h4>
        </div>
        <div class="ui segment">
          <div class='ui grid'>
          <div class='row'>
            <div class='four wide column'>
              <h4>Type Of Action:</h4>
            </div>
            <div class='six wide column'>
              <div class="ui checkbox">
                <input type="checkbox">
                <label>Corrective Action</label>
              </div>
            </div>
            <div class='six wide column'>
              <div class="ui checkbox">
                <input type="checkbox">
                <label>Preventive Action</label>
              </div>
            </div>
          </div>
          </div>
        </div>
        <div class='ui segment'>
          <div class='ui grid'>
            <div class='row'>
              <div class='four wide column'>
                <h4>Request Source:</h4>
              </div>
              <div class='four wide column'>
                <div class='row'>
                  <div class="ui checkbox">
                    <input type="checkbox">
                    <label>Internal Action</label>
                  </div>
                </div>
                <div class='row'>
                  <div class="ui checkbox">
                    <input type="checkbox">
                    <label>External Action</label>
                  </div>
                </div>
              </div>
              <div class='four wide column'>
                <div class='row'>
                  <div class="ui checkbox">
                    <input type="checkbox">
                    <label>Client Feedback(s)</label>
                  </div>
                </div>
                <div class='row'>
                  <div class="ui checkbox">
                    <input type="checkbox">
                    <label>Management Review</label>
                  </div>
                </div>
              </div>
              <div class='four wide column'>
                <div class='row'>
                  <div class="ui checkbox">
                    <input type="checkbox">
                    <label>Others</label>
                  </div>
                </div>
                <div class='row'>
                  <label>Specify</label>
                  <div class="ui input" style = 'width:100%;'>
                    <input type = 'text' value='' readonly>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class='ui segment'>
          <h4>Non Conformance/Potential Nonconformance</h4>
          <div class='row' style ='height:100px;'>
          </div>
        </div>
        <div class='ui segment'>
          <h4>Description</h4>
          <div class='row' style ='height:100px;'>
          </div>
        </div>
        <div class='ui horizontal segments'>
          <div class='ui segment'>
            <div class='ui grid'>
              <div class='row'>
                <label>Issued By:</label>
              </div>
              <div class="ui input" style = 'width:100%;'>
                <input type = 'text' readonly>
              </div>
              <div class='row'>
                <label>Ephraim Lambarte</label>
              </div>
            </div>
          </div>
          <div class='ui segment'>
            <div class='ui grid'>
              <div class='row'>
                <div class='sixteen wide column'>
                <h4>Risk Level</h4>
                </div>
              </div>
              <div class='row'>
                <div class='five wide column'>
                  <div class="ui checkbox">
                    <input type="checkbox">
                    <label>High</label>
                  </div>
                </div>
                <div class='six wide column'>
                  <div class="ui checkbox">
                    <input type="checkbox">
                    <label>Medium</label>
                  </div>
                </div>
                <div class='five wide column'>
                  <div class="ui checkbox">
                    <input type="checkbox">
                    <label>Low</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class='ui segment'>
            <div class='ui grid'>
              <div class='row'>
                <div class='sixteen wide column'>
                  <h4>Due Date:</h4>
                  <div class="ui input" style = 'width:100%;'>
                    <input type="text" >

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class='ui horizontal segments'>
          <div class='ui segment'>
            <div class='ui grid'>
              <div class='row'>
                <div class='sixteen wide column'>
                  <label>Conformed By:</label>
                </div>
              </div>
              <div class="ui input" style = 'width:100%;'>
                <input type = 'text' readonly>
              </div>
              <div class='row'>
                <div class='sixteen wide column'>
                  <label>Ephraim R. Lambarte</label>
                </div>
              </div>
            </div>
          </div>
          <div class='ui segment'>
            <div class='ui grid'>
              <div class='row'>
                <div class='sixteen wide column'>
                  <label>Acknowledged By:</label>
                </div>
              </div>
              <div class="ui input" style = 'width:100%;'>
                <input type = 'text' readonly>
              </div>
              <div class='row'>
                <div class='sixteen wide column'>
                  <label>Ephraim Lambarte</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="ui segment" style = 'padding:5px; background-color:#cccccc; margin:0px;'>
            <h4>Part II: Evaluation, Analysis and Implementation of Action plan (To be filled-up by the RFA owner)</h4>
        </div>
        <div class="ui segment">
          <h4>Root cause(s)/Possible cause(s)</h4>
          <div class='row' style ='height:100px;'>
          </div>
        </div>
        <div class='ui segment'>
          <h4>Mitigating/Immediate action</h4>
          <div class='row' style ='height:100px;'>
          </div>
        </div>
        <div class='ui horizontal segments'>
          <div class='ui segment'>
            <label>
              Implemented By:
            </label>
          </div>
          <div class='ui segment'>
            <label>
              Date:
            </label>
          </div>
        </div>
        <div class='ui segment'>
          <h4>Permanent Corrective/Preventive Action</h4>
          <div class='row' style ='height:100px;'>
          </div>
        </div>
        <div class='ui horizontal segments'>
          <div class='ui segment'>
            <label>
              Implemented By:
            </label>
          </div>
          <div class='ui segment'>
            <label>
              Date:
            </label>
          </div>
        </div>
        <div class="ui segment" style = 'padding:5px; background-color:#cccccc; margin:0px;'>
            <h4>Part III - FOLLOW-UP on the Implementation of Action (To be filled-up by the Requesting Source or auditor)</h4>
        </div>
        <div class='ui segment'>
          <table class="ui celled padded table">
            <thead>
              <tr>
              <th class='single line'>Date</th>
              <th>Details</th>
              <th>Satisfactory</th>
              <th>No Satisfactory</th>
              <th>Verified By</th>
            </tr></thead>
            <tbody>
              <tr>
                <td>
                  <h2 class="ui center aligned header">A</h2>
                </td>
                <td class="single line">
                  Power Output
                </td>
                <td>
                  <div class="ui star rating" data-rating="3" data-max-rating="3"></div>
                </td>
                <td class="right aligned">
                  80% <br>
                  <a href="#">18 studies</a>
                </td>
                <td>Creatine supplementation is the reference compound for increasing muscular creatine levels; there is variability in this increase, however, with some nonresponders.</td>
              </tr>
              <tr>
                <td>
                  <h2 class="ui center aligned header">A</h2>
                </td>
                <td class="single line">
                  Weight
                </td>
                <td>
                  <div class="ui star rating" data-rating="3" data-max-rating="3"></div>
                </td>
                <td class="right aligned">
                  100% <br>
                  <a href="#">65 studies</a>
                </td>
                <td>Creatine is the reference compound for power improvement, with numbers from one meta-analysis to assess potency</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="ui segment" style = 'padding:5px; background-color:#cccccc; margin:0px;'>
            <h4>Part IV - CLOSE-OUT (To be filled-up by the Unit Head and Quality Management Officer)</h4>
        </div>
        <div class='ui segment'>
          <div class='ui grid'>
            <div class='row'>
              <div class='eight wide column'>
                <center>
                <div class="ui checkbox">
                  <input type="checkbox">
                  <label>Satisfactory</label>
                </div>
              </center>
              </div>
              <div class='eight wide column'>
                <center>
                <div class="ui checkbox">
                  <input type="checkbox">
                  <label>Not Satisfactory(issue new rfa)</label>
                </div>
              </center>
              </div>
            </div>
          </div>
        </div>
        <div class='ui horizontal segments'>
          <div class='ui segment'>
            <div class='ui grid'>
              <div class='row' style = 'height:80px;'>
                <div class='sixteen wide column'>
                  Verified By:
                </div>
              </div>
            </div>
            <div class="ui segment" style = 'padding:0px; margin:0px;'>
                <label>Ephraim Lambarte</label>
            </div>
          </div>
          <div class='ui segment'>
            <div class='ui grid'>
              <div class='row' style = 'height:80px;'>
                <div class='sixteen wide column'>
                  Remarks:
                </div>
              </div>
            </div>
            <div class="ui segment" style = 'padding:0px; margin:0px;'>
                <label>Date:</label>
            </div>
          </div>
        </div>
        <div class='ui horizontal segments'>
          <div class='ui segment'>
            <div class='ui grid'>
              <div class='row' style = 'height:80px;'>
                <div class='sixteen wide column'>
                  Validated By:
                </div>
              </div>
            </div>
            <div class="ui segment" style = 'padding:0px; margin:0px;'>
                <label>Ephraim Lambarte</label>
            </div>
          </div>
          <div class='ui segment'>
            <div class='ui grid'>
              <div class='row' style = 'height:80px;'>
                <div class='sixteen wide column'>
                  Remarks:
                </div>
              </div>
            </div>
            <div class="ui segment" style = 'padding:0px; margin:0px;'>
                <label>Date:</label>
            </div>
          </div>
        </div>
      </div>
      <!--/div-->
    </div>
  </body>
</html>
