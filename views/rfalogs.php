<?php
  session_start();
  include "../model/model.php";
  $model = new model();
  $model->connectDatabase();
  if($_SESSION['user']==""){
    header("Location: ../index.php");
  }
  if(isset($_SESSION['notifrfa'])){
    if($_SESSION['notifrfa']!= "initalized"){
      $model->sendNotifRfa($_SESSION['user']);
      $_SESSION['notifrfa']= "initalized";
    }
  }
 ?>

<!DOCTYPE html>
<html>

  <head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <title>RFA Monitoring</title>

      <?php include "./includeheader2.php"; ?>
      <link rel="stylesheet" href="../public/bargraph/css/style.css">
      <!--link rel = "stylesheet" type = "text/css" href="../public/stylesheet/bootstrap.css"-->
      <link rel = "stylesheet" type = "text/css" href="../public/bargraph/css/bar.css">
      <link rel = "stylesheet" type = "text/css" href="../public/stylesheet/rfalogs.css">
  </head>

  <body>

    <header>

      <div class="ui menu"> <!-- TOP HEADER -->
        <div class="item top-header">
          <img class="ui middle aligned tiny image" src="../public/images/logo.png">
          <span class="top-header-span"><div>
              Western Mindanao State University
            </div>
            <div>
              Quality Management Office
            </div>
            <div>
              RFA MONITORING SYSTEM
            </div>
            </span>
        </div>
      </div>

      <div class="ui right aligned secondary menu header-item-2"> <!-- BOTTOM HEADER -->

        <div class="right item">

          <span class="header-right-item"><?php echo $model->getAccNameById($_SESSION['user']); ?></span>
          <span class="header-right-item">|</span>
          <span style="cursor: pointer" class="header-right-item"
                onclick="logout()">LOGOUT</span>

        </div>

      </div>

    </header>

    <section>

      <div class="ui grid">

        <div class="three wide column">

          <?php include "./sideNavbar.php"; ?>

        </div>

        <div class="twelve wide column" ><br/><br/><br/>
          <div class="sixteen wide column">
            <div class='ui input' style="width:100%;" data-content = 'search'>
              <input type = 'text' id = 'searchRfaLog' placeholder="Search">
            </div>
          </div>
          <form class="ui fluid form" onsubmit="return false">

            <div class="ui segments">

              <div class="ui segment" style="background-color: grey; color: white;">
                <div class='row'>
                  <div class='ten wide column'>
                    
                  </div>
                  <div class='two wide column'>
                    <button class="ui button" id = 'statisticSummaryButton'>Statistic Summary</button>
                      <button class="ui button" id = 'viewRFALogsButtonPDF'>View PDF</button>
                  </div>
                </div>
              </div>

              <div class="ui segment" id = "loadrfalogshere">


              </div>
              <div id = 'mypagination'>
                <center><ul id = 'pagination'>
                </ul></center>
              </div>
            </div>

          </form>

        </div>

      </div>

    </section>

    <?php include './modalsinclude.php'; ?>
    <div class="ui small modal" id = "barGraphModal" style = 'width:80%;'>
      <i class="close icon"></i>
      <div class="header">
        Statistics
      </div>
      <div class="image content" style = 'padding-left:50px;'>
        <div id = 'curve_chart' style="width: 900px; height: 500px"></div>

      </div>
      <div class="actions">

        <div class="ui black deny button">
          Close
        </div>
      </div>
    </div>
    <?php include '../modals/yesnomodal.php'; ?>
    <?php include '../modals/editrfapartonemodal.php'; ?>
    <?php include '../modals/notificationmodal.php'; ?>
    <?php include '../modals/notificationusermodal.php'; ?>
    <?php include '../modals/rfaparttwomodal.php'; ?>
    <?php include '../modals/rfapartthreemodal.php'; ?>
    <?php include '../modals/rfapartfourmodal.php'; ?>
    <?php include '../modals/viewpdfmodal.php'; ?>
    <?php include "./includefooter2.php" ?>
    <!--script src="../public/bargraph/js/graph.js"></script-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="../public/javascript/jquery.twbsPagination.js"></script>
    <script src="../public/javascript/rfalogs.js"></script>
    <script src="../public/javascript/showmodal.js"></script>

  </body>


</html>
