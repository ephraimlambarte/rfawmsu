<div class="ui grey inverted vertical menu sidebar-right-side-menu">

  <a href="./rfalogs.php" >
    <div class="item sidebar-item item-onhover" id = "rfalogs">
      <i class="large archive icon sidebar-icon"></i>
      <div class="sidebar-header">RFA Logs</div>
    </div>
  </a>

  <a href="./rfapartone.php" > <!-- RFA MODULE -->
    <div class="item sidebar-item item-onhover" id = "rfaissue">
      <i class="large file text icon sidebar-icon"></i>
      <div class="sidebar-header">RFA Issue</div>
    </div>
  </a>



  <?php if($model->getUserType($_SESSION['user'])=='Admin' || $model->getUserType($_SESSION['user'])=='SubAdmin') { ?>
    <a href="./accounts.php" > <!-- Account MODULE -->
      <div class="item sidebar-item item-onhover" id = "manageaccounts">
        <i class="large user text icon sidebar-icon"></i>
        <div class="sidebar-header">Manage Accounts</div>
      </div>
    </a>
  <a onclick="showCUModal()" style="cursor: pointer"> <!-- Core Unit MODULE -->
    <div class="item sidebar-item item-onhover">
      <i class="large setting icon sidebar-icon"></i>
      <div class="sidebar-header">Manage Core Unit</div>
    </div>
  </a>

  <a onclick="showPUModal()" style="cursor: pointer"> <!-- Process Unit MODULE -->
    <div class="item sidebar-item item-onhover">
      <i class="large setting icon sidebar-icon"></i>
      <div class="sidebar-header">Manage Process Unit</div>
    </div>
  </a>

  <a onclick="showSUModal()" style="cursor: pointer"> <!-- Sub Unit MODULE -->
    <div class="item sidebar-item item-onhover">
      <i class="large setting icon sidebar-icon"></i>
      <div class="sidebar-header">Manage Sub Unit</div>
    </div>
  </a>
  <a onclick="showDesignationModal()" style="cursor: pointer"> 
    <div class="item sidebar-item item-onhover">
      <i class="large setting icon sidebar-icon"></i>
      <div class="sidebar-header">Manage Designations</div>
    </div>
  </a>
  <?php } ?>
  <a onclick="showNotsModal()" style="cursor: pointer"> <!-- Core Unit MODULE -->
    <div class="item sidebar-item item-onhover">
      <i class="large setting icon sidebar-icon"></i>
      <div class="sidebar-header">Notification
        <?php if($model->getNotificationNumber($_SESSION['user'])>0) {?>
        <span class='.notification'id = 'mynotification'>
          <?php echo $model->getNotificationNumber($_SESSION['user']); ?>
        </span>
        <?php } ?>
      </div>
    </div>
  </a>
  <a href = './about.php' style="cursor: pointer"> 
    <div class="item sidebar-item item-onhover">
      <i class="large info circle icon sidebar-icon"></i>
      <div class="sidebar-header">About</div>
    </div>
  </a>
</div>
