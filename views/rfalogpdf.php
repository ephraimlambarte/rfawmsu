<?php
  include '../dompdf/autoload.inc.php';
  session_start();
  include '../model/model.php';
  $model = new model();
  $model->connectDatabase();
  // reference the Dompdf namespace
  use Dompdf\Dompdf;

  // instantiate and use the dompdf class
  $dompdf = new Dompdf();

  $html= "<html>
            <head>
              <style>@page{
                  margin:20px 20px;
                }
                body {
                    font-family: Arial;
                    font-size: 10px;
                  }
                .mydatediv{
                  display:inline-block;
                  width:30%;
                }
                #datediv{
                  margin-left:100px;
                }
                .myinput {
                  border-right:none;
                  border-left:none;
                  border-top:none;
                }
                #firstdate{
                  width:140px;
                }
                .unitdiv{
                  width:70%;
                  display:inline-block;
                }
                .seconddiv{

                  height:50px;
                }
                .controldiv{
                  width:30%;
                  display:inline-block;
                  padding-left:30px;
                }
                .childunitdiv{
                  width:32%;
                  display:inline-block;
                  padding-top:30px;
                }
                .blockdiv{
                  display:block;
                }
                .smallinput{
                  width:100px;
                }
                .boxdiv{
                  border-style:solid;
                  border-width:1px;
                  border-color:black;
                  padding:0px;
                }
                .linediv{
                  border-bottom-style:solid;
                  border-bottom-width:1px;
                  border-bottom-color:black;
                }
                .genericdiv{
                  padding:0px;
                }
                .genericheader{
                  padding:0px;
                  margin:0px;
                }
                .partoneheader{
                  background-color:#cccccc;
                }
                .threediv{
                  width:30%;
                  float:left;
                  margin-top:5px;
                  margin-bottom:10px;
                }
                .actionTypeDiv{
                  width:24%;
                  display:inline-block;
                  padding-top:20px;
                }
                .issuedbydiv{
                  width:49%;
                  display:inline-block;
                  margin-top:5px;
                  border-right-style:solid;
                  border-right-width:1px;
                  border-right-color:black;
                }
                .issuedbycontainer{
                  height:50px;
                  border-bottom-style:solid;
                  border-bottom-width:1px;
                  border-bottom-color:black;
                }
                .risklevel{
                  display:inline-block;
                  width:49%;
                }
                td{
                  border-style:solid;
                  border-width:1px;
                  margin:0px;
                  cellspacing:0;
                  text-align:center;
                }
                th{
                  border-style:solid;
                  border-width:1px;
                  margin:0px;
                  text-align:center;
                }
              </style>
            </head>
			
            <body>
              <div>
                <div style = 'width:80%; display:inline-block;'>
                  <h4 class='header' style='color:#083c91; margin:0px;'>Western Mindanao State University</h4>
                  <h4 class='header' style='color:#083c91; margin:0px;'>QUALITY MANAGEMENT OFFICE</h4>
                  <h4 class='header' style='color:#083c91; margin:0px;'>Zamboanga City</h4>
                </div>
                <div style = 'width:20%; display:inline-block;'>
                    <h4 class='header' style='color:#083c91; margin:0px;'>REQUEST FOR ACTION(RFA)</h4>
                </div>
              </div>
              <div>
                <div class='mydatediv'>
                </div>
                <div class='mydatediv'>
                </div>
                <div class='mydatediv'>
                  <label id ='datediv'>Date:</label>
                  <input type = 'text' value = '".$model->getDate()."' class='myinput' id = 'firstdate'>
                </div>
              </div>
              <div class='seconddiv'>
                <div class='unitdiv'>
                  <div class='childunitdiv'>
                    <div class='blockdiv'>
                      <label>Core Unit:</label>
                    </div>
                    <div class='blockdiv'>
                      <input type = 'text' value = '".$model->getRFAPartOneCoreUnit($_SESSION['RFAPartoneID'])."' class='myinput' id = 'firstdate'>
                    </div>
                  </div>
                  <div class='childunitdiv'>
                    <div class='blockdiv'>
                      <label>Process Unit:</label>
                    </div>
                    <div class='blockdiv'>
                      <input type = 'text' value = '".$model->getProcessUnitIDByRFAID($_SESSION['RFAPartoneID'])."' class='myinput' id = 'firstdate'>
                    </div>
                  </div>
                  <div class='childunitdiv'>
                    <div class='blockdiv'>
                      <label>Sub Unit:</label>
                    </div>
                    <div class='blockdiv'>
                      <input type = 'text' value = '".$model->getSubUnitIDByRFAID($_SESSION['RFAPartoneID'])."' class='myinput' id = 'firstdate'>
                    </div>
                  </div>
                </div>
                <div class='controldiv'>
                  <div class='blockdiv'>
                    <label>RFA Control No:</label>
                  </div>
                  <div class='blockdiv'>
                    <input type = 'text' value = '".$model->getControlNumberRFA($_SESSION['RFAPartoneID'])."' class='myinput smallinput' id = 'firstdate'>
                  </div>
                </div>
              </div>
              <div class='boxdiv'>
                <div class='linediv genericdiv'>
                  <i><h4 class='genericheader partoneheader'>Part I: Problem Identification (To be filled-up by the Requesting Source representative or auditor)</h4></i>
                </div>
                <div class='linediv' style = 'height:25px; padding:0px;'>
                  <div class='threediv'>
                    <h4 style = 'padding:0px; margin:0px; margin-left:10px;margin-bottom:5px;'>Type Of Action:</h4>
                  </div>
                  <div class='threediv'>
                    <div style= 'display:inline-block;'>";
                      if($model->getActionType($_SESSION['RFAPartoneID'])=="Corrective Action"){
                        $html .= "<input type='checkbox'
                                value = 'Corrective Action' checked style = 'margin:0px; padding:0px;' id = 'correctiveAction'>";
                      }else{
                        $html .= "<input type='checkbox'
                                value = 'Corrective Action' style = 'margin:0px; padding:0px;' id = 'correctiveAction'>";
                      }
                      $html .= "
                    </div>
                    <div style = 'display:inline-block; padding-bottom:5px;'>  
                      <label for = 'correctiveAction'>- CORRECTIVE ACTION</label>
                    </div>    
                  </div>
                  <div class='threediv'>
                    <div style= 'display:inline-block;'>";
                      if($model->getActionType($_SESSION['RFAPartoneID'])=="Preventive Action"){
                        $html .= "<input type='checkbox'
                                value = 'Corrective Action' checked style = 'margin:0px; padding:0px;'>";
                      }else{
                        $html .= "<input type='checkbox'
                                value = 'Corrective Action' style = 'margin:0px; padding:0px;'>";
                      }
                        $html .= "
                    </div>  
                    <div style = 'display:inline-block; padding-bottom:5px;'>  
                      <label style = 'margin:0px; padding:0px;'>- PREVENTIVE ACTION</label>
                    </div>  
                  </div>
                </div>
                <div class='linediv' style='height:60px;'>
                  <div class='actionTypeDiv' style = 'height:50px;'>
                    <h4 style = 'padding:0px; margin:0px;  margin-left:10px;'>Request Source:</h4>
                  </div>
                  <div class='actionTypeDiv'>
                    <div>
                      <div style= 'display:inline-block;'>";
                        if($model->getRequestSource($_SESSION['RFAPartoneID'])=="Internal Audit"){
                          $html .= "<input type='checkbox'
                                  value = 'Corrective Action' checked style = 'margin:0px; padding:0px;'>";
                        }else{
                          $html .= "<input type='checkbox'
                                  value = 'Corrective Action' style = 'margin:0px; padding:0px;'>";
                        }
                          $html .= "
                      </div>
                      <div style = 'display:inline-block; padding-bottom:5px;'>      
                        <label style = 'margin:0px; padding:0px;'>- INTERNAL AUDIT</label>
                      </div>
                    </div>
                    <div>
                      <div style= 'display:inline-block;'>";
                        if($model->getRequestSource($_SESSION['RFAPartoneID'])=="External Audit"){
                          $html .= "<input type='checkbox'
                                  value = 'Corrective Action' checked style = 'margin:0px; padding:0px;'>";
                        }else{
                          $html .= "<input type='checkbox'
                                  value = 'Corrective Action' style = 'margin:0px; padding:0px;'>";
                        }
                          $html .= "
                      </div>
                      <div style = 'display:inline-block; padding-bottom:5px;'>  
                        <label style = 'margin:0px; padding:0px;'>- EXTERNAL AUDIT</label>
                      </div>
                    </div>
                  </div>
                  <div class='actionTypeDiv'>
                    <div>
                      <div style= 'display:inline-block;'>";
                        if($model->getRequestSource($_SESSION['RFAPartoneID'])=="Client Feedback"){
                          $html .= "<input type='checkbox'
                                  value = 'Corrective Action' checked style = 'margin:0px; padding:0px;'>";
                        }else{
                          $html .= "<input type='checkbox'
                                  value = 'Corrective Action' style = 'margin:0px; padding:0px;'>";
                        }
                          $html .= "
                      </div>
                      <div style = 'display:inline-block; padding-bottom:5px;'> 
                        <label style = 'margin:0px; padding:0px;'>- CLIENT FEEDBACK(s)</label>
                      </div>
                    </div>
                    <div>
                      <div style= 'display:inline-block;'>";
                        if($model->getRequestSource($_SESSION['RFAPartoneID'])=="Management Review"){
                          $html .= "<input type='checkbox'
                                  value = 'Corrective Action' checked style = 'margin:0px; padding:0px;'>";
                        }else{
                          $html .= "<input type='checkbox'
                                  value = 'Corrective Action' style = 'margin:0px; padding:0px;'>";
                        }
                          $html .= "
                      </div>
                      <div style = 'display:inline-block; padding-bottom:5px;'> 
                        <label style = 'margin:0px; padding:0px;'>- MANAGEMENT REVIEW</label>
                      </div>
                    </div>
                  </div>
                  <div class='actionTypeDiv' style = 'padding:0px;'>
                    <div style = 'margin:0px; padding:0px;'>
                    <div style= 'display:inline-block;'>";
                      if($model->getRequestSource($_SESSION['RFAPartoneID'])=="specify"){
                        $html .= "<input type='checkbox'
                                value = 'Corrective Action' checked style = 'margin:0px; padding:0px;'>";
                      }else{
                        $html .= "<input type='checkbox'
                                value = 'Corrective Action' style = 'margin:0px; padding:0px;'>";
                      }
                        $html .= "
                    </div>
                    <div style = 'display:inline-block; padding-bottom:5px;'>
                      <label style = 'margin:0px; padding:0px;'>- OTHERS</label>
                    </div>
                    </div>
                    <div style ='margin-top:0px;'>
                      <input type = 'text' class='myinput' value = 'Specify:".$model->getRequestSpecified($_SESSION['RFAPartoneID'])."'>
                    </div>
                  </div>
                </div>
                <div class='linediv' style = 'height:40px; padding-left:10px;'>
                  <u><label><b>Nonconformance / Potential Nonconformance:</b></label></u>
                  <p>".$model->getNonConformity($_SESSION['RFAPartoneID'])."</p>
                </div>
                <div class='linediv' style = 'height:40px; padding-left:10px;'>
                  <u><label><b>Description:</b></label></u>
                  <p>".$model->getDescription($_SESSION['RFAPartoneID'])."</p>
                </div>
                <div class='linediv' style = 'height:80px; padding-top:10px;'>
                  <div class='issuedbydiv'>
                    <div class='issuedbycontainer' style = 'margin-top:20px; padding-left:10px;'>
                      <label style = 'margin-top:0px; padding:0px;'><b style = 'margin:0px; padding:0px;'>Issued By:</b></label>
                    </div>
                    <center>".$model->getAccNameById($model->getIssuedBy($_SESSION['RFAPartoneID']))." ".$model->getDesignationOfUser($model->getIssuedBy($_SESSION['RFAPartoneID']))."</center>
                  </div>
                  <div class='issuedbydiv' style ='border-right-style:none; padding:0px;'>
                    <div class='issuedbycontainer'>
                      <div class='risklevel' style ='border-right-style:solid;border-right-width:1px; height:65px;'>
                        <center style ='border-bottom-style:solid;border-bottom-width:1px; margin-bottom:10px;'>RISK LEVEL</center>";
                        if($model->getRiskLevel($_SESSION['RFAPartoneID'])=="Medium Risk"){
                          $html.="
                          
                            <div style= 'margin:0px; padding:0px; padding-left:20px;'>
                              <div style= 'display:inline-block; margin:0px; padding:0px;'>
                                <div style= 'display:inline-block;'>
                                  <input type = 'checkbox'>
                                </div>
                                <div style= 'display:inline-block; padding-bottom:5px;'>
                                  <label>-H</label>
                                </div>
                              </div>
                              <div style= 'display:inline-block; margin:0px; padding:0px;'>
                                <div style= 'display:inline-block;'>
                                  <input type = 'checkbox' checked>
                                </div>
                                <div style= 'display:inline-block;  padding-bottom:5px;'>
                                  <label>-M</label>
                                </div>
                              </div>
                              <div style= 'display:inline-block; margin:0px; padding:0px;'>
                                <div style= 'display:inline-block;'>
                                  <input type = 'checkbox'>
                                </div>
                                <div style= 'display:inline-block;  padding-bottom:5px;'>
                                  <label>-L</label>
                                </div>
                              </div>
                              
                            </div>";
                        }elseif ($model->getRiskLevel($_SESSION['RFAPartoneID'])=="High Risk") {
                          $html.="
                          
                            <div style= 'margin:0px; padding:0px; padding-left:20px;'>
                              <div style= 'display:inline-block; margin:0px; padding:0px;'>
                                <div style= 'display:inline-block;'>
                                  <input type = 'checkbox' checked>
                                </div>
                                <div style= 'display:inline-block; padding-bottom:5px;'>
                                  <label>-H</label>
                                </div>
                              </div>
                              <div style= 'display:inline-block; margin:0px; padding:0px;'>
                                <div style= 'display:inline-block;'>
                                  <input type = 'checkbox'>
                                </div>
                                <div style= 'display:inline-block;  padding-bottom:5px;'>
                                  <label>-M</label>
                                </div>
                              </div>
                              <div style= 'display:inline-block; margin:0px; padding:0px;'>
                                <div style= 'display:inline-block;'>
                                  <input type = 'checkbox'>
                                </div>
                                <div style= 'display:inline-block;  padding-bottom:5px;'>
                                  <label>-L</label>
                                </div>
                              </div>
                              
                            </div>
                          ";
                        }else {
                          $html.=" <div style= 'margin:0px; padding:0px; padding-left:20px;'>
                            <div style= 'display:inline-block; margin:0px; padding:0px;'>
                              <div style= 'display:inline-block;'>
                                <input type = 'checkbox'>
                              </div>
                              <div style= 'display:inline-block; padding-bottom:5px;'>
                                <label>-H</label>
                              </div>
                            </div>
                            <div style= 'display:inline-block; margin:0px; padding:0px;'>
                              <div style= 'display:inline-block;'>
                                <input type = 'checkbox'>
                              </div>
                              <div style= 'display:inline-block;  padding-bottom:5px;'>
                                <label>-M</label>
                              </div>
                            </div>
                            <div style= 'display:inline-block; margin:0px; padding:0px;'>
                              <div style= 'display:inline-block;'>
                                <input type = 'checkbox' checked>
                              </div>
                              <div style= 'display:inline-block;  padding-bottom:5px;'>
                                <label>-L</label>
                              </div>
                            </div>
                          </div>";
                        }

                      $html .= "</div>
                      <div class='risklevel' style= 'height:65px;'>
                        <center style ='border-bottom-style:solid;border-bottom-width:1px; margin-bottom:21px;'>DUE DATE:</center>
                        <center<span>".$model->getDueDate($_SESSION['RFAPartoneID'])."</span></center>
                      </div>
                    </div>
                    <center style = 'background-color:#cccccc;'>To: Quality Management Office</center>
                  </div>
                </div>
                <div class='linediv' style = 'height:60px; padding-top:0px;'>
                  <div class='issuedbydiv'>
                    <div class='issuedbycontainer' style = 'margin-top:10px;'>
                      <label style = 'margin-top:0px; padding:0px;'><b style = 'margin:0px; padding:0px;'>Conformed By:</b></label>
                    </div>
                      <center>".$model->getAccNameById($model->getConformedBy($_SESSION['RFAPartoneID']))." ".$model->getDesignationOfUser($model->getConformedBy($_SESSION['RFAPartoneID']))."</center>
                  </div>
                  <div class='issuedbydiv'  style ='border-right-style:none; padding-bottom:5px;'>
                    <div class='issuedbycontainer' style ='border-right-style:none;'>
                      <label style = 'margin-top:0px; padding:0px;'><b style = 'margin:0px; padding:0px;'>Acknowledged By:</b></label>
                    </div>
                    <center>".$model->getAccNameById($model->getAcknowledgedBy($_SESSION['RFAPartoneID']))." ".$model->getDesignationOfUser($model->getAcknowledgedBy($_SESSION['RFAPartoneID']))."</center>
                  </div>
                </div>
                <div class='linediv genericdiv'>
                  <i><h4 class='genericheader partoneheader'>Part II: Evaluation, Analysis and Implementation of Action plan (To be filled-up by the RFA owner)</h4></i>
                </div>
                <div class='linediv' style = 'height:50px;'>
                  <u><b>Root Cause(s)/Possible Cause(s):</b></u>
                  <p>".$model->getDataRfaPartTwo($_SESSION['RFAPartoneID'], 'rootcauses')."</p>
                </div>
                <div class='linediv' style = 'height:50px;'>
                  <u><b>Mitigating/Immediate Action:</b></u>
                  <p>".$model->getDataRfaPartTwo($_SESSION['RFAPartoneID'], 'immediateaction')."</p>
                </div>
                <div class='linediv' style = 'height:12px;'>
                  <span style = 'padding-right:400px; border-right-style:solid; border-right-width:1px;'>Implemented By:".$model->getDataRfaPartTwo($_SESSION['RFAPartoneID'], 'implementedby1')."</span><span>Date:".$model->getDataRfaPartTwo($_SESSION['RFAPartoneID'], 'date1')."</span>
                </div>
                <div class='linediv' style = 'height:50px;'>
                  <u><b>Permanent Corrective/Preventive Action:</b><u/>
                  <p>".$model->getDataRfaPartTwo($_SESSION['RFAPartoneID'], 'preventiveaction')."</p>
                </div>
                <div class='linediv' style = 'height:12px;'>
                  <span style = 'padding-right:400px; border-right-style:solid; border-right-width:1px;'>Implemented By:".$model->getDataRfaPartTwo($_SESSION['RFAPartoneID'], 'implementedby2')."</span><span>Date:".$model->getDataRfaPartTwo($_SESSION['RFAPartoneID'], 'date2')."</span>
                </div>
                <div class='linediv genericdiv'>
                  <i><h4 class='genericheader partoneheader'>Part III - FOLLOW-UP on the Implementation of Action (To be filled-up by the Requesting Source or auditor)</h4></i>
                </div>
                <div class='linediv'>
                  <table style='width:100%; border-spacing:0'>
                    <thead>
                      <tr>
                        <th width='80'>Date</th>
                        <th width='200'>Details</th>
                        <th>Satisfactory</th>
                        <th>Not Satisfactory</th>
                        <th width='100'>Verified By</th>
                      </tr>
                    </thead>
                    <tbody>";
            $html.=$model->displayTablePdfPartThree($_SESSION['RFAPartoneID']);
            if($model->displayTablePdfPartThree($_SESSION['RFAPartoneID'])==""){
              $html.="
              <tr>
                <td width='80' height='10'></td>
                <td width='200'></td>
                <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                <td width='100'></td>
              </tr>
              <tr>
                <td width='80' height='10'></td>
                <td width='200'></td>
                <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                <td width='100'></td>
              </tr>
              <tr>
                <td width='80' height='10'></td>
                <td width='200'></td>
                <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                <td width='100'></td>
              </tr>
              <tr>
                <td width='80' height='10'></td>
                <td width='200'></td>
                <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                <td width='100'></td>
              </tr>
              <tr>
                <td width='80' height='10'></td>
                <td width='200'></td>
                <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                <td width='100'></td>
              </tr>
              ";
            }
            //asas
            //
            $html.="</tbody>
                  </table>
                </div>
              </div>
              <div class='boxdiv'>
                <div class='linediv genericdiv'>
                  <i><h4 class='genericheader partoneheader'>Part IV - CLOSE-OUT (To be filled-up by the Unit Head and Quality Management Officer)</h4></i>
                </div>
                <div class='linediv' style = ' padding-top: 10px; height: 30px; padding-left:10px;'>
                    <div style= 'display:inline-block; width:49%;'>
                      <center>";
                      if($model->checkIfSatisfactory($_SESSION['RFAPartoneID'])==true){
                        $html .="
                        <div>
                          <div style = 'display:inline-block;'>
                            <input type = 'checkbox' checked>
                          </div>
                          <div style = 'display:inline-block; padding-bottom:5px;'>
                            <label>- SATISFACTORY</label>
                          </div>
                        </div>";
                      }else{
                        $html .="<div>
                        <div style = 'display:inline-block;'>
                          <input type = 'checkbox'>
                        </div>
                        <div style = 'display:inline-block; padding-bottom:5px;'>
                          <label>- SATISFACTORY</label>
                        </div>
                      </div>";
                      }

                      $html .="</center>
                    </div>
                    <div style= 'display:inline-block; width:49%;'>
                      <center>";
                      if($model->checkIfSatisfactory($_SESSION['RFAPartoneID'])==true){
                        $html .="<div>
                        <div style = 'display:inline-block;'>
                          <input type = 'checkbox'>
                        </div>
                        <div style = 'display:inline-block; padding-bottom:5px;'>
                          <label>- NOT SATISFACTORY</label>
                        </div>
                      </div>";
                      }else{
                        $html .="<div>
                        <div style = 'display:inline-block;'>
                          <input type = 'checkbox' checked>
                        </div>
                        <div style = 'display:inline-block; padding-bottom:5px;'>
                          <label>- NOT SATISFACTORY</label>
                        </div>
                      </div>";
                      }

                      $html .="</center>
                    </div>
                </div>
                <div class='linediv' style = 'height:69px; padding-top:10px;'>
                  <div class='issuedbydiv'>
                    <div class='issuedbycontainer' style = 'margin-top:10px;'>
                      <label style = 'margin-top:0px; paddingpadding:0px;'><b style = 'margin:0px; padding:0px;'>Verified By:</b></label>
                    </div>
                      <div style='height:12px;'><center>".$model->getDataRFAPartFour($_SESSION['RFAPartoneID'], 'verifiedby').$model->getDesignationOfUser($model->getIssuedBy($_SESSION['RFAPartoneID']))."</center></div>
                  </div>
                  <div class='issuedbydiv'  style ='border-right-style:none;'>
                    <div class='issuedbycontainer' style ='border-right-style:none;'>
                      <label style = 'margin-top:0px; padding:0px;'><b style = 'margin:0px; padding:0px;'>REMARKS:</b></label>
                      <p>".$model->getDataRFAPartFour($_SESSION['RFAPartoneID'], 'remarks1')."</p>
                    </div>
                    <b>DATE:</b>".$model->getDataRFAPartFour($_SESSION['RFAPartoneID'], 'date1')."
                    </div>
                </div>
                <div class='linediv' style = 'height:69px; padding-top:10px;'>
                  <div class='issuedbydiv'>
                    <div class='issuedbycontainer' style = 'margin-top:10px;'>
                      <label style = 'margin-top:0px; padding:0px;'><b style = 'margin:0px; padding:0px;'>Validated By:</b></label>
                    </div>
                      <div style='height:12px;'><center>".$model->getDataRFAPartFour($_SESSION['RFAPartoneID'], 'validatedby')."</center></div>
                  </div>
                  <div class='issuedbydiv'  style ='border-right-style:none;'>
                    <div class='issuedbycontainer' style ='border-right-style:none;'>
                      <label style = 'margin-top:0px; padding:0px;'><b style = 'margin:0px; padding:0px;'>REMARKS:</b></label>
                      <p>".$model->getDataRFAPartFour($_SESSION['RFAPartoneID'], 'remarks2')."</p>
                    </div>
                    <b>DATE:</b>".$model->getDataRFAPartFour($_SESSION['RFAPartoneID'], 'date2')."
                    </div>
                </div>
              </div>
              <div style = ' display:block;'>
              <div>
                <div style = 'padding-top:10px; '>
                  <label>
                    <b style='font-size:8px;'>Risk Level:</b>
                  </label>
                </div>
              </div>
              <div>
                <div style = 'width:32%; float:left; font-size:8px'>
                  <div>
                    <label>
                      <i><b>•H-High:</b></i>
                    </label>
                  </div>
                  <div>
                    <i><label>
                    condition where risk is identified as having a high probability of occurrence and the 
                    consequence would have significant impact on cost, schedule, and/or processes.
                    </label></i>
                  </div>
                </div>
                <div style = 'width:32%; float:left;font-size:8px'>
                  <div>
                    <label>
                      <i><b>•M-Medium:</b></i>
                    </label>
                  </div>
                  <div>
                    <i><label>
                    condition where risk is identified as one having less major
                    that could possibly affect business objectives, cost, or schedule.  
                    The probability of occurrence is high enough to require 
                    close control of all contributing factors.                      
                    </label></i>
                  </div>
                </div>
                <div style = ' width:32%; float:left;font-size:8px'>
                  <div>
                    <label>
                      <i><b>•L-Low:</b></i>
                    </label>
                  </div>
                  <div>
                    <i><label>
                    condition where risk is identified as having minor effects 
                    on business objectives; the probability of occurrence 
                    is sufficiently low to cause only minor concern.
                    </label></i>
                  </div>
                </div>
              </div>
            </div>
            <div style = ' padding-top:120px;'>
              <div style = 'float:left; '>
                <label>
                WMSU-QMO-FR-018.01
                </label>
              </div>
              <div style = 'float:right; '>
                <label>
                Effectivity Date: 
                20-APR-2017
                </label>
              </div>
            </div>
            </body>
          ";

  $dompdf->loadHtml($html);
  // (Optional) Setup the paper size and orientation
  //$dompdf->setPaper('A4', 'portrait');
  $dompdf->setPaper(array(0,0,612,936), 'portrait');

  // Render the HTML as PDF
  $dompdf->render();

  // Output the generated PDF to Browser
  $dompdf->stream('RFA', array('Attachment'=>0));
?>
