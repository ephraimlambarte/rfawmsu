<?php
  include '../dompdf/autoload.inc.php';
  session_start();
  include '../model/model.php';
  $model = new model();
  $model->connectDatabase();
  // reference the Dompdf namespace
  use Dompdf\Dompdf;

  // instantiate and use the dompdf class
  $dompdf = new Dompdf();

  $html = "
          <html>
            <head>
              <style>
              body {
                  font-family: Arial;
                }
                .header{
                  margin:0px;
                }
                body{
                  margin:0px;
                  padding:0px;
                }
                td{
                  border-style:solid;
                  border-width:1px;
                  margin:0px;
                  cellspacing:0;
                  text-align:center;
                }
                tr{
                  page-break-inside:avoid;
                  page-break-after:auto;
                }
                @page { margin-top: 90px; }
                th{
                  border-style:solid;
                  border-width:1px;
                  margin:0px;
                  background-color:#D3D3D3;
                }
                .page{ page-break-after: always; 
                  }
                .page:last-child { page-break-after: never;}
              </style>
            </head>
            <body>
			
              <header style = 'position:fixed; top:-60px; left:0px; right:0px; height: 60px;'>
                <div style = 'width:80%; display:inline-block;'>
                  <h4 class='header' style='color:#083c91;'>Western Mindanao State University</h4>
                  <h4 class='header' style='color:#083c91;'>Quality Management Office</h4>
                  <h4 class='header' style='color:#083c91;'>Zamboanga City</h4>
                </div>
                <div style = 'width:20%; display:inline-block;'>
                    <h4 class='header' style='color:#083c91;'>RFA MONITORING LOG</h4>
                    <h4 class='header' style='color:#083c91;'>YEAR:".$model->getYear()."</h4>
                </div>
              </header>
              <footer style = 'position:fixed; bottom:0px; 
              bottom: 0px;
              left: 0px;
              right: 0px;'>
                <div style = 'display:inline-block; width:50%; text-align:left;'>
                WMSU-QMO-FR-019.00
                </div>
                <div style = 'display:inline-block; width:50%; text-align:right;'>
                  Effectivity Date: 
                  31-OCT-2016
                </div>
              </footer>      
              <div class = 'page'>
              <div>
                
                <table style='width:100%; border-spacing:0;'>
                  <thead>
                    <tr style='text-align:center;'>
                      <th>RFA Control No.</th>
                      <th>Date Issued</th>
                      <th>Request Source</th>
                      <th>Nonconformity/ Potential Nonconformity</th>
                      <th>Root Cause Analysis</th>
                      <th>Action Taken</th>
                      <th>Type Of Action</th>
                      <th>Risk Level</th>
                      <th>[1]</th>
                      <th>[2]</th>
                      <th>[3]</th>
                      <th>Auditor Name</th>
                      <th>Due Date</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>";
                    $sql = "SELECT * FROM tblrfapartone WHERE rfaDateCreated BETWEEN :startDate AND :endDate";
                    if($_SESSION['filter']=='closed'){
                      $sql = "SELECT tblrfapartone.* FROM tblrfapartone INNER JOIN tblrfaparttwo 
                      ON tblrfapartone.rfapartoneid = tblrfaparttwo.rfapartoneid INNER JOIN 
                      tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
                      INNER JOIN tblrfapartfour ON tblrfapartfour.rfapartthreeid = 
                      tblrfapartthree.rfppartthreeid
                      WHERE tblrfapartone.rfaDateCreated BETWEEN :startDate AND :endDate";
                    }elseif($_SESSION['filter']=='forverification'){
                      $sql = "SELECT a.* FROM tblrfapartone a INNER JOIN tblrfaparttwo 
                      ON a.rfapartoneid = tblrfaparttwo.rfapartoneid
                      WHERE NOT EXISTS (SELECT b.* FROM tblrfapartone b INNER JOIN tblrfaparttwo 
                      ON b.rfapartoneid = tblrfaparttwo.rfapartoneid INNER JOIN 
                      tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
                                       WHERE a.rfapartoneid = b.rfapartoneid)
                      AND 
                      a.rfaDateCreated BETWEEN :startDate AND :endDate";
                    }elseif($_SESSION['filter']=='verified'){
                      $sql = "SELECT a.* FROM tblrfapartone a INNER JOIN tblrfaparttwo 
                      ON a.rfapartoneid = tblrfaparttwo.rfapartoneid
                      INNER JOIN tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
                      WHERE NOT EXISTS (SELECT b.* FROM tblrfapartone b INNER JOIN tblrfaparttwo 
                      ON b.rfapartoneid = tblrfaparttwo.rfapartoneid INNER JOIN 
                      tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
                       INNER JOIN tblrfapartfour ON tblrfapartfour.rfapartthreeid = tblrfapartthree.rfppartthreeid
                                       WHERE a.rfapartoneid = b.rfapartoneid)
                      AND 
                      a.rfaDateCreated BETWEEN :startDate AND :endDate";
                    }elseif($_SESSION['filter']=='pendingactionplan'){
                      $sql = "SELECT a.* FROM tblrfapartone a 
                      WHERE NOT EXISTS (SELECT b.* FROM tblrfapartone b INNER JOIN tblrfaparttwo 
                      ON b.rfapartoneid = tblrfaparttwo.rfapartoneid
                      WHERE a.rfapartoneid = b.rfapartoneid)
                      AND 
                      a.rfaDateCreated BETWEEN :startDate AND :endDate";
                    }
                    $query = $model->handler->prepare($sql);
                    $query->execute(array(
                      ":startDate" => $_SESSION['startDate'],
                      ":endDate" => $_SESSION['endDate']
                    ));
                    while($row = $query->fetch()){
                      $html .= "<tr>";
                        $html .="<td>".$row['rfacontrolno']."</td>";
                        $html .="<td>".$row['rfaDateCreated']."</td>";
                        $html .="<td>".$row['rfaRequestSource']."</td>";
                        $html .="<td style='text-align:left;'>".$row['rfaNonConformity']."</td>";
                        $html .="<td style='text-align:left;'>".$model->getRootCause($row['rfapartoneid'])."</td>";
                        $html .="<td style='text-align:left;'>".$model->getDataRfaPartTwo($row['rfapartoneid'], 'immediateaction')."</td>";
                        $html .="<td>".$row['rfaTypeOfAction']."</td>";
                        $html .="<td>".$row['rfaRiskLevel']."</td>";
                        $html .="<td>".$row['rfaDateCreated']."</td>";
                        $html .="<td>".$model->getDataRfaPartTwo($row['rfapartoneid'], 'date1')."</td>";
                        $html .="<td>".$model->getDataRFAPartFour($row['rfapartoneid'], 'date1')."</td>";
                        $html .= "<td>".$model->getAccNameById($model->getIssuedBy($row['rfapartoneid']))."</td>";
                        $html .= "<td>".$model->getDueDateNotFormatted($row['rfapartoneid'])."</td>";
                        if($model->checkIfRFAClosed($row['rfapartoneid'])){
                          $html .="<td>Closed</td>";
                        }else{
                          if($model->getDataRFAPartThree($row['rfapartoneid'], "")!=""){
                            $html .="<td>Verified</td>";
                          }else{
                            if($model->getDataRfaPartTwo($row['rfapartoneid'], "rfapartoneid")!=""){
                              $html .="<td>For Verification</td>";
                            }else{
                                $html .="<td>Pending Action Plan</td>";
                            }
                          }
                        }
                      $html .="</tr>";
                    }
                  $html .= "</tbody>
                </table>
                <div style = 'margin-top:30px;'>
                <div>
                              <div style = 'float:left; width:19%;'>
                                  <div>
                                    <label>
                                      <b>Request Source:</b>
                                    </label>
                                  </div>
                                  <div>
                                    <label>
                                      IA-Internal Audit
                                    </label>
                                  </div>
                                  <div>
                                    <label>
                                      EA-External Audit
                                    </label>
                                  </div>
                                  <div>
                                    <label>
                                      MR-Management Review
                                    </label>
                                  </div>
                                  <div>
                                    <label>
                                      CF-Client Feedbacks
                                    </label>
                                  </div>
                                  <div>
                                    <label>
                                      O-Others
                                    </label>
                                  </div>
                              </div>
                              <div style = 'float:left; width:19%;'>
                                <div>
                                  <label>
                                    <b>Type Of Action:</b>
                                  </label>
                                </div>
                                <div>
                                  <label>
                                    CA-Corrective Action
                                  </label>
                                </div>
                                <div>
                                  <label>
                                    PA-Preventive Action
                                  </label>
                                </div>
                              </div>
                              <div style = 'float:left; width:19%;'>
                                  <div>
                                    <label>
                                      <b>Risk Level:</b>
                                    </label>
                                  </div>
                                  <div>
                                    <label>
                                      H-Medium
                                    </label>
                                  </div>
                                  <div>
                                    <label>
                                      M-Medium
                                    </label>
                                  </div>
                                  <div>
                                    <label>
                                      L-Low
                                    </label>
                                  </div>
                              </div>
                              <div style = 'float:left; width:19%;'>
                                <div>
                                <label>
                                  <b>Status:</b>
                                </label>
                                </div>
                                <div>
                                  <label>
                                    Open
                                  </label>
                                </div>
                                <div>
                                  <label>
                                    For Verification
                                  </label>
                                </div>
                                <div>
                                  <label>
                                    Closed
                                  </label>
                                </div>
                                <div>
                                  <label>
                                    Discarded
                                  </label>
                                </div>
                              </div>
                              <div style = 'float:left; width:19%;'>
                                  <div>
                                    <label>
                                      <b>Dates:</b>
                                    </label>
                                  </div>
                                  <div>
                                    <label>
                                      [1]-Date Implemented
                                    </label>
                                  </div>
                                  <div>
                                    <label>
                                      [2]-Date Verified Implemented
                                    </label>
                                  </div>
                                  <div>
                          <label>
                                     [3]-Date Verified or Closed Discarded
                          </label>
                                  </div>
                                    
                                  </div>
                              
                </div>
              </div>
                
                      
              </div>
              
      </body>
			
    </html>
		  ";

  $dompdf->loadHtml($html);
  // (Optional) Setup the paper size and orientation
  //$dompdf->setPaper('A4', 'landscape');
  $dompdf->setPaper(array(0,0,612,936), 'landscape');
  // Render the HTML as PDF
  $dompdf->render();

  // Output the generated PDF to Browser
  $dompdf->stream('RFA', array('Attachment'=>0));
?>
