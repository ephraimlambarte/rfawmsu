<?php
  session_start();
  include "../model/model.php";
  $model = new model();
  $model->connectDatabase();
  if($_SESSION['user']==""){
    header("Location: ../index.php");
  }
  if(isset($_SESSION['notifrfa'])){
    if($_SESSION['notifrfa']!= "initalized"){
      $model->sendNotifRfa($_SESSION['user']);
      $_SESSION['notifrfa']= "initalized";
    }
  }
 ?>

<!DOCTYPE html>
<html>
  <head>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <title>About Us</title>
    <?php include "./includeheader2.php"; ?>
  </head>
  <body>
    
  <header>

      <div class="ui menu"> <!-- TOP HEADER -->
        <div class="item top-header">
          <img class="ui middle aligned tiny image" src="../public/images/logo.png">
          <span class="top-header-span"><div>
              Western Mindanao State University
            </div>
            <div>
              Quality Management Office
            </div>
            <div>
              RFA MONITORING SYSTEM
            </div>
            </span>
        </div>
      </div>

      <div class="ui right aligned secondary menu header-item-2"> <!-- BOTTOM HEADER -->

        <div class="right item">

          <span class="header-right-item"><?php echo $model->getAccNameById($_SESSION['user']); ?></span>
          <span class="header-right-item">|</span>
          <span style="cursor: pointer" class="header-right-item"
                onclick="logout()">LOGOUT</span>

        </div>

      </div>

    </header>
    <section>
        <div class="ui grid">
            <div class="three wide column">
                <?php include "./sideNavbar.php"; ?>
            </div>
            <div class='thirteen wide column'>
                <div style = 'margin-top:20px;'>
                    <button class="ui button" id = 'creditsbutton' style = 'width:150px;'>
                        Credits
                    </button>
                </div>
                <div style = 'margin-top:20px;'>
                    <button class="ui button" id = 'manualbutton' style = 'width:150px;'>
                        <a href = '../public/OJT_USER_MANUAL.pdf'>Manual</a>
                    </button>
                </div>
            </div>
        </div>
    </section>
    <?php include '../modals/creditsModal.php'; ?>
    <?php include "./includefooter2.php" ?>
    <script src="../public/javascript/about.js"></script>
  </body>
</html>