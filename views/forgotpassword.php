<?php
  session_start();
  include "../model/model.php";
  $model = new model();
  $model->connectDatabase();

  /*include "../PHPMailer_5.2.4/class.phpmailer.php";
  $mail = new PHPMailer();  // create a new object
  $mail->IsSMTP(); // enable SMTP
  $mail->SMTPDebug = 1;  // debugging: 1 = errors and messages, 2 = messages only
  $mail->SMTPAuth = true;  // authentication enabled
  $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = 465; 
  $mail->Username = "neilivannava@gmail.com";  
  $mail->Password = "ashkelioraquionie";           
  $mail->SetFrom("neilivannava@gmail.com", "neilivannava@gmail.com");
  $mail->Subject = "Subject test";
  $mail->Body = "Hello";
  $mail->AddAddress("neilivannava27@gmail.com");
  if(!$mail->Send()) {
      $error = 'Mail error: '.$mail->ErrorInfo; 
      return false;
  } else {
      $error = 'Message sent!';
      return true;
  }*/
 ?>
 <html>
  <head>
    <?php
      include "./includeheader2.php";
    ?>
    <style>
        .indexButton{
            display:block;
            width:111px;
            margin:20px;
        }
        #buttonContainer{
            width:111px;
            height:100px;
            position: fixed;
            top:40%;
            right:10%;
        }
        #mymodaldialog{
            width:255px;
        }
        .loginInput{
            display: block;
            margin:5px 0;
            height:30px;
        }
        #cbDepartment{
        width:100%;
        }
        #login, #register{
        background-color: #CCFFBB;
        }
        @media screen and (max-width: 488px) {
            #buttonContainer{
                top:35%;
                right:20%;
            }
        }
        #buttonLogin, #register{
            width:100%;
            margin-bottom:20px;
            background-color: #CCFFBB;
            color:green;
        }
        #loginContainer{
            display:flex;
            flex:11;
        }
        #firstflex, #thirdflex{
            flex:4;
        }
        #loginflex{
            flex:3;
            background-color:rgba(13, 13, 13, 0.5);
            padding:20px;
        }
        #loginflex label{
            color:white;
            margin:0px;
            padding:0px;
        }
        #loginflex input{
            border-radius:5px;
            width:100%;
        }
        #superContainer{
            display:flex;
            justify-content:center;
            align-items:center;
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            overflow: auto;
        }
    </style>
  </head>
  <body>
    <div id = "superContainer">
        <div id = 'loginContainer'>
            <div id = 'firstflex'></div>
            <div id = 'loginflex'>
                <div>
                    <div>
                    <input type = 'text' id = 'username' placeholder = 'Username'>
                    </div>
                    <button id = 'generateQuestion'>
                        Generate Question 
                    </button>
                </div>
                <div>
                    Please answer this security question:
                </div>
                <div>
                    <p id = 'securityQuestion'>...</p>
                </div>
                <div>
                    <input type = 'text' id = 'answer'>
                </div>
                <div>
                    <button id= 'submitAnswer'>Submit</button>
                </div>
            </div>
            <div id = 'thirdflex'></div>
        </div>
    </div>
    <?php include "./includefooter2.php" ?>
    <script src="../public/javascript/forgotpassword.js"></script>
  </body>
 </html>