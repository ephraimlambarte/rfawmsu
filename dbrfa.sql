-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2017 at 02:19 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbrfa`
--

-- --------------------------------------------------------

--
-- Table structure for table `designationhistory`
--

CREATE TABLE `designationhistory` (
  `historyID` int(11) NOT NULL,
  `accountid` int(11) NOT NULL,
  `designationid` int(11) NOT NULL,
  `dateAssigned` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `designationhistory`
--

INSERT INTO `designationhistory` (`historyID`, `accountid`, `designationid`, `dateAssigned`) VALUES
(4, 19, 2, '2017-11-16'),
(5, 6, 1, '2017-11-16'),
(6, 10, 2, '2017-11-16'),
(7, 11, 1, '2017-11-16');

-- --------------------------------------------------------

--
-- Table structure for table `designationstable`
--

CREATE TABLE `designationstable` (
  `designationID` int(11) NOT NULL,
  `designationName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `designationstable`
--

INSERT INTO `designationstable` (`designationID`, `designationName`) VALUES
(1, 'secretary'),
(2, 'janitor');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `notificationid` int(11) NOT NULL,
  `accountid` int(11) NOT NULL,
  `message` varchar(500) NOT NULL,
  `status` text NOT NULL,
  `rfapartoneid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`notificationid`, `accountid`, `message`, `status`, `rfapartoneid`) VALUES
(195, 6, 'RFA with control no: 2017-002036 is not yet processed and its due date is within less than 2 weeks away!', 'Read', 2036),
(196, 6, 'RFA with control no: 2017-002036 is not yet processed and its due date is within less than 2 weeks away!', 'Read', 2036),
(197, 16, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 3),
(198, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 4),
(199, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 5),
(200, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 6),
(201, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 7),
(202, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 8),
(203, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 9),
(204, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 10),
(205, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 11),
(206, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 12),
(207, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 13),
(208, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 14),
(209, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 15),
(210, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 16),
(211, 6, 'An RFA with Control no:  have been issued to you for part 2 by lambarte, ephraims', '', 17);

-- --------------------------------------------------------

--
-- Table structure for table `session_table`
--

CREATE TABLE `session_table` (
  `sessionid` int(11) NOT NULL,
  `datelogged` date NOT NULL,
  `timelogged` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session_table`
--

INSERT INTO `session_table` (`sessionid`, `datelogged`, `timelogged`) VALUES
(6, '2017-12-13', '22:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `tblaccount`
--

CREATE TABLE `tblaccount` (
  `accountid` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `usertype` varchar(40) NOT NULL,
  `security_Question` varchar(100) NOT NULL,
  `question_Answer` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblaccount`
--

INSERT INTO `tblaccount` (`accountid`, `username`, `password`, `firstname`, `lastname`, `status`, `usertype`, `security_Question`, `question_Answer`, `email`) VALUES
(6, 'ephraims', 'ephraim', 'ephraims', 'lambarte', 1, 'Admin', 'What is your mother\'s maiden name?', 'cora', 'ephraimlambarte@yahoo.com'),
(10, 'a', 'a', 'Tifannie pao    ', 'Dacalos', 1, 'Auditor', 'Who is your first love?', 'ephraim', 'tifannie@gmail.com'),
(11, 'auditor', 'auditor', 'auditor', 'auditor', 1, 'Auditor', 'What is your mother\'s maiden name?', 'hey', 'hey@gmail.com'),
(12, 'auditee', 'auditee', 'auditee', 'auditee', 1, 'SubAdmin', '', '', ''),
(13, 'test1', 'test1', 'test1', 'test', 1, 'Auditor', 'What is the name of your first pet?', 'test1', ''),
(15, 'shielas', 'shiela', 'Shiela', 'Macrohon', 1, 'Auditor', 'What is your mother\'s maiden name?', 'shiela', 'shie@gmail.com'),
(16, 'asd', 'asd', 'asd', 'asd', 1, 'Auditee', 'What is your mother\'s maiden name?', 'asdssss', 'asd'),
(19, 'gem', 'gem', 'Gem', 'Labrador', 1, 'Auditor', 'What is the name of your first pet?', 'gem', 'gem@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tblcoreunit`
--

CREATE TABLE `tblcoreunit` (
  `coreunitid` int(11) NOT NULL,
  `cuname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcoreunit`
--

INSERT INTO `tblcoreunit` (`coreunitid`, `cuname`) VALUES
(3, 'computer'),
(2, 'shit');

-- --------------------------------------------------------

--
-- Table structure for table `tblprocessunit`
--

CREATE TABLE `tblprocessunit` (
  `processunitid` int(11) NOT NULL,
  `coreunitid` int(11) NOT NULL,
  `puname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblprocessunit`
--

INSERT INTO `tblprocessunit` (`processunitid`, `coreunitid`, `puname`) VALUES
(2, 2, 'mousess'),
(3, 3, 'speaker'),
(5, 3, 'speakers');

-- --------------------------------------------------------

--
-- Table structure for table `tblrfapartfour`
--

CREATE TABLE `tblrfapartfour` (
  `rfapartfourid` int(11) NOT NULL,
  `rfapartthreeid` int(11) NOT NULL,
  `satisfaction` tinyint(4) NOT NULL,
  `verifiedby` varchar(200) NOT NULL,
  `remarks1` varchar(255) NOT NULL,
  `date1` date NOT NULL,
  `validatedby` varchar(200) NOT NULL,
  `remarks2` varchar(255) NOT NULL,
  `date2` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tblrfapartone`
--

CREATE TABLE `tblrfapartone` (
  `rfapartoneid` int(11) NOT NULL,
  `rfacontrolno` varchar(500) NOT NULL COMMENT 'format 2017-XXXXX',
  `coureunitid` int(11) NOT NULL,
  `processunitid` int(11) NOT NULL,
  `subunitid` int(11) NOT NULL,
  `rfaTypeOfAction` varchar(20) NOT NULL,
  `rfaRequestSource` varchar(20) NOT NULL,
  `rfaRequestSourceSpecified` varchar(255) NOT NULL,
  `rfaNonConformity` varchar(500) NOT NULL,
  `rfaDescription` varchar(500) NOT NULL,
  `rfaRiskLevel` varchar(20) NOT NULL,
  `rfaDateCreated` date NOT NULL,
  `rfaDateDue` date NOT NULL COMMENT 'add 30 days from date created',
  `rfaIssuedBy` int(11) NOT NULL,
  `rfaConformedBy` int(11) NOT NULL,
  `rfaAcknowledgeBy` int(11) NOT NULL,
  `acknowledged` tinyint(1) NOT NULL,
  `conformed` tinyint(1) NOT NULL,
  `issued` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblrfapartone`
--

INSERT INTO `tblrfapartone` (`rfapartoneid`, `rfacontrolno`, `coureunitid`, `processunitid`, `subunitid`, `rfaTypeOfAction`, `rfaRequestSource`, `rfaRequestSourceSpecified`, `rfaNonConformity`, `rfaDescription`, `rfaRiskLevel`, `rfaDateCreated`, `rfaDateDue`, `rfaIssuedBy`, `rfaConformedBy`, `rfaAcknowledgeBy`, `acknowledged`, `conformed`, `issued`) VALUES
(3, '2017-003', 2, 2, 5, 'Corrective Action', 'Client Feedback', '', 'test', 'test', 'Medium Risk', '2017-11-21', '2017-12-21', 13, 16, 6, 0, 0, 1),
(4, '2017-004', 2, 2, 5, 'Corrective Action', 'Management Review', '', 'test2', 'test2', 'Medium Risk', '2017-11-21', '2017-12-21', 10, 6, 6, 0, 0, 1),
(5, '2017-005', 2, 2, 5, 'Corrective Action', 'Management Review', '', 'test3', 'test3', 'Medium Risk', '2017-11-21', '2017-12-21', 10, 6, 6, 0, 0, 1),
(6, '2017-006', 2, 2, 5, 'Corrective Action', 'Management Review', '', 'test4', 'test4', 'Medium Risk', '2017-11-21', '2017-12-21', 10, 6, 6, 0, 0, 1),
(7, '2017-007', 2, 2, 5, 'Corrective Action', 'Management Review', '', 'test5', 'test5', 'Medium Risk', '2017-11-21', '2017-12-21', 10, 6, 6, 0, 0, 1),
(8, '2017-008', 2, 2, 5, 'Corrective Action', 'Management Review', '', 'test6', 'test6', 'Medium Risk', '2017-11-21', '2017-12-21', 10, 6, 6, 0, 0, 1),
(9, '2017-009', 2, 2, 5, 'Corrective Action', 'Management Review', '', 'test7', 'test7', 'Medium Risk', '2017-11-21', '2017-12-21', 10, 6, 6, 0, 0, 1),
(10, '', 2, 2, 5, 'Corrective Action', 'Client Feedback', '', 'test8', 'test8', 'Medium Risk', '2017-11-22', '2017-12-22', 11, 6, 12, 0, 0, 1),
(11, '2017-011', 2, 2, 5, 'Corrective Action', 'Client Feedback', '', 'test9 ', 'test 9', 'Medium Risk', '2017-11-22', '2017-12-22', 11, 6, 12, 0, 0, 1),
(12, '2017-012', 2, 2, 5, 'Corrective Action', 'Client Feedback', '', 'test 10', ' test 10', 'Medium Risk', '2017-11-22', '2017-12-22', 11, 6, 12, 0, 0, 1),
(13, '2017-013', 2, 2, 5, 'Corrective Action', 'Client Feedback', '', 'test 11', 'test 11', 'Medium Risk', '2017-11-22', '2017-12-22', 11, 6, 12, 0, 0, 1),
(14, '2017-014', 2, 2, 5, 'Corrective Action', 'Client Feedback', '', 'test 12', 'test 12', 'Medium Risk', '2017-11-22', '2017-12-22', 11, 6, 12, 0, 0, 1),
(15, '2017-015', 2, 2, 5, 'Corrective Action', 'Client Feedback', '', 'test 13', 'test 13', 'Medium Risk', '2017-11-22', '2017-12-22', 11, 6, 12, 0, 0, 1),
(16, '2017-016', 2, 2, 5, 'Corrective Action', 'Client Feedback', '', 'test 14', 'test 14', 'Medium Risk', '2017-11-22', '2017-12-22', 11, 6, 12, 0, 0, 1),
(17, '2017-017', 2, 2, 5, 'Corrective Action', 'Client Feedback', '', 'test 15 ', 'test 15', 'Medium Risk', '2017-11-22', '2017-12-22', 11, 6, 12, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblrfapartthree`
--

CREATE TABLE `tblrfapartthree` (
  `rfppartthreeid` int(11) NOT NULL,
  `rfaparttwoid` int(11) NOT NULL,
  `followupdate` date NOT NULL,
  `details` varchar(500) NOT NULL,
  `satisfaction` tinyint(4) DEFAULT NULL,
  `verifiedby` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tblrfaparttwo`
--

CREATE TABLE `tblrfaparttwo` (
  `parttwoid` int(11) NOT NULL,
  `rfapartoneid` int(11) NOT NULL,
  `rootcauses` varchar(500) NOT NULL,
  `immediateaction` varchar(500) DEFAULT NULL,
  `implementedby1` varchar(200) DEFAULT NULL,
  `date1` date DEFAULT NULL,
  `preventiveaction` varchar(500) DEFAULT NULL,
  `implementedby2` varchar(200) DEFAULT NULL,
  `date2` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tblsubunit`
--

CREATE TABLE `tblsubunit` (
  `subunitid` int(11) NOT NULL,
  `processunitid` int(11) NOT NULL,
  `suname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsubunit`
--

INSERT INTO `tblsubunit` (`subunitid`, `processunitid`, `suname`) VALUES
(1, 2, 'samsung'),
(3, 3, 'oppos'),
(4, 3, 'opposs'),
(5, 2, 'samsungs'),
(6, 3, 'opposss');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `designationhistory`
--
ALTER TABLE `designationhistory`
  ADD PRIMARY KEY (`historyID`),
  ADD KEY `accountid` (`accountid`),
  ADD KEY `designationid` (`designationid`);

--
-- Indexes for table `designationstable`
--
ALTER TABLE `designationstable`
  ADD PRIMARY KEY (`designationID`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`notificationid`),
  ADD KEY `accountid` (`accountid`);

--
-- Indexes for table `session_table`
--
ALTER TABLE `session_table`
  ADD PRIMARY KEY (`sessionid`);

--
-- Indexes for table `tblaccount`
--
ALTER TABLE `tblaccount`
  ADD PRIMARY KEY (`accountid`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- Indexes for table `tblcoreunit`
--
ALTER TABLE `tblcoreunit`
  ADD PRIMARY KEY (`coreunitid`),
  ADD UNIQUE KEY `cuname_UNIQUE` (`cuname`);

--
-- Indexes for table `tblprocessunit`
--
ALTER TABLE `tblprocessunit`
  ADD PRIMARY KEY (`processunitid`),
  ADD UNIQUE KEY `puname_UNIQUE` (`puname`),
  ADD KEY `coreunitid` (`coreunitid`);

--
-- Indexes for table `tblrfapartfour`
--
ALTER TABLE `tblrfapartfour`
  ADD PRIMARY KEY (`rfapartfourid`),
  ADD KEY `verifiedby` (`verifiedby`),
  ADD KEY `validatedby` (`validatedby`),
  ADD KEY `rfapartthreeid` (`rfapartthreeid`);

--
-- Indexes for table `tblrfapartone`
--
ALTER TABLE `tblrfapartone`
  ADD PRIMARY KEY (`rfapartoneid`),
  ADD KEY `rfaConformedBy` (`rfaConformedBy`),
  ADD KEY `rfaIssuedBy` (`rfaIssuedBy`),
  ADD KEY `coureunitid` (`coureunitid`),
  ADD KEY `processunitid` (`processunitid`),
  ADD KEY `subunitid` (`subunitid`),
  ADD KEY `rfaAcknowledgeBy` (`rfaAcknowledgeBy`);

--
-- Indexes for table `tblrfapartthree`
--
ALTER TABLE `tblrfapartthree`
  ADD PRIMARY KEY (`rfppartthreeid`),
  ADD KEY `verifiedby` (`verifiedby`),
  ADD KEY `rfaparttwoid` (`rfaparttwoid`);

--
-- Indexes for table `tblrfaparttwo`
--
ALTER TABLE `tblrfaparttwo`
  ADD PRIMARY KEY (`parttwoid`),
  ADD KEY `implementedby2` (`implementedby2`),
  ADD KEY `implementedby1` (`implementedby1`),
  ADD KEY `rfapartoneid` (`rfapartoneid`);

--
-- Indexes for table `tblsubunit`
--
ALTER TABLE `tblsubunit`
  ADD PRIMARY KEY (`subunitid`),
  ADD UNIQUE KEY `suname_UNIQUE` (`suname`),
  ADD KEY `processunitid` (`processunitid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `designationhistory`
--
ALTER TABLE `designationhistory`
  MODIFY `historyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `designationstable`
--
ALTER TABLE `designationstable`
  MODIFY `designationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `notificationid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;
--
-- AUTO_INCREMENT for table `tblaccount`
--
ALTER TABLE `tblaccount`
  MODIFY `accountid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tblcoreunit`
--
ALTER TABLE `tblcoreunit`
  MODIFY `coreunitid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblprocessunit`
--
ALTER TABLE `tblprocessunit`
  MODIFY `processunitid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tblrfapartfour`
--
ALTER TABLE `tblrfapartfour`
  MODIFY `rfapartfourid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblrfapartone`
--
ALTER TABLE `tblrfapartone`
  MODIFY `rfapartoneid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tblrfapartthree`
--
ALTER TABLE `tblrfapartthree`
  MODIFY `rfppartthreeid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblrfaparttwo`
--
ALTER TABLE `tblrfaparttwo`
  MODIFY `parttwoid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsubunit`
--
ALTER TABLE `tblsubunit`
  MODIFY `subunitid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `designationhistory`
--
ALTER TABLE `designationhistory`
  ADD CONSTRAINT `designationhistory_ibfk_1` FOREIGN KEY (`accountid`) REFERENCES `tblaccount` (`accountid`),
  ADD CONSTRAINT `designationhistory_ibfk_2` FOREIGN KEY (`designationid`) REFERENCES `designationstable` (`designationID`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`accountid`) REFERENCES `tblaccount` (`accountid`);

--
-- Constraints for table `tblprocessunit`
--
ALTER TABLE `tblprocessunit`
  ADD CONSTRAINT `tblprocessunit_ibfk_1` FOREIGN KEY (`coreunitid`) REFERENCES `tblcoreunit` (`coreunitid`);

--
-- Constraints for table `tblrfapartfour`
--
ALTER TABLE `tblrfapartfour`
  ADD CONSTRAINT `tblrfapartfour_ibfk_3` FOREIGN KEY (`rfapartthreeid`) REFERENCES `tblrfapartthree` (`rfppartthreeid`);

--
-- Constraints for table `tblrfapartone`
--
ALTER TABLE `tblrfapartone`
  ADD CONSTRAINT `tblrfapartone_ibfk_1` FOREIGN KEY (`rfaIssuedBy`) REFERENCES `tblaccount` (`accountid`),
  ADD CONSTRAINT `tblrfapartone_ibfk_2` FOREIGN KEY (`rfaConformedBy`) REFERENCES `tblaccount` (`accountid`),
  ADD CONSTRAINT `tblrfapartone_ibfk_3` FOREIGN KEY (`rfaIssuedBy`) REFERENCES `tblaccount` (`accountid`),
  ADD CONSTRAINT `tblrfapartone_ibfk_4` FOREIGN KEY (`coureunitid`) REFERENCES `tblcoreunit` (`coreunitid`),
  ADD CONSTRAINT `tblrfapartone_ibfk_5` FOREIGN KEY (`processunitid`) REFERENCES `tblprocessunit` (`processunitid`),
  ADD CONSTRAINT `tblrfapartone_ibfk_7` FOREIGN KEY (`rfaAcknowledgeBy`) REFERENCES `tblaccount` (`accountid`);

--
-- Constraints for table `tblrfapartthree`
--
ALTER TABLE `tblrfapartthree`
  ADD CONSTRAINT `tblrfapartthree_ibfk_2` FOREIGN KEY (`rfaparttwoid`) REFERENCES `tblrfaparttwo` (`parttwoid`);

--
-- Constraints for table `tblrfaparttwo`
--
ALTER TABLE `tblrfaparttwo`
  ADD CONSTRAINT `tblrfaparttwo_ibfk_3` FOREIGN KEY (`rfapartoneid`) REFERENCES `tblrfapartone` (`rfapartoneid`);

--
-- Constraints for table `tblsubunit`
--
ALTER TABLE `tblsubunit`
  ADD CONSTRAINT `tblsubunit_ibfk_1` FOREIGN KEY (`processunitid`) REFERENCES `tblprocessunit` (`processunitid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
