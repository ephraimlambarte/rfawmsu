<?php
  session_start();
  include "../model/model.php";
  $model = new model();
  $model->connectDatabase();
  if($_SESSION['user']==""){
    header("Location: ../index.php");
  }
 ?>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel = "stylesheet" type = "text/css" href="../public/Libraries/semantic.css">
    <link rel = "stylesheet" type = "text/css" href="../public/Libraries/icon.css">
    <link rel = "stylesheet" type = "text/css" href="../public/stylesheet/loader.css">
    <link rel = "stylesheet" type = "text/css" href="../public/stylesheet/generic.css">
    <!--script src="./Libraries/ajax-3.1.1.js"></script-->
    <script src="../public/javascript/jquery-3.1.0.js"></script>
    <script src="../public/Libraries/semantic.js"></script>
    <script src="../public/Libraries/semantic.min.js"></script>
    <style>
      .header{
        margin: 0px;
        padding: 0px;
      }
      .mydiv{
        margin-top: 10px;
      }
    </style>
  </head>
  <body>
    <div class='ui container'>
      <!--div class='ui grid'-->
      <div class='ui grid'>
        <div class='row' style = 'margin:5px; padding-bottom:0px;'>
          <div class='four wide column'>
            <h4 class='header'>Western Mindanao State University</h4>
            <h4 class='header'>QUALITY MANAGEMENT OFFICE</h4>
            <h4 class='header'>Zamboanga City</h4>
          </div>
          <div class='eight wide column'>
          </div>
          <div class='four wide column'>
            <h4 class='header'>
              REQUEST FOR ACTION (RFA)
            </h4>
          </div>
        </div>
        <div class='row' style = 'margin:5px; padding:0px;'>
          <div class='four wide column'>
          </div>
          <div class='eight wide column'>
          </div>
          <div class='four wide column'>
            <label>Date:</label>
            <div class="ui input">
              <input type = 'date' value='<?php echo date("Y-m-d");?>' readonly>
            </div>
          </div>
        </div>
        <div class='row' style = 'margin:5px; padding:0px;'>
          <div class='four wide column'>
            <div class='row'>
              <label>Core Unit:</label>
            </div>
            <div class="ui input">
              <input type = 'text' value='<?php echo $model->getRFAPartOneCoreUnit($_SESSION['RFAPartoneID']);?>' readonly>
            </div>
          </div>
          <div class='four wide column'>
            <div class='row'>
              <label>Process Unit:</label>
            </div>
            <div class="ui input">
              <input type = 'text' value='<?php echo $model->getProcessUnitIDByRFAID($_SESSION['RFAPartoneID']);?>' readonly>
            </div>
          </div>
          <div class='four wide column'>
            <div class='row'>
              <label>Sub Unit:</label>
            </div>
            <div class="ui input">
              <input type = 'text' value='<?php echo $model->getSubUnitIDByRFAID($_SESSION['RFAPartoneID']);?>' readonly>
            </div>
          </div>
          <div class='four wide column'>
            <div class='row'>
              <label>RFA Control No:</label>
          </div>
            <div class="ui input">
              <input type = 'text' value='<?php echo $model->getControlNumberRFA($_SESSION['RFAPartoneID']);?>' readonly>
            </div>
          </div>
        </div>
      </div>
      <div class='ui segments sixteen wide column' style = 'padding:5px;'>
        <div class="ui segment" style = 'padding:5px; background-color:#cccccc; margin:0px;'>
          <h4>Part I: Problem Identification (To be filled-up by the Requesting Source representative or auditor)</h4>
        </div>
        <div class="ui segment">
          <div class='ui grid'>
          <div class='row'>
            <div class='four wide column'>
              <h4>Type Of Action:</h4>
            </div>
            <div class='six wide column'>
              <label>Corrective Action:</label>
              <input type = 'radio'>
            </div>
            <div class='six wide column'>
              <label>Preventive Action:</label>
              <input type = 'radio'>
            </div>
          </div>
          </div>
        </div>
        <div class='ui segment'>
          <div class='ui grid'>
            <div class='row'>
              <div class='four wide column'>
                <h4>Request Source:</h4>
              </div>
              <div class='four wide column'>
              </div>
              <div class='four wide column'>
              </div>
              <div class='four wide column'>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/div-->
    </div>
  </body>
</html>
