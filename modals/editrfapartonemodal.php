<div class="ui large modal" id = "editRfaPartOneModal">
  <i class="close icon"></i>
  <div class="header">
    RFA Issue
  </div>
  <div class="image content" id = "rfaPartOneDetails">
    <div class='ui grid'>

        <div class='row'>
          <div class="four wide column" id="rfa_cudiv">

            <label>Core Unit</label>
            <div class="ui selection dropdown rfaModalDropDown" id="rfa_cu">

              <input type="hidden">
              <i class="dropdown icon"></i>
              <div class="default text">Core Unit</div>

              <div class="menu" id="loadrfa_cu">

              </div>

            </div>

          </div>
          <div class="four wide column" id="rfa_sudiv">

            <label>Process Unit</label>
            <div class="ui selection dropdown rfaModalDropDown" id="rfa_su">

              <input type="hidden">
              <i class="dropdown icon"></i>
              <div class="default text">Process Unit</div>

              <div class="menu" id="loadrfa_su">

              </div>

            </div>

          </div>
          <div class="four wide column" id="rfa_pudiv">

            <label>Sub Unit</label>
            <div class="ui selection dropdown rfaModalDropDown" id="rfa_pu">

              <input type="hidden">
              <i class="dropdown icon"></i>
              <div class="default text">Sub Unit</div>

              <div class="menu" id="loadrfa_pu">

              </div>

            </div>

          </div>
          <div class="four wide column" id="rfa_Date">
            <div class="four wide field">
              <div class='row'>
              <label>Date:</label>
              </div>
              <div class='ui input'>
                <input type="date" id="rfa_dateissued" value="<?php echo date("Y-m-d");?>" readonly>
              </div>
            </div>
          </div>

      </div>
      <div class="ui inverted divider"></div>

        <div class='row'>
          <div class='five wide column'>
            <label>
              Type Of Action:
            </label>
          </div>
          <div class='five wide column'>
            <div class='ui radio'>
              <input type="radio" name="Type_Of_Action" id="at_ca"
                      value = "Corrective Action" class= "checkBoxActionType">
              <label for = 'at_ca'>Corrective Action</label>
            </div>
          </div>
          <div class='five wide column'>
            <div class='ui radio'>
              <input type="radio" name="Type_Of_Action" id="at_pa"
                      value = "Preventive Action" class= "checkBoxActionType">
              <label for = 'at_pa'>Preventive Action</label>
            </div>
          </div>
        </div>
          <div class="ui inverted divider"></div>
        <div class='row'>
          <div class='three wide column'>
            <label>
              Request Source:
            </label>
          </div>
          <div class='three wide column'>
            <div class='row'>
              <div class='ui radio'>
                <input type="radio" name="requestSource" id="internalAuditRadio"
                        value = "Internal Audit" class= "radioActionType">
                <label for = 'internalAuditRadio'>Internal Audit</label>
              </div>
            </div>
            <div class='row'>
              <div class='ui radio'>
                <input type="radio" name="requestSource" id="externalAuditRadio"
                        value = "Internal Audit" class= "radioActionType">
                <label for = 'externalAuditRadio'>External Audit</label>
              </div>
            </div>
          </div>
          <div class='three wide column'>
            <div class='row'>
              <div class='ui radio'>
                <input type="radio" name="requestSource" id="clientFeedBackRadio"
                        value = "Client Feedbacks" class= "radioActionType">
                <label for = 'clientFeedBackRadio'>Client Feedback</label>
              </div>
            </div>
            <div class='row'>
              <div class='ui radio'>
                <input type="radio" name="requestSource" id="managementReviewRadio"
                        value = "Management Review" class= "radioActionType">
                <label for = 'managementReviewRadio'>Management Review</label>
              </div>
            </div>
          </div>
          <div class='three wide column'>
            <div class='row'>
              <div class='ui radio'>
                <input type="radio" name="requestSource" id="specifyRadio"
                        value = "specify" class= "radioActionType">
                <label for = 'specifyRadio'>Others(specify)</label>
              </div>
            </div>
          </div>
          <div class='four wide column'>
            <label>Specify</label>
            <div class='ui input'>
              <input type = 'text' id = 'specifyText' placeholder="Specify">
            </div>
          </div>
        </div>
        <div class="ui inverted divider"></div>
        <div class='row'>
          <div class="ui form sixteen wide column">
            <div class='field'>
                <label>Nonconformance / Potential Nonconformance</label>
                <textarea rows="3" type="text" id="rfa_nonconformity" style = 'width:100%;'></textarea>
            </div>
          </div>
        </div>
        <div class="ui inverted divider"></div>
        <div class='row'>
          <div class="ui form sixteen wide column">
            <div class='field'>
                <label>Description</label>
                <textarea rows="3" type="text" id="rfa_desc" style = 'width:100%;'></textarea>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='eight wide column'>
            <div class='row'>
              <label style="font-weight: 600">Issued By:</label>
            </div>
            <div class='row'>
              <div class='ui input' style = 'width:100%;'>
                <input type = 'text' id = 'rfa_issuedbyText' placeholder="Signature over printed name" readonly>
              </div>
            </div>
            <div class='row'>
              <label style="margin-top: 20px; font-weight: 600" id="rfa_issuedby"><?php echo $model->getAccNameById($_SESSION['user']); ?></label>
            </div>
            <input type = "hidden" id = "issuedby1" value='<?php echo $_SESSION['user']; ?>'>
            <div class="ui inverted divider"></div>
            <div class='row'>
              <div class='sixteen wide column'>
                <div class='row'>
                  <label style="font-weight: 600">Conformed By</label>
                </div>
                <div class='row'>
                  <div class = 'ui input' style = 'width:100%;'>
                    <input type  = 'text' readonly placeholder="Signature over printed name">
                  </div>
                </div>
              </div>
              <div class='sixteen wide column'>
                <div class="ui selection dropdown rfaModalDropDown" id="rfa_conformedby" style = 'width:100%;'>

                  <input type="hidden">
                  <i class="dropdown icon"></i>
                  <div class="default text">Conformed By</div>

                  <div class="menu" id="loadrfa_conformedby">

                  </div>

                </div>
              </div>
            </div>
          </div>
          <div class='eight wide column'>
            <div class='row'>
              <div class='eight wide column'>
                <div class='sixteen wide column'>
                  <h5>Risk Level</h5>
                </div>
                <div class="ui inverted divider"></div>
                <div class='sixteen wide column'>
                  <div class='row'>
                    <div class='ui radio'>
                      <input type="radio" name="riskLevel" id="highRiskRadio"
                              value = "High" class= "radioActionType">
                      <label for = 'highRiskRadio'>High Risk</label>
                    </div>
                    <div class='ui radio'>
                      <input type="radio" name="riskLevel" id="mediumRiskRadio"
                              value = "Medium" class= "radioActionType">
                      <label for = 'mediumRiskRadio'>Medium Risk</label>
                    </div>
                  </div>
                  <div class='row'>
                    <div class='ui radio'>
                      <input type="radio" name="riskLevel" id="lowRiskRadio"
                              value = "Low" class= "radioActionType">
                      <label for = 'lowRiskRadio'>Low Risk</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="ui inverted divider"></div>
              <div class='eight wide column'>
                <label>Due Date:</label>
                <div class='ui input'>
                  <input type="date" id="rfa_duedate" readonly value="<?php echo date('Y-m-d', strtotime("+30 days")); ?>">
                </div>
              </div>
            </div>
              <div class="ui inverted divider"></div>
            <div class='row'>
              <div class='row'>
                <label style="font-weight: 600">Acknowledged By:</label>
              </div>
              <div class='row'>
                <div class='ui input' style = 'width:100%;'>
                  <input type = 'text' id = 'acknowledgedBy' placeholder="Signature over printed name" readonly>
                </div>
              </div>
              <div class='row'>
                <label style="margin-top: 20px; font-weight: 600" id="rfa_issuedby"><?php echo $model->getAccNameById($_SESSION['user']); ?></label>
              </div>
              <input type = 'hidden' id = 'acknowledgedByRfa'>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="actions">
      <div class="ui black deny button">
        Close
      </div>
      
    </div>
  </div>
