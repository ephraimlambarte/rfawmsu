<?php include "../views/includeheader2.php"; ?>
<div class="header" style="background-color: grey; color: white">
  <h4 style="color: white">Manage Process Unit</h4>
</div>

<div class="content">

  <div class="fields" style="margin-left: 10px; margin-right: 10px;">

    <form class="ui form" id="addprocessunitForm" onsubmit="return false">

      <h5 class="ui dividing header" style="color: grey">Add New Process Unit</h5>

      <div class="fields">

        <div class="sixteen wide field">

          <div class="fields">

            <div class="six wide field" id="pu_namediv">
              <label>Name</label>
              <input type="text" name="pu_name" placeholder="Process Unit Name" id="pu_name">
            </div>

            <div class="six wide field" id="pu_cudiv">
              <label>Core Unit</label>
              <div class="ui fluid selection dropdown" id="mycoreunit">
                <input type="hidden" name="new_dcategory" id="mycoreunit2">
                <i class="dropdown icon"></i>
                <div class="default text">Core unit</div>
                <div class="menu" id = "mycoreunit3">

                </div>
              </div>

            </div>
          </div>
          <div class = 'fields'>
            <div class="twelve wide field">
              <button class="ui blue button" id = "processUnitModalAddButton">Add</button>
              <button class="ui grey button" id = "processUnitModalSaveButton">Save</button>
              <button class="ui green button" id = "processUnitModalClearButton">Clear</button>
            </div>
          </div>

        </div>

      </div>

    </form>

    <h5 class="ui dividing header" style="color: grey">List of Process Unit</h5>

    <form class="ui form t1" id="processunitListForm" onsubmit="return false"
          style="overflow:scroll; overflow-x:hidden; height: 200px;">

      <table class="ui celled table">

        <thead>
          <tr>
            <th>Name</th>
            <th>Core Unit</th>
            <th># of Sub-Unit</th>
            <th>Delete</th>
          </tr>
        </thead>

        <tbody id="processunitListTable">

        </tbody>

      </table>

    </form>

  </div>

</div>

<div class="actions">

  <input class="ui grey cancel button" type="submit" name="submit" value="Close">

</div>

<?php include './notificationmodal.php'; ?>
<div class="ui small modal" id = "yesnomodalProcessUnit">
  <i class="close icon"></i>
  <div class="header">
    Alert!
  </div>
  <div class="image content">
    <div>
      <h4 id="questionss">Are you sure you want to delete?</h4>
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Nope
    </div>
    <div class="ui positive right labeled icon button deny" id = "yesMyProcessUnitButton">
      Yep
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>
<?php include "../views/includefooter2.php" ?>
<script src="../public/javascript/processunitmodal.js"></script>
