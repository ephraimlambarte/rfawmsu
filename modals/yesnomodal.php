<div class="ui small modal" id = "yesnomodal">
  <i class="close icon"></i>
  <div class="header">
    Alert!
  </div>
  <div class="image content">
    <div>
      <h4 id="question">Are you sure you want to delete?<i class="warning sign icon"></i></h4>
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      No
    </div>
    <div class="ui positive right labeled icon button deny" id = "yesButton">
      Yes
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>
