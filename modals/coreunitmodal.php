<div class="header" style="background-color: grey; color: white">
  <h4 style="color: white">Manage Core Unit</h4>
</div>

<div class="content">

  <div class="fields" style="margin-left: 10px; margin-right: 10px;">

    <form class="ui form" id="addcoreunitForm" onsubmit="return false">

      <h5 class="ui dividing header" style="color: grey">Add New Core Unit</h5>

      <div class="fields">

        <div class="sixteen wide field">

          <label>Name</label>

          <div class="two fields">

            <div class="field" id="cu_namediv">
                <input type="text" name="cu_name" placeholder="Core Unit Name" id="cu_name">
            </div>

            <div class="field">
              <button class="ui blue button" id = "addCoreunitModalButton">Add</button>
              <button class = "ui grey button" id = "saveCoreunitModalButton">Save</button>

              <button class="ui green button" id = "clearCoreunitModalButton">Clear</button>
            </div>

          </div>

        </div>

      </div>

    </form>

    <h5 class="ui dividing header" style="color: grey">List of Core Unit</h5>

    <form class="ui form t1" id="coreunitListForm" onsubmit="return false"
          style="overflow:scroll; overflow-x:hidden; height: 200px;">

      <table class="ui celled table">

        <thead>
          <tr>
            <th>Name</th>
            <th># of Process Unit</th>
            <th>Delete</th>
          </tr>
        </thead>

        <tbody id="coreunitListTable">

        </tbody>

      </table>

    </form>

  </div>

</div>

<div class="actions">

  <input class="ui grey cancel button" type="submit" name="submit" value="Close">

</div>

<?php include './notificationmodal.php'; ?>
<div class="ui small modal" id = "yesnomodalCoreunit">
  <i class="close icon"></i>
  <div class="header">
    Alert!
  </div>
  <div class="image content">
    <div>
      <h4 id="questions">Are you sure you want to delete?</h4>
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Nope
    </div>
    <div class="ui positive right labeled icon button deny" id = "yesCoreUnitButton">
      Yep
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>
<?php include "../views/includefooter2.php" ?>
<script src="../public/javascript/coreunitmodal.js"></script>
