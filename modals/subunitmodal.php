<div class="header" style="background-color: grey; color: white">
  <h4 style="color: white">Manage Sub Unit</h4>
</div>

<div class="content">

  <div class="fields" style="margin-left: 10px; margin-right: 10px;">

    <form class="ui form" id="addsubunitForm" onsubmit="return false">

      <h5 class="ui dividing header" style="color: grey">Add New Sub Unit</h5>

      <div class="fields">

        <div class="sixteen wide field">

          <div class="fields">

            <div class="six wide field" id="su_namediv">
              <label>Name</label>
              <input type="text" name="su_name" placeholder="Sub Unit Name" id="su_name">
            </div>

            <div class="six wide field" id="su_pudiv">
              <label>Process Unit</label>
              <div class="ui selection dropdown suModalDropDown" id="su_pu">

                <input type="hidden" name="debeType1">
                <i class="dropdown icon"></i>
                <div class="default text">Process Unit</div>

                <div class="menu" id="loadpu_dp">

                </div>

              </div>

            </div>



          </div>
          <div class='fields'>
            <div class="twelve wide field">
              <button class = 'ui blue button' id = 'addSubUnitButton'>Add</button>
              <input class="ui grey button"
                      type="submit" name="submit" value="Save" id = 'saveSubUnitButton'>
              <button class = 'ui green button' id = 'clearSubUnitButton'>Clear</button>
            </div>
          </div>

        </div>

      </div>

    </form>

    <h5 class="ui dividing header" style="color: grey">List of Sub Unit</h5>

    <form class="ui form t1" id="subunitListForm" onsubmit="return false"
          style="overflow:scroll; overflow-x:hidden; height: 200px;">

      <table class="ui celled table">

        <thead>
          <tr>
            <th>Name</th>
            <th>Process Unit</th>
            <th>Delete</th>
          </tr>
        </thead>

        <tbody id="subunitListTable">

        </tbody>

      </table>

    </form>



  </div>

</div>

<div class="actions">

  <input class="ui grey cancel button" type="submit" name="submit" value="Close">

</div>
<?php include './notificationmodal.php'; ?>
<div class="ui small modal" id = "yesnomodalSubunit">
  <i class="close icon"></i>
  <div class="header">
    Alert!
  </div>
  <div class="image content">
    <div>
      <h4 id="question">Are you sure you want to delete?</h4>
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Nope
    </div>
    <div class="ui positive right labeled icon button deny" id = "yesSubUnitButton">
      Yep
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>
<?php include "../views/includefooter2.php" ?>
<script src="../public/javascript/subunitmodal.js"></script>
