<div class="ui small modal" id = "viewPdfModal">
  <i class="close icon"></i>
  <div class="header">
    Set PDF date
  </div>
  <div class="image content">
    <div class='ui grid'>
      <div class='row'>
        <div class='five wide column'>
          <div>
            <label>
              Start Date
            </label>
          </div>
          <input type = 'date' id = 'startDate'>
        </div>
        <div class='five wide column'>
          <div>
            <label>
              End Date
            </label>
          </div>
          <input type = 'date' id = 'endDate'>
        </div>
        <div class='five wide column'>
          <div>
            <label>Status</label>
          </div>
          <div>
            <select id = 'statusFilter'>
              <option value = 'all'>All</option>
              <option value = 'verified'>Verified</option>
              <option value = 'forverification'>For Verification</option>
              <option value = 'pendingactionplan'>Pending Action Plan</option>
              <option value = 'closed'>Closed</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="actions">
    <div class="ui green button" id = 'submitPdfModal'>
      Process
    </div>
    <div class="ui black deny button">
      Close
    </div>
  </div>
</div>
