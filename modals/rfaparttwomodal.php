<div class="ui small modal" id = "rfaparttwomodal">
  <i class="close icon"></i>
  <div class="header">
    RFA Action Plan
  </div>
  <div class="image content">
    <div class='ui container' id = 'partTwoContainer'>
        <div class='ui grid'>
          <div class='row'>
            <div class='sixteen wide column'><label>Root Cause(s)/Possible Cause(s):</label></textarea></div>
            <div class='sixteen wide column'><textarea id = 'rootCausesTextArea' class='ui fluid mywidth' rows="3"></textarea></div>
          </div>
          <div class='row'>
            <div class='sixteen wide column'><label>Mitigating/Immediate Action:</label></textarea></div>
            <div class='sixteen wide column'><textarea id = 'immedateActionTextArea' class='ui fluid mywidth' rows="3"></textarea></div>
            <div class='eight wide column'>
              <label>Implemented By:</label>
              <div class='ui input'>
                <input type = 'text' id = 'implementedByText' placeholder="Implemented By">
              </div>
            </div>
            <div class='eight wide column'>
              <label>Date:</label>
              <div class="ui input">
                <input type = 'date' id = 'implementedByDate' value='<?php echo date("Y-m-d");?>' readonly>
              </div>
            </div>
          </div>
          <div class='row'>
            <div class='sixteen wide column'><label>Permanent Corrective/Preventive Action:</label></textarea></div>
            <div class='sixteen wide column'><textarea id = 'preventiveActionTextArea' class='ui fluid mywidth' rows="3"></textarea></div>
            <div class='eight wide column'>
              <label>Implemented By:</label>
              <div class='ui input'>
                <input type = 'text' id = 'preventiveActionText' placeholder="Implemented By">
              </div>
            </div>
            <div class='eight wide column'>
              <label>Date:</label>
              <div class="ui input">
                <input type = 'date' id = 'preventiveActionDate' value='<?php echo date("Y-m-d");?>' readonly>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Cancel
    </div>
    <!--div class="ui positive right labeled icon button deny" id = "saveRFAPartTwo">
      Update
      <i class="checkmark icon"></i>
    </div-->
    <div class="ui positive right labeled icon button deny" id = "processRFAPartTwo">
      Process
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>
