<div class="ui small modal" id = "notificationmodal">
  <i class="close icon"></i>
  <div class="header">
    Notification
  </div>
  <div class="image content">
    <div id="notification"></div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Close
    </div>
  </div>
</div>
