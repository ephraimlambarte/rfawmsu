<div class="ui small modal" id = "rfapartfourmodal">
  <i class="close icon"></i>
  <div class="header">
    RFA Close Out
  </div>
  <div class="image content">
    <div class='ui container' id = 'partFourContainer'>
      <div class='ui grid'>
        <div class='row'>
          <div class='eight wide column'>
            <input type = 'radio' value='1' id = 'satisfactoryPartFour' name='satisfactoryPartFour'>Satisfactory
          </div>
          <div class='eight wide column'>
              <input type = 'radio' value='0' id = 'notSatisfactoryPartFour' name='satisfactoryPartFour'>Not Satisfactory
          </div>
        </div>
        <div class='row'>
          <div class='sixteen wide column'><label>Remarks:</label></div>
          <div class='sixteen wide column'>
            <textarea id = 'remarksOnePartFour' class='ui fluid mywidth' rows="3"></textarea>
          </div>
          <div class='eight wide column'>
            <label>Verified By:</label>
            <div class='ui input'>
              <input type = 'text' id  ='verifiedbyPartFour' placeholder="Verified By">
            </div>
          </div>
          <div class='eight wide column'>
            <label>Verified By:</label>
            <div class='ui input'>
              <input type = 'date' id  ='verifiedByDatePartFour' value='<?php echo date("Y-m-d");?>' readonly>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='sixteen wide column'><label>Remarks:</label></div>
          <div class='sixteen wide column'>
            <textarea id = 'remarksTwoPartFour' class='ui fluid mywidth' rows="3"></textarea>
          </div>
          <div class='eight wide column'>
            <label>Validated By:</label>
            <div class='ui input'>
              <input type = 'text' id  ='validatedByPartFour' placeholder="Validated By">
            </div>
          </div>
          <div class='eight wide column'>
            <label>Validated By:</label>
            <div class='ui input'>
              <input type = 'date' id  ='validatedByDatePartFour' value='<?php echo date("Y-m-d");?>' readonly>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Cancel
    </div>
    <!--div class="ui right positive labeled icon button" id = "saveRFAPartFour">
      Update
      <i class="checkmark icon"></i>
    </div-->
    <div class="ui right green labeled icon button" id = "processRFAPartFour">
      Process
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>
