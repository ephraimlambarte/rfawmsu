<div class="ui small modal" id = "rfapartthreemodal">
  <i class="close icon"></i>
  <div class="header">
    RFA Verification
  </div>
  <div class="image content">
    <div class='ui container' id = 'partThreeContainer'>
      <div class='ui grid'>
        <div class='row'>
          <div class='sixteen wide column'>
            <label>Details:</label>
          </div>
          <div class='sixteen wide column'>
            <textarea id = 'detailesRFAParthThreeText' class='ui fluid mywidth' rows="3"></textarea>
          </div>
        </div>
        <div class='row'>
          <div class='eight wide column'>
            <input type='radio' name = 'satisfactory' id ='satisfactory'>Satisfactory
          </div>
          <div class='eight wide column'>
            <input type='radio' name = 'satisfactory' id ='notsatisfactory'>Not Satisfactory
          </div>
        </div>
        <div class='row'>
          <div class='eight wide column'>
            <label>Verifed By:</label>
            <div class='ui input'>
              <input type = 'text' id = 'verifiedByRFAPartThree' placeholder="Verified By" list = 'auditors'>
              <datalist id="auditors">

              </datalist>
            </div>
          </div>
          <div class='eight wide column'>
            <label>Date:</label>
            <div class="ui input">
              <input type = 'date' id = 'verifiedByDate' value='<?php echo date("Y-m-d");?>' >
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='sixteen wide column' id = 'tableRfaPartThree' style="height:200px; overflow:scroll;">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Cancel
    </div>
    <button class="ui right blue labeled icon button" id = "saveRFAPartThree">
      Update
      <i class="checkmark icon"></i>
    </button>
    <button class="ui right green labeled icon button" id = "processRFAPartThree">
      Process
      <i class="checkmark icon"></i>
    </button>
  </div>
</div>
