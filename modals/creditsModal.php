<div class="ui basic modal">
  <div class="ui icon header">
    <h2>This project is made possible by the following people.</h2>
  </div>
  <div class="content">
    <div >
        <p>Ivy Niezel Araneta - System Analyt / Archivist</p>
    </div>
    <div style = 'margin-top:30px;'>
        <p>Dameston A. Morla - System Tester/ Asst. Programmer</p>
    </div>
    <div style = 'margin-top:30px;'>
        <p>Neil Ivan R. Nava - Programmer / Asst. System Tester</p>
    </div>
  </div>
  <div class="actions">
  </div>
</div>