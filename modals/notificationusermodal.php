<div class="ui small modal" id = "notificationusermodal">
  <i class="close icon"></i>
  <div class="header">
    Notifications
  </div>
  <div class="image content">
    <div class='ui container' id ='notificationusermodalContainer' style='height:300px; overflow:scroll;'>

    </div>
  </div>
  <div class="actions">
    <div class="ui positive right labeled icon button deny" id = "yesButton">
      Ok
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>
<!--script src="../public/javascript/notificationusermodal.js"></script-->
