<?php
  class model {
    var $handler;
    function connectDatabase(){
      $server = 'mysql:host=127.0.0.1;dbname=dbrfa';
      $username = 'root';
      $password = '';
      try{
        $this->handler=new PDO($server,$username,$password);
        $this->handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      }catch(PDOexception $e){
          die("sorry, database problem.");
      }
    }
    function getUserIDByUsername($username){
      $sql = "SELECT accountid FROM tblaccount WHERE username = :username";
      $query = $this->handler->prepare($sql);
      $query->execute(array(
        ':username' => $username
      ));
      $row = $query->fetch();
      return $row['accountid'];
    }
    function getAccNameById($id){
      $sql = "SELECT firstname, lastname FROM tblaccount WHERE accountid = :id";
      $query = $this->handler->prepare($sql);
      $query->execute(array(
          ':id' => $id
      ));
      $row = $query->fetch();
      return $row['lastname'].', '.$row['firstname'];
    }
    function checkIfUsernameExist($username){
      $sql = "SELECT * FROM tblaccount WHERE username = :username";
      $query = $this->handler->prepare($sql);
      $query->execute(array(
        ':username' =>$username
      ));
      if ($query->rowCount()>0){
        return true;
      }else {
        return false;
      }
    }
    function getAdminID(){
      $sql = "SELECT accountid FROM tblaccount WHERE usertype = 'Admin'";
      $this->connectDatabase();
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['accountid'];
    }
    function getNoProcessUnitOfCoreUnit($coreunitid){
      $sql ="SELECT COUNT(coreunitid) AS cuid FROM tblprocessunit WHERE coreunitid = '".$coreunitid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['cuid'];
    }
    function getCoreUnitName($id){
      $sql = "SELECT cuname FROM tblcoreunit WHERE coreunitid = '".$id."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['cuname'];
    }
    function getProcessUnitName($id){
      $sql = "SELECT puname FROM tblprocessunit WHERE processunitid = '".$id."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['puname'];
    }
    function getNoSubUnitProcessUnit($processunitid){
      $sql = "SELECT COUNT(processunitid) AS puid FROM tblsubunit WHERE processunitid = '".$processunitid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['puid'];
    }
    function loadProcessUnitToDropBox(){
      $sql = "SELECT puname, processunitid FROM tblprocessunit";

      $result = $this->handler->query($sql);

      if($result->rowCount() > 0){

        while($row = $result->fetch()){

          echo "<div class='item' data-value=\"".$row['processunitid']."\">".$row['puname']."</div>";

        }

      }
    }
    function getYear(){
      date_default_timezone_set('Asia/Manila');
      $year=strftime('%Y');
      return $year;
    }
    function updateControlNo($newid, $id){
      $sql = "UPDATE tblrfapartone SET rfacontrolno = '".$newid."' WHERE rfapartoneid ='".$id."'";
      if($this->handler->query($sql)){
        return true;
      }else{
        return false;
      }
    }
    function generateControlNumber($id){
      if($id<10){
        return $this->getYear()."-"."00".$id;
      }elseif ($id<100 && $id>10) {
        return $this->getYear()."-"."0".$id;
      }elseif ($id<1000 && $id>100) {
        return $this->getYear()."-"."".$id;
      }elseif ($id<10000 && $id>1000){
        return $this->getYear()."-"."".$id;
      }elseif ($id<100000 && $id>10000) {
        return $this->getYear()."-"."".$id;
      }elseif ($id<1000000 && $id>100000) {
        return $this->getYear()."-".$id;
      }
    }
    function insertNotification($message, $userid, $rfaid){
      $sql = "INSERT INTO notification(message, accountid, rfapartoneid) VALUES('".$message."', '".$userid."', '".$rfaid."')";
      try {
        if($this->handler->query($sql)){
          return true;
        }else{
          return false;
        }
      } catch (PDOException $e) {
        return false;
      }
    }
    function checkIfRFAClosed($rfaid){
      /*$sql = "SELECT * FROM tblrfaparttwo WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      if($query->rowCount()>0){
        $row = $query->fetch();
        $sql = "SELECT * FROM tblrfapartthree WHERE rfaparttwoid = '".$row['parttwoid']."'";
        $query = $this->handler->query($sql);
        if($query->rowCount()>0){
          
          $row = $query->fetch();
          $sql = "SELECT * FROM tblrfapartfour WHERE rfapartthreeid = '".$row['rfppartthreeid']."'";
          $query = $this->handler->query($sql);
          if($query->rowCount()>0){
            return true;
          }else{
            return $row['rfppartthreeid'];
          }
        }else{
          return false;
        }
      }else{
        return false;
      }*/
      $sql = "SELECT tblrfapartone.* FROM tblrfapartone INNER JOIN tblrfaparttwo 
      ON tblrfapartone.rfapartoneid = tblrfaparttwo.rfapartoneid INNER JOIN 
      tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
      INNER JOIN tblrfapartfour ON tblrfapartfour.rfapartthreeid = 
      tblrfapartthree.rfppartthreeid
      WHERE tblrfapartone.rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      if($query->rowCount()>0){
        return true;
      }else{
        return false;
      }
    }
    function getUserType($userid){
      $sql = "SELECT usertype FROM tblaccount WHERE accountid = '".$userid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['usertype'];
    }
    function getDate(){
      date_default_timezone_set('Asia/Manila');
     $year=strftime('%Y');
     $month=strftime('%b');
     $day=strftime('%d');
     $date=$day.'-'.$month.'-'.$year;
     return $date;
    }
    function getRFAPartOneCoreUnit($rfaid){
      $sql = "SELECT tblcoreunit.cuname FROM tblrfapartone INNER JOIN tblcoreunit ON
              tblcoreunit.coreunitid = tblrfapartone.coureunitid WHERE tblrfapartone.rfapartoneid
              ='".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['cuname'];
    }
    function getProcessUnitIDByRFAID($rfaid){
      $sql = "SELECT tblprocessunit.puname FROM tblrfapartone INNER JOIN tblprocessunit ON
              tblprocessunit.processunitid = tblrfapartone.processunitid WHERE tblrfapartone.rfapartoneid
              ='".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['puname'];
    }
    function getSubUnitIDByRFAID($rfaid){
      $sql = "SELECT tblsubunit.suname FROM tblrfapartone INNER JOIN tblsubunit ON
              tblsubunit.subunitid = tblrfapartone.subunitid WHERE tblrfapartone.rfapartoneid
              ='".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['suname'];
    }
    function getControlNumberRFA($rfaid){
      $sql = "SELECT rfacontrolno FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfacontrolno'];
    }
    function getActionType($rfaid){
      $sql = "SELECT rfaTypeOfAction FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfaTypeOfAction'];
    }
    function getRequestSource($rfaid){
      $sql = "SELECT rfaRequestSource FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfaRequestSource'];
    }
    function getRequestSpecified($rfaid){
      $sql = "SELECT rfaRequestSourceSpecified FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfaRequestSourceSpecified'];
    }
    function getNonConformity($rfaid){
      $sql = "SELECT rfaNonConformity FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfaNonConformity'];
    }
    function getDescription($rfaid){
      $sql = "SELECT rfaDescription FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfaDescription'];
    }
    function getDueDate($rfaid){
      $sql = "SELECT rfaDateDue FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return date("m-d-Y", strtotime($row['rfaDateDue']));
      //return $row['rfaDateDue'];
    }
    function getDueDateNotFormatted($rfaid){
      $sql = "SELECT rfaDateDue FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfaDateDue'];
    }
    function getRiskLevel($rfaid){
      $sql = "SELECT rfaRiskLevel FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfaRiskLevel'];
    }
    function getIssuedBy($rfaid){
      $sql = "SELECT rfaIssuedBy FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfaIssuedBy'];
    }
    function getConformedBy($rfaid){
      $sql = "SELECT rfaConformedBy FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfaConformedBy'];
    }
    function getAcknowledgedBy($rfaid){
      $sql = "SELECT rfaAcknowledgeBy FROM tblrfapartone WHERE rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfaAcknowledgeBy'];
    }
    function getDataRfaPartTwo($rfaid, $column){
      $sql= "SELECT tblrfaparttwo.".$column." FROM tblrfapartone INNER JOIN tblrfaparttwo ON
            tblrfapartone.rfapartoneid = tblrfaparttwo.rfapartoneid WHERE tblrfapartone.rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row[$column];

    }
    function displayTablePdfPartThree($rfaid){
      $sql = "SELECT tblrfapartthree.details, tblrfapartthree.followupdate, tblrfapartthree.satisfaction,
              tblrfapartthree.verifiedby FROM tblrfapartthree INNER JOIN tblrfaparttwo ON tblrfaparttwo.parttwoid =
              tblrfapartthree.rfaparttwoid
              INNER JOIN tblrfapartone ON tblrfapartone.rfapartoneid =
              tblrfaparttwo.rfapartoneid WHERE tblrfapartone.rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $html = "";
      while($row = $query->fetch()){
        $html.= "<tr>";
          $html.= "<td width='80'>".$row['followupdate']."</td>";
          $html.= "<td width='200'>".$row['details']."</td>";
          if($row['satisfaction']=='1'){
            $html.= "<td><div style = 'padding-left:55px;'><input type = 'checkbox' checked></div></td>";
            $html.= "<td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>";
          }else{
            $html.= "<td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>";
            $html.= "<td><div style = 'padding-left:55px;'><input type = 'checkbox' checked></div></td>";
          }
          $html.= "<td width='100'>".$row['verifiedby']."</td>";
        $html.= "</tr>";
      }
      if($query->rowCount()==1){
        $html.="<tr>
                  <td width='80' height='10'></td>
                  <td width='200'></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td width='100'></td>
                </tr>
                <tr>
                  <td width='80' height='10'></td>
                  <td width='200'></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td width='100'></td>
                </tr>
                <tr>
                  <td width='80' height='10'></td>
                  <td width='200'></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td width='100'></td>
                </tr>
                <tr>
                  <td width='80' height='10'></td>
                  <td width='200'></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td width='100'></td>
                </tr>";
      }elseif($query->rowCount()==2){
        $html.="<tr>
                  <td width='80' height='10'></td>
                  <td width='200'></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td width='100'></td>
                </tr>
                <tr>
                  <td width='80' height='10'></td>
                  <td width='200'></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td width='100'></td>
                </tr>
                <tr>
                  <td width='80' height='10'></td>
                  <td width='200'></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td width='100'></td>
                </tr>";
      }elseif($query->rowCount()==3){
        $html.="<tr>
                  <td width='80' height='10'></td>
                  <td width='200'></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td width='100'></td>
                </tr>
                <tr>
                  <td width='80' height='10'></td>
                  <td width='200'></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td width='100'></td>
                </tr>";
      }elseif($query->rowCount()==4){
        $html.="<tr>
                  <td width='80' height='10'></td>
                  <td width='200'></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td><div style = 'padding-left:55px;'><input type = 'checkbox'></div></td>
                  <td width='100'></td>
                </tr>";
      }
      return $html;
    }
    function displayTablePartThree($rfaid){
      $sql = "SELECT tblrfapartthree.rfppartthreeid, tblrfapartthree.details, tblrfapartthree.followupdate, tblrfapartthree.satisfaction,
              tblrfapartthree.verifiedby FROM tblrfapartthree INNER JOIN tblrfaparttwo ON tblrfaparttwo.parttwoid =
              tblrfapartthree.rfaparttwoid
              INNER JOIN tblrfapartone ON tblrfapartone.rfapartoneid =
              tblrfaparttwo.rfapartoneid WHERE tblrfapartone.rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);

      while($row = $query->fetch()){
        $date_format = new DateTime($row['followupdate']);
        echo "<tr id = 'parththree-".$row['rfppartthreeid']."'>";
          echo "<td width='100'>".$row['followupdate']."</td>";
          echo "<td width='200'>".$row['details']."</td>";
          if($row['satisfaction']=='1'){
            echo "<td>*</td>";
            echo "<td></td>";
          }else{
            echo "<td></td>";
            echo "<td>*</td>";
          }
          echo "<td width='100'>".$row['verifiedby']."</td>";
          echo "<td><div id ='divparthree-".$row['rfppartthreeid']."' class='ui icon button'><i class='delete icon'></i></div></td>";
        echo "</tr>";
      }
    }
    function getDataRFAPartThree($rfaid, $column){
      $sql = "SELECT tblrfapartthree.rfppartthreeid FROM tblrfapartthree INNER JOIN tblrfaparttwo ON tblrfaparttwo.parttwoid =
              tblrfapartthree.rfaparttwoid
              INNER JOIN tblrfapartone ON tblrfapartone.rfapartoneid =
              tblrfaparttwo.rfapartoneid WHERE tblrfapartone.rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rfppartthreeid'];
    }
    function getDataRFAPartFour($rfaid, $column){
      $sql = "SELECT tblrfapartfour.".$column." FROM tblrfapartfour INNER JOIN tblrfapartthree ON tblrfapartthree.rfppartthreeid =
              tblrfapartfour.rfapartthreeid INNER JOIN tblrfaparttwo ON tblrfaparttwo.parttwoid = tblrfapartthree.rfaparttwoid
              INNER JOIN tblrfapartone ON tblrfapartone.rfapartoneid = tblrfaparttwo.rfapartoneid
              WHERE tblrfapartone.rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row[$column];
    }
    function checkIfPartTwoEditable($rfaid){
      $sql = "SELECT tblrfapartthree.details, tblrfapartthree.satisfaction  FROM tblrfapartthree INNER JOIN tblrfaparttwo ON
              tblrfaparttwo.parttwoid = tblrfapartthree.rfaparttwoid INNER JOIN tblrfapartone ON
              tblrfapartone.rfapartoneid = tblrfaparttwo.rfapartoneid
              WHERE tblrfapartone.rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      if($query->rowCount()>0){
        while($row = $query->fetch()){
          if($row['satisfaction']=='1'){
            return false;
          }
        }
        if($query->rowCount()==5){
          return false;
        }else{
          return true;
        }
      }else{
        return true;
      }
    }
    function getRowsPartTwoInPartThree($parttwoid){
      $sql = "SELECT rfppartthreeid FROM tblrfapartthree WHERE rfaparttwoid = '".$parttwoid."'";
      $query = $this->handler->query($sql);
      return $query->rowCount();
    }

    function checkIfSatisfactory($rfaid){
      $sql = "SELECT tblrfapartthree.satisfaction FROM tblrfapartthree INNER JOIN tblrfaparttwo ON tblrfaparttwo.parttwoid =
              tblrfapartthree.rfaparttwoid
              INNER JOIN tblrfapartone ON tblrfapartone.rfapartoneid =
              tblrfaparttwo.rfapartoneid WHERE tblrfapartone.rfapartoneid = '".$rfaid."'";
      $query = $this->handler->query($sql);
      while($row=$query->fetch()){
        if($row['satisfaction']==1){
          return true;
        }
      }
      return false;
    }
    function changeNotificationState($notid){
        $sql = "UPDATE notification SET status = 'Read' WHERE notificationid = '".$notid."'";
        try {
          $this->handler->query($sql);
        } catch (PDOException $e) {

        }
    }
    function checkStateNotification($notid){
      $sql = "SELECT status FROM notification WHERE notificationid ='".$notid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['status'];
    }
    function getNotificationNumber($userid){
      $sql = "SELECT * FROM notification WHERE accountid = '".$userid."' AND status <> 'read'";
      $query = $this->handler->query($sql);
      return $query->rowCount();
    }
    function getLatestRfa($rfaid){
      if($this->getDataRfaPartTwo($rfaid, 'parttwoid')==""){
        return "part2";
      }else{
        if($this->getDataRFAPartThree($rfaid,'aaa')==""){
          return "part3";
        }else{
          return "part4";
        }
      }
    }
    function loadImplementedBy(){
      $sql = "SELECT firstname, lastname FROM tblaccount WHERE usertype = 'Admin' OR usertype = 'Auditor'";
      $query = $this->handler->query($sql);
      while($row = $query->fetch()){
        echo "<div class='item' data-value=".$row['firstname']." ".$row['lastname'].">".$row['firstname']." ".$row['lastname']."</div>";
      }
    }
    function getRowCountDate($year, $month){
      $sql = "SELECT * FROM tblrfapartone WHERE YEAR(rfaDateCreated) = $year AND MONTH(rfaDateCreated) = $month";
      $query = $this->handler->query($sql);
      return $query->rowCount();
    }
    function getRowCountClosedOpenDate($year, $month, $closeoropen){
      $sql = "";
      if($closeoropen == "close"){
        $sql = "SELECT tblrfapartone.* FROM tblrfapartone INNER JOIN tblrfaparttwo 
        ON tblrfapartone.rfapartoneid = tblrfaparttwo.rfapartoneid INNER JOIN 
        tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
        INNER JOIN tblrfapartfour ON tblrfapartfour.rfapartthreeid = 
        tblrfapartthree.rfppartthreeid
        WHERE YEAR(tblrfapartone.rfaDateCreated) = $year AND MONTH(tblrfapartone.rfaDateCreated)=$month";
      }else{
        $sql = "SELECT a.* FROM tblrfapartone a 
        WHERE NOT EXISTS (SELECT b.* FROM tblrfapartone b INNER JOIN tblrfaparttwo 
        ON b.rfapartoneid = tblrfaparttwo.rfapartoneid INNER JOIN 
        tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
         INNER JOIN tblrfapartfour ON tblrfapartfour.rfapartthreeid = tblrfapartthree.rfppartthreeid
                         WHERE a.rfapartoneid = b.rfapartoneid)
        AND 
        YEAR(a.rfaDateCreated) = $year AND MONTH(a.rfaDateCreated)=$month";
      }
      $query = $this->handler->query($sql);
      return $query->rowCount();
    }
    function getRfaPartOneData($column, $id){
      $sql = "SELECT $column FROM tblrfapartone WHERE rfapartoneid = $id";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row[$column];
    }
    function loadCoreUnitToDropBox(){

      $sql = "SELECT cuname,coreunitid FROM tblcoreunit

              ORDER BY cuname ASC";

      $result = $this->handler->query($sql);

      if($result->rowCount() > 0){

        while($row = $result->fetch()){

          echo "<div class='item' data-value=\"".$row['coreunitid']."\">".$row['cuname']."</div>";

        }

      }

    }
    function loadPuDropDown($coreunitid){
      $sql = "SELECT puname, processunitid FROM tblprocessunit WHERE coreunitid = :cuid";
      $query = $this->handler->prepare($sql);
      $query->execute(array(
        ":cuid"=>$coreunitid
      ));
      while($row = $query->fetch()){
        echo "<div class='item' data-value=\"".$row['processunitid']."\">".$row['puname']."</div>";
      }
    }
    function loadSuDropdown($processunit){

      $sql = "SELECT suname, subunitid FROM tblsubunit WHERE processunitid = :puid";
      $query = $this->handler->prepare($sql);
      $query->execute(array(
        ":puid"=>$processunit
      ));
      while($row = $query->fetch()){
        echo "<div class='item' data-value=".$row['subunitid'].">".$row['suname']."</div>";
      }
    }
    function loadConformedBy(){

      $sql = "SELECT firstname,lastname,accountid FROM tblaccount
              WHERE status = '1'
              AND (usertype = 'Auditee' OR usertype='Admin')";

      $result = $this->handler->query($sql);

      if($result->rowCount() > 0){

        while($row = $result->fetch()){

          echo "<div class='item' data-value=\"".$row['accountid']."\">".$row['lastname'].", ".$row['firstname']."</div>";

        }

      }
    }
    function getMilitaryTime(){
       date_default_timezone_set('Asia/Manila');
       $time=date("h:i:s:a");
       $ctime = explode(":", $time);
       if($ctime[3]=="pm"){
         if($ctime[0]!=12){
           $ctime[0]=$ctime[0]+12;
         }
       }
       $mtime = $ctime[0].":".$ctime[1].":".$ctime[2];
       return $mtime;
     }
    function get_time_difference($time1, $time2){
      $time1 = strtotime("1/1/1980 $time1");
      $time2 = strtotime("1/1/1980 $time2");

      if ($time2 < $time1)
      {
          $time2 = $time2 + 86400;
      }
      return ($time2 - $time1) / 60;
    }
    function getactualdate(){
      date_default_timezone_set('Asia/Manila');
      $year=strftime('%Y');
      $month=strftime('%m');
      $day=strftime('%d');
      $date= $year.'-'.$month.'-'.$day;
      return $date;
    }
    function updateTimeLogged($userid){
      $sql = "UPDATE session_table SET datelogged = CURDATE(), timelogged = NOW() WHERE sessionid = '".$userid."'";
      $this->handler->query($sql);
      //$this->sendNotifRfa($userid);
    }
    function logoutacc($userid){
      $sql = "DELETE FROM session_table WHERE sessionid = '".$userid."'";
      $this->handler->query($sql);
    }
    function getRootCause($rfapartoneid){
      $sql = "SELECT rootcauses FROM tblrfaparttwo WHERE rfapartoneid = '".$rfapartoneid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return $row['rootcauses'];
    }
    function sendNotifRfa($sessionid){
      $sql = "";
      if($this->getUserType($sessionid)=="Admin"){
        $sql = "SELECT o.* FROM tblrfapartone o INNER JOIN tblrfaparttwo t ON
        o.rfapartoneid = t.rfapartoneid INNER JOIN tblrfapartthree h ON t.parttwoid
         = h.rfaparttwoid WHERE NOT EXISTS(SELECT * FROM tblrfapartfour f WHERE
           f.rfapartthreeid = h.rfppartthreeid) AND DATEDIFF(o.rfaDateDue,'".$this->getactualdate()."')
           <= 14 AND DATEDIFF(o.rfaDateDue, '".$this->getactualdate()."') > 0 GROUP BY  o.rfapartoneid";
      }elseif ($this->getUserType($sessionid)=="Auditor") {
        $sql = "SELECT o.* FROM tblrfapartone o INNER JOIN tblrfaparttwo t ON
        o.rfapartoneid = t.rfapartoneid INNER JOIN tblrfapartthree h ON t.parttwoid
         = h.rfaparttwoid WHERE NOT EXISTS(SELECT * FROM tblrfapartfour f WHERE
           f.rfapartthreeid = h.rfppartthreeid) AND DATEDIFF(o.rfaDateDue,'".$this->getactualdate()."')
           <= 14 AND DATEDIFF(o.rfaDateDue, '".$this->getactualdate()."') > 0 AND o.rfaIssuedBy = $sessionid
           GROUP BY  o.rfapartoneid";
      }elseif ($this->getUserType($sessionid)=="Auditee") {
        $sql = "SELECT o.* FROM tblrfapartone o INNER JOIN tblrfaparttwo t ON
        o.rfapartoneid = t.rfapartoneid INNER JOIN tblrfapartthree h ON t.parttwoid
         = h.rfaparttwoid WHERE NOT EXISTS(SELECT * FROM tblrfapartfour f WHERE
           f.rfapartthreeid = h.rfppartthreeid) AND DATEDIFF(o.rfaDateDue, '".$this->getactualdate()."')
           <= 14 AND DATEDIFF(o.rfaDateDue, '".$this->getactualdate()."') > 0 AND o.rfaConformedBy = $sessionid
           GROUP BY  o.rfapartoneid";
      }elseif($this->getUserType($sessionid)=="SubAdmin"){
        $sql = "SELECT o.* FROM tblrfapartone o INNER JOIN tblrfaparttwo t ON
        o.rfapartoneid = t.rfapartoneid INNER JOIN tblrfapartthree h ON t.parttwoid
         = h.rfaparttwoid WHERE NOT EXISTS(SELECT * FROM tblrfapartfour f WHERE
           f.rfapartthreeid = h.rfppartthreeid) AND DATEDIFF(o.rfaDateDue,'".$this->getactualdate()."')
           <= 14 AND DATEDIFF(o.rfaDateDue, '".$this->getactualdate()."') > 0 AND o.rfaAcknowledgeBy = $sessionid
           GROUP BY  o.rfapartoneid";
      }
      $query = $this->handler->query($sql);
      if($query->rowCount()>0){
        $sql = "";
        while($row = $query->fetch()){
          $sql .= "INSERT INTO notification(accountid, rfapartoneid, message) VALUES('".$sessionid."', '".$row['rfapartoneid']."',
          'RFA with control no: ".$row['rfacontrolno']." is not yet processed and its due date is within less than 2 weeks away!'); ";
        }
        $this->handler->query($sql);
      }
      
    }
    function checkIfDueDate($rfapartoneid){
      $sql = "SELECT DATEDIFF(rfaDateDue, '".$this->getactualdate()."') AS mydate FROM tblrfapartone WHERE rfapartoneid = '".$rfapartoneid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      if($row['mydate']<0){
        return true;
      }else{
        return false;
      }
    }
    function getAuditors(){
      $sql = "SELECT firstname, lastname FROM tblaccount WHERE usertype = 'Auditor'";
      $query = $this->handler->query($sql);
      while($row = $query->fetch()){
          echo "<option value = '".$row['lastname'].", ".$row['firstname']."'>";
      }
    }
    function loadDesignationsOnDropDown(){
      $sql = "SELECT designationID, designationName FROM designationstable";
      $result = $this->handler->query($sql);
      
      if($result->rowCount() > 0){

        while($row = $result->fetch()){

          echo "<div class='item' data-value='".$row['designationID']."'>".$row['designationName']."</div>";

        }

      }
    }
    function insertUserDesignation($userid, $designationid){
      $sql = "INSERT INTO designationhistory (accountid, designationid, dateAssigned) VALUES
              (:accountid, :designationid, NOW())";
      $query = $this->handler->prepare($sql);
      if($query->execute(array(
        ':accountid' => $userid,
        ':designationid' => $designationid
      ))){
        return true;
      }else{
        return false;
      }
    }
    function deleteUserInDesignation($userid){
      $sql = "DELETE FROM designationhistory WHERE accountid = :accountid";
      $query = $this->handler->prepare($sql);
      if($query->execute(array(
        ':accountid' => $userid,
      ))){
        return true;
      }else{
        return false;
      }
    }
    function getDesignationOfUser($userid){
      $sql = "SELECT designationstable.designationName AS name FROM 
      designationhistory INNER JOIN designationstable 
      ON designationstable.designationID = 
      designationhistory.designationid WHERE 
      designationhistory.accountid = '".$userid."'";
      $query = $this->handler->query($sql);
      $row = $query->fetch();
      return '('.$row['name'].')';
    }
  }
?>
