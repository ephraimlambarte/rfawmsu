<!DOCTYPE html>

<html>

  <head>

    <title>Login</title>

    <?php
      include "./views/includeheader.php";
    ?>
    <style>

      body {
        background-color: #DADADA;
      }
      body > .grid {
        height: 100%;
      }
      .image {
        margin-top: -100px;
      }
      .column {
        max-width: 450px;
      }
      a:hover{
        cursor:pointer;
      }
    </style>

  </head>

  <body>

    <div class="ui middle aligned center aligned grid">
      <div class="column">

        <h2 class="ui teal image header">

          <div class="content">
            <img src="./public/images/logo.png" height="720" width="420">
          </div>
        </h2>

        <form class="ui large form error" id = "loginform">

          <div class="ui stacked segment">

            <div class="field">
              <div class="ui left icon input">
                <i class="user icon"></i>
                <input type="text" name="username" placeholder="username" id="acc_un">
              </div>
            </div>

            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" name="password" placeholder="password" id="acc_pw">
              </div>
            </div>

            <div id= "login" class="ui fluid large red submit button">Login</div>
            <a style = 'text-decoration:none;' href = './views/forgotpassword.php'>Forgot password</a>
          </div>

        </form>

      </div>
    </div>
    <?php
      include "./views/includefooter.php";
    ?>
    <script src="./public/javascript/index.js"></script>

  </body>

</html>
