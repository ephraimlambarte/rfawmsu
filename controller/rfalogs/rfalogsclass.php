<?php
  include "../../model/model.php";


  class RFALogs{
    var $model;
    private function getRequestSource($type){

      switch ($type) {

        case 7:
          return "Corrective<br/>Action";
          break;
        case 6:
          return "Preventive<br/>Action";
          break;
        case 1:
          return "Internal<br/>Action";
          break;
        case 2:
          return "External<br/>Action";
          break;
        case 3:
          return "Client<br/>Feedback";
          break;
        case 4:
          return "Management<br/>Review";
          break;
        case 5:
          return "Others";
          break;

      }

    }

    private function getTypeOfAction($type){

      switch ($type) {

        case 1:
          return "Corrective<br/>Action";
          break;
        case 2:
          return "Preventive<br/>Action";
          break;

      }

    }

    private function getRiskLevel($type){

      switch ($type) {

        case 1:
          return "Low";
          break;
        case 2:
          return "Medium";
          break;
        case 3:
          return "High";
          break;

      }

    }

    public function loadToTable(){
      $this->model = new model();
      $this->model->connectDatabase();

      $sql = "SELECT * FROM tblrfapartone";

      $result = $this->model->handler->query($sql);

      if($result->rowCount() > 0){

        $counter = 1;

        while($row = $result->fetch()){

          $date_format = new DateTime($row['rfaDateCreated']);

          echo "<tr>";
            echo "<td class='center aligned'>".$counter."</td>";
            echo "<td class='center aligned'>".$row['rfaID']."</td>";
            echo "<td class='center aligned'>".$date_format->format("M j, Y")."</td>";
            echo "<td class='center aligned'>".$this->getRequestSource($row['rfaRequestSource'])."</td>";
            echo "<td class='center aligned'>".$row['rfaNonConformity']."</td>";
            echo "<td class='center aligned'></td>";
            echo "<td class='center aligned'>".$this->getTypeOfAction($row['rfaTypeOfAction'])."</td>";
            echo "<td class='center aligned'>".$this->getRiskLevel($row['rfaRiskLevel'])."</td>";

            if ($row['rfaStatus'] == 1){

              echo "<td class='positive center aligned'>Open</td>";

            }
            else {

              echo "<td class='center aligned'>Close</td>";

            }

          echo "</tr>";

          $counter++;

        }

      }

    }

  }

 ?>
