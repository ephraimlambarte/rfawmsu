<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "UPDATE tblrfapartone SET coureunitid = :coreunit, processunitid = :processunit,
          subunitid = :subunit,
          rfaTypeOfAction = :typeofaction,
          rfaRequestSource = :requestsource,
          rfaRequestSourceSpecified=:rfarequestsourcespecified,
          rfaNonConformity = :nonconformity,
          rfaDescription = :description,
          rfaRiskLevel = :risklevel,
          rfaConformedBy = :conformedby,
          rfaDateDue = :duedate WHERE rfapartoneid = :partoneid";
  try {
    $query = $model->handler->prepare($sql);
    if($_POST['requestsource2']=="specify"){
      if($query->execute(array(
          ":coreunit" => $_POST['coreunit'],
          ":processunit" => $_POST['processunit'],
          ":subunit" => $_POST['subunit'],
          ":typeofaction" => $_POST['actiontype'],
          ":requestsource" => "",
          ":rfarequestsourcespecified" => $_POST['requestsource'],
          ":nonconformity" => $_POST['nonconformity'],
          ":description" => $_POST['description'],
          ":risklevel" => $_POST['risklevel'],
          ":conformedby" => $_POST['conformedby'],
          ":partoneid" => $_POST['rfaid'],
          ":duedate" =>$_POST['rfaDateDue']
      ))){
        echo "success";
      }else{
        echo "error";
      }
    }else{
      if($query->execute(array(
          ":coreunit" => $_POST['coreunit'],
          ":processunit" => $_POST['processunit'],
          ":subunit" => $_POST['subunit'],
          ":typeofaction" => $_POST['actiontype'],
          ":requestsource" => $_POST['requestsource'],
          ":rfarequestsourcespecified" => "",
          ":nonconformity" => $_POST['nonconformity'],
          ":description" => $_POST['description'],
          ":risklevel" => $_POST['risklevel'],
          ":conformedby" => $_POST['conformedby'],
          ":partoneid" => $_POST['rfaid'],
          ":duedate" =>$_POST['rfaDateDue']
      ))){
        echo "success";
      }else{
        echo "error";
      }
    }

  } catch (PDOexception $e) {
    echo "error";
  }

?>
