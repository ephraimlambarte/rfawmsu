<?php
  session_start();
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $model->updateTimeLogged($_SESSION['user']);
  $rfapartthreeid = $model->getDataRFAPartThree($_POST['rowid'], 'hahah');
  if($_SESSION['user']!=$model->getAcknowledgedBy($_POST['rowid']) && $model->getUserType($_SESSION['user']) != 'Admin'){
    echo "This rfa is not assigned to you for processing!";
    return;
  }
  if($rfapartthreeid==""){
    echo "<h3>This rfa have not yet been processed for rfa part 3!</h3>";
    return;
  }
  if($model->checkIfDueDate($_POST['rowid'])== true){
    echo "This rfa have already exceeded due date!";
    return;
  }
  $sql ="INSERT INTO tblrfapartfour (satisfaction, verifiedby, remarks1,
        date1, validatedby, remarks2, date2, rfapartthreeid)
        VALUES(:satisfaction, :verifiedby, :remarks1, :date1, :validatedby, :remarks2, :date2, '".$rfapartthreeid."')";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":satisfaction"=>$_POST['satisfactory'],
      ":verifiedby"=>$_POST['verifiedby'],
      ":remarks1"=>$_POST['remarks1'],
      ":date1"=>$_POST['date1'],
      ":validatedby"=>$_POST['validatedby'],
      ":remarks2"=>$_POST['remarks2'],
      ":date2"=>$_POST['date2']
      ))){
        echo "<h3>Successfully saved!</h3>";
    }else{
      echo "<h3>Something went wrong. Please try again later!</h3>";
    }
  } catch (PDOException $e) {
    echo "<h3>Something went wrong. Please try again later!</h3>";
  }

?>
