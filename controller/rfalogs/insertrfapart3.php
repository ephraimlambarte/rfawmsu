<?php
  session_start();
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $model->updateTimeLogged($_SESSION['user']);
  if($_SESSION['user']!=$model->getConformedBy($_POST['rfaid']) && $model->getUserType($_SESSION['user']) != 'Admin'){
    echo "This rfa is not assigned to you for processing!";
    return;
  }
  if($model->checkIfDueDate($_POST['rfaid'])== true){
    echo "This rfa have already exceeded due date!";
    return;
  }
  $sql = "INSERT INTO tblrfaparttwo (rfapartoneid, immediateaction, implementedby1,
          implementedby2, date1, date2, preventiveaction,
          rootcauses) VALUES(:rfapartoneid, :immediateaction, :implementedby1, :implementedby2, :date1, :date2,
          :preventiveaction, :rootcauses)";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":immediateaction"=> $_POST['immediateaction'],
      ":implementedby1" => $_POST['implementedby'],
      ":implementedby2" => $_POST['implementedby2'],
      ":date1" => $_POST['date1'],
      ":date2" => $_POST['date2'],
      ":preventiveaction" => $_POST['preventiveaction'],
      ":rootcauses" => $_POST['rootcauses'],
      ":rfapartoneid" =>$_POST['rfaid']
    ))){
      $model->insertNotification("An RFA with Control no: ".$model->getControlNumberRFA($_POST['rfaid'])." have been issued to you for part 2 by ".$model->getAccNameById($_SESSION['user']), $model->getIssuedBy($_POST['rfaid']), $_POST['rfaid']);
      echo "<h3>Succesfully processed!</h3>";
    }else{
      echo "<h3>Something went wrong, please try again later!</h3>";
    }
  } catch (PDOException $e) {
    echo "<h3>Something went wrong, please try again later!</h3>";
  }
?>
