<?php
  session_start();
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $model->updateTimeLogged($_SESSION['user']);
  $rfapartfourid = $model->getDataRFAPartFour($_POST['rowid'], 'rfapartfourid');
  if($_SESSION['user']!=$model->getAcknowledgedBy($_POST['rowid']) && $model->getUserType($_SESSION['user']) != 'Admin'){
    echo "This rfa is not assigned to you for processing!";
    return;
  }
  $sql = "UPDATE tblrfapartfour SET satisfaction = :satisfaction, verifiedby=:verifiedby, remarks1 = :remarks1,
        date1=:date1, validatedby=:validatedby, remarks2 = :remarks2, date2 = :date2 WHERE rfapartfourid =
        '".$rfapartfourid."'";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":satisfaction"=>$_POST['satisfactory'],
      ":verifiedby"=>$_POST['verifiedby'],
      ":remarks1"=>$_POST['remarks1'],
      ":date1"=>$_POST['date1'],
      ":validatedby"=>$_POST['validatedby'],
      ":remarks2"=>$_POST['remarks2'],
      ":date2"=>$_POST['date2']
      ))){
        echo "<h3>Successfully updated!</h3>";
    }else{
      echo "<h3>Something went wrong. Please try again later!</h3>";
    }
  } catch (PDOException $e) {
    echo "<h3>Something went wrong. Please try again later!</h3>";
  }

?>
