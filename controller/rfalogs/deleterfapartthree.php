<?php
  session_start();
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();

  $model->updateTimeLogged($_SESSION['user']);
  if($_SESSION['user']!=$model->getIssuedBy($_POST['rfaid']) && $model->getUserType($_SESSION['user']) != 'Admin'){
    echo "This rfa is not assigned to you for processing!";
    return;
  }
  if($model->getDataRFAPartFour($_POST['partoneid'], 'rfapartfourid')!=""){
    echo "This rfa have already been processed in part 4 and cannot be deleted!";
    return;
  }
  $sql = "DELETE FROM tblrfapartthree WHERE rfppartthreeid = :id";
  $query= $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":id"=>$_POST['partthreeid']
    ))){
      echo "Success!";
    }else{
      echo "Something went wrong. Please try again later!";
    }
  } catch (PDOException $e) {
    echo "Something went wrong. Please try again later!";
  }

?>
