<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $editable = "";
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  if($model->getDataRfaPartTwo($_POST['rfaid'], 'parttwoid')==""){
    $editable="false";
  }else{
    $editable = "true";
  }
  if($model->checkIfPartTwoEditable($_POST['rfaid'])){
    echo "<div class='ui grid'>
      <div class='row'>
        <input type = 'hidden' id = 'editable' value='".$editable."'>
        <div class='sixteen wide column'><label>Root Cause(s)/Possible Cause(s):</label></textarea></div>
        <div class='sixteen wide column'><textarea id = 'rootCausesTextArea' class='ui fluid mywidth' rows='3'>".$model->getDataRfaPartTwo($_POST['rfaid'], 'rootcauses')."</textarea></div>
      </div>
      <div class='row'>
        <div class='sixteen wide column'><label>Mitigating/Immediate Action:</label></textarea></div>
        <div class='sixteen wide column'><textarea id = 'immedateActionTextArea' class='ui fluid mywidth' rows='3'>".$model->getDataRfaPartTwo($_POST['rfaid'], 'immediateaction')."</textarea></div>
        <div class='eight wide column'>

          <label>Implemented By:</label>
          <div class='ui selection dropdown rfaModalDropDown' id='implementedByText'>
            <input type = 'hidden' value='".$model->getDataRfaPartTwo($_POST['rfaid'], 'implementedby1')."' class='default text' placeholder = '".$model->getDataRfaPartTwo($_POST['rfaid'], 'implementedby1')."'>
            <i class='dropdown icon'></i>
            <div class='default text'>Implemented By</div>

            <div class='menu' id='implementedByTextLoad'>";
              $model->loadImplementedBy();
            echo "</div>

          </div>
        </div>
        <div class='eight wide column'>
          <label>Date:</label>
          <div class='ui input'>";
            if($model->getDataRfaPartTwo($_POST['rfaid'], 'date1')!=""){
              echo "<input type = 'date' id = 'implementedByDate' value='".$model->getDataRfaPartTwo($_POST['rfaid'], 'date1')."'>";
            }else{
              echo "<input type = 'date' id = 'implementedByDate' value='".date("Y-m-d")."' >";
            }
          echo "</div>
        </div>
      </div>
      <div class='row'>
        <div class='sixteen wide column'><label>Permanent Corrective/Preventive Action:</label></textarea></div>
        <div class='sixteen wide column'><textarea id = 'preventiveActionTextArea' class='ui fluid mywidth' rows='3'>".$model->getDataRfaPartTwo($_POST['rfaid'], 'preventiveaction')."</textarea></div>
        <div class='eight wide column'>

          <label>Implemented By:</label>
          <div class='ui selection dropdown rfaModalDropDown' id='preventiveActionText'>
            <input type = 'hidden' value='".$model->getDataRfaPartTwo($_POST['rfaid'], 'implementedby2')."' class='default text' placeholder = '".$model->getDataRfaPartTwo($_POST['rfaid'], 'implementedby2')."'>
            <i class='dropdown icon'></i>
            <div class='default text'>Implemented By</div>

            <div class='menu' id='preventiveActionTextLoad'>";
              $model->loadImplementedBy();
            echo "</div>

          </div>
        </div>
        <div class='eight wide column'>
          <label>Date:</label>
          <div class='ui input'>";
            if($model->getDataRfaPartTwo($_POST['rfaid'], 'date2')!=""){
            echo "<input type = 'date' id = 'preventiveActionDate' value='".$model->getDataRfaPartTwo($_POST['rfaid'], 'date2')."'>";
          }else{
            echo "<input type = 'date' id = 'preventiveActionDate' value='".date("Y-m-d")."' >";
          }
          echo "</div>
        </div>
      </div>
    </div>";
  }else{
    echo "<div class='ui grid'>
      <div class='row'>
      <input type = 'hidden' id = 'editable' value='".$editable."'>
        <div class='sixteen wide column'><label>Root Cause(s)/Possible Cause(s):</label></textarea></div>
        <div class='sixteen wide column'><textarea id = 'rootCausesTextArea' class='ui fluid mywidth' rows='3' disabled>
        ".$model->getDataRfaPartTwo($_POST['rfaid'], 'rootcauses')."</textarea></div>
      </div>
      <div class='row'>
        <div class='sixteen wide column'><label>Mitigating/Immediate Action:</label></textarea></div>
        <div class='sixteen wide column'><textarea id = 'immedateActionTextArea' class='ui fluid mywidth' rows='3' disabled>
        ".$model->getDataRfaPartTwo($_POST['rfaid'], 'immediateaction')."</textarea></div>
        <div class='eight wide column'>
          <label>Implemented By:</label>
          <div class='ui selection dropdown rfaModalDropDown' id='implementedByText'>
            <input type = 'hidden' value='".$model->getDataRfaPartTwo($_POST['rfaid'], 'implementedby1')."' class='default text' placeholder = '".$model->getDataRfaPartTwo($_POST['rfaid'], 'implementedby1')."'>
            <i class='dropdown icon'></i>
            <div class='default text'>Implemented By</div>

            <div class='menu' id='implementedByTextLoad'>";
              $model->loadImplementedBy();
            echo "</div>

          </div>
        </div>
        <div class='eight wide column'>
          <label>Date:</label>
          <div class='ui input'>";
            if($model->getDataRfaPartTwo($_POST['rfaid'], 'date1')!=""){
              echo "<input type = 'date' id = 'implementedByDate' value='".$model->getDataRfaPartTwo($_POST['rfaid'], 'date1')."'>";
            }else{
              echo "<input type = 'date' id = 'implementedByDate' value='".date("Y-m-d")."' >";
            }
          echo "</div>
        </div>
      </div>
      <div class='row'>
        <div class='sixteen wide column'><label>Permanent Corrective/Preventive Action:</label></textarea></div>
        <div class='sixteen wide column'><textarea id = 'preventiveActionTextArea' class='ui fluid mywidth' rows='3' disabled>
        ".$model->getDataRfaPartTwo($_POST['rfaid'], 'preventiveaction')."</textarea></div>
        <div class='eight wide column'>
          <label>Implemented By:</label>
          <div class='ui selection dropdown rfaModalDropDown' id='preventiveActionText'>
            <input type = 'hidden' value='".$model->getDataRfaPartTwo($_POST['rfaid'], 'implementedby2')."' class='default text' placeholder = '".$model->getDataRfaPartTwo($_POST['rfaid'], 'implementedby2')."'>
            <i class='dropdown icon'></i>
            <div class='default text'>Implemented By</div>

            <div class='menu' id='preventiveActionTextLoad'>";
              $model->loadImplementedBy();
            echo "</div>

          </div>
        </div>
        <div class='eight wide column'>
          <label>Date:</label>
          <div class='ui input'>
            <input type = 'date' id = 'preventiveActionDate' value='".$model->getDataRfaPartTwo($_POST['rfaid'], 'date2')."' disabled>
          </div>
        </div>
      </div>
    </div>";
  }
?>
