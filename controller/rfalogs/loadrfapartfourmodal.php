<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  if($model->getDataRFAPartFour($_POST['rfaid'], 'rfapartfourid')==""){
    echo "<input type = 'hidden' id='rfapartfoureditable' value='noteditable'>";
  }else{
    echo "<input type = 'hidden' id='rfapartfoureditable' value='editable'>";
  }
  echo "<div class='ui grid'>
      <div class='row'>";
  if($model->checkIfSatisfactory($_POST['rfaid'])==true){
    echo "<div class='eight wide column'>
            <input type = 'radio' value='1' id = 'satisfactoryPartFour' name='satisfactoryPartFour' checked disabled>Satisfactory
          </div>
          <div class='eight wide column'>
            <input type = 'radio' value='0' id = 'notSatisfactoryPartFour' name='satisfactoryPartFour' disabled>Not Satisfactory
          </div>";
  }else{
    echo "<div class='eight wide column'>
            <input type = 'radio' value='1' id = 'satisfactoryPartFour' name='satisfactoryPartFour' disabled>Satisfactory
          </div>
          <div class='eight wide column'>
            <input type = 'radio' value='0' id = 'notSatisfactoryPartFour' name='satisfactoryPartFour' checked disabled>Not Satisfactory
          </div>";
  }

  echo "</div>
      <div class='row'>
        <div class='sixteen wide column'><label>Remarks:</label></div>
        <div class='sixteen wide column'>
          <textarea id = 'remarksOnePartFour' class='ui fluid mywidth' rows='3'>".$model->getDataRFAPartFour($_POST['rfaid'], 'remarks1')."</textarea>
        </div>
        <div class='eight wide column'>
          <label>Verified By:</label>
          <div class='ui input'>";
            if($model->getDataRFAPartFour($_POST['rfaid'], 'verifiedby')!= ""){
              echo "<input type = 'text' list = 'myauditors' id  ='verifiedbyPartFour' placeholder='Verified By' value='".$model->getDataRFAPartFour($_POST['rfaid'], 'verifiedby')."'>";
            }else{
              echo "<input type = 'text' list ='myauditors' id  ='verifiedbyPartFour' placeholder='Verified By' value='".$model->getAccNameById($model->getIssuedBy($_POST['rfaid']))."'>";
            }
            echo "<datalist id = 'myauditors'>";
              $model->getAuditors();
            echo "</datalist>";
          echo "</div>
        </div>
        <div class='eight wide column'>
          <label>Verified By:</label>
          <div class='ui input'>";
            if($model->getDataRFAPartFour($_POST['rfaid'], 'date1')!=""){
            echo "<input type = 'date' id  ='verifiedByDatePartFour' value='".$model->getDataRFAPartFour($_POST['rfaid'], 'date1')."'>";
          }else{
            echo "<input type = 'date' id  ='verifiedByDatePartFour' value='".date("Y-m-d")."'>";
          }
          echo "</div>
        </div>
      </div>
      <div class='row'>
        <div class='sixteen wide column'><label>Remarks:</label></div>
        <div class='sixteen wide column'>
          <textarea id = 'remarksTwoPartFour' class='ui fluid mywidth' rows='3'>".$model->getDataRFAPartFour($_POST['rfaid'], 'remarks2')."</textarea>
        </div>
        <div class='eight wide column'>
          <label>Validated By:</label>
          <div class='ui input'>
            <input type = 'text' id  ='validatedByPartFour' placeholder='Validated By' value='".$model->getDataRFAPartFour($_POST['rfaid'], 'validatedby')."'>
          </div>
        </div>
        <div class='eight wide column'>
          <label>Validated By:</label>
          <div class='ui input'>";
          if($model->getDataRFAPartFour($_POST['rfaid'], 'date2')!=""){
            echo "<input type = 'date' id  ='validatedByDatePartFour' value='".$model->getDataRFAPartFour($_POST['rfaid'], 'date2')."'>";
          }else{
            echo "<input type = 'date' id  ='validatedByDatePartFour' value='".date("Y-m-d")."'>";
          }
          echo "</div>
        </div>
      </div>
    </div>";
?>
