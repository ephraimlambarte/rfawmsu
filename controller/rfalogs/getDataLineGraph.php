<?php

  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $myobj = new stdClass();

  $myobj->January = $model->getRowCountDate($model->getYear(), 1);
  $myobj->February = $model->getRowCountDate($model->getYear(), 2);
  $myobj->March = $model->getRowCountDate($model->getYear(), 3);
  $myobj->April = $model->getRowCountDate($model->getYear(), 4);
  $myobj->May = $model->getRowCountDate($model->getYear(), 5);
  $myobj->June = $model->getRowCountDate($model->getYear(), 6);
  $myobj->July = $model->getRowCountDate($model->getYear(), 7);
  $myobj->August = $model->getRowCountDate($model->getYear(), 8);
  $myobj->September = $model->getRowCountDate($model->getYear(), 9);
  $myobj->October = $model->getRowCountDate($model->getYear(), 10);
  $myobj->November = $model->getRowCountDate($model->getYear(), 11);
  $myobj->December = $model->getRowCountDate($model->getYear(), 12);
  $myobj->janopen = $model->getRowCountClosedOpenDate($model->getYear(), 1, "open");
  $myobj->janclose = $model->getRowCountClosedOpenDate($model->getYear(), 1, "close");
  $myobj->febopen = $model->getRowCountClosedOpenDate($model->getYear(), 2, "open");
  $myobj->febclose = $model->getRowCountClosedOpenDate($model->getYear(), 2, "close");
  $myobj->maropen = $model->getRowCountClosedOpenDate($model->getYear(), 3, "open");
  $myobj->marclose = $model->getRowCountClosedOpenDate($model->getYear(), 3, "close");
  $myobj->apropen = $model->getRowCountClosedOpenDate($model->getYear(), 4, "open");
  $myobj->aprclose = $model->getRowCountClosedOpenDate($model->getYear(), 4, "close");
  $myobj->mayopen = $model->getRowCountClosedOpenDate($model->getYear(), 5, "open");
  $myobj->mayclose = $model->getRowCountClosedOpenDate($model->getYear(), 5, "close");
  $myobj->junopen = $model->getRowCountClosedOpenDate($model->getYear(), 6, "open");
  $myobj->junclose = $model->getRowCountClosedOpenDate($model->getYear(), 6, "close");
  $myobj->julopen = $model->getRowCountClosedOpenDate($model->getYear(), 7, "open");
  $myobj->julclose = $model->getRowCountClosedOpenDate($model->getYear(), 7, "close");
  $myobj->augopen = $model->getRowCountClosedOpenDate($model->getYear(), 8, "open");
  $myobj->augclose = $model->getRowCountClosedOpenDate($model->getYear(), 8, "close");
  $myobj->sepopen = $model->getRowCountClosedOpenDate($model->getYear(), 9, "open");
  $myobj->sepclose = $model->getRowCountClosedOpenDate($model->getYear(), 9, "close");
  $myobj->octopen = $model->getRowCountClosedOpenDate($model->getYear(), 10, "open");
  $myobj->octclose = $model->getRowCountClosedOpenDate($model->getYear(), 10, "close");
  $myobj->novopen = $model->getRowCountClosedOpenDate($model->getYear(), 11, "open");
  $myobj->novclose = $model->getRowCountClosedOpenDate($model->getYear(), 11, "close");
  $myobj->decopen = $model->getRowCountClosedOpenDate($model->getYear(), 12, "open");
  $myobj->decclose = $model->getRowCountClosedOpenDate($model->getYear(), 12, "close");
  $myjson = json_encode($myobj);
  echo $myjson;
?>
