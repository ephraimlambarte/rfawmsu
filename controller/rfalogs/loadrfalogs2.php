<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
    $model->updateTimeLogged($_SESSION['user']);
  $usertype = $model->getUserType($_SESSION['user']);
   $sql = "SELECT * FROM tblrfapartone";
   if($usertype == "Admin"){
     $sql = "SELECT * FROM tblrfapartone WHERE rfaAcknowledgeBy = '".$_SESSION['user']."' ORDER BY rfapartoneid DESC LIMIT ".$_POST['page'].", 5";
   }elseif ($usertype == "Auditee") {
     $sql = "SELECT * FROM tblrfapartone WHERE rfaConformedBy = '".$_SESSION['user']."' ORDER BY rfapartoneid DESC LIMIT ".$_POST['page'].", 5";
   }elseif ($usertype == "Auditor") {
     $sql = "SELECT * FROM tblrfapartone WHERE rfaIssuedBy = '".$_SESSION['user']."' OR rfaConformedBy = '".$_SESSION['user']."' ORDER BY rfapartoneid DESC LIMIT ".$_POST['page'].", 5";
   }elseif ($usertype == "SubAdmin") {
     $sql = "SELECT * FROM tblrfapartone WHERE rfaAcknowledgeBy = '".$_SESSION['user']."' ORDER BY rfapartoneid DESC LIMIT ".$_POST['page'].", 5";
   }
   $query = $model->handler->query($sql);

   echo "<table class='ui celled table'>";
    echo "<thead>";	
      echo "<th>RFA Control No</th>
            <th>Date</th>
            <th>Request</th>
            <th>Noncomformity</th>
            <th>Root Cause</th>
            <th>Type Of Action</th>
            <th>Risk Level</th>
            <th>Status</th>
            <th>Edit</th>
            <th>View Full log</th>";
            if($usertype=="Admin"){
              echo "<th>RFA part 2/3/4</th>";
            }elseif ($usertype=="Auditor") {
              echo "<th>RFA part 2/3</th>";
            }elseif ($usertype=="Auditee") {
              echo "<th>RFA part 2</th>";
            }
    echo "</thead>";
    echo "<tbody>";

      while($row = $query->fetch()){
        $date_format = new DateTime($row['rfaDateCreated']);
        /*if($model->checkIfRFAClosed($row['rfapartoneid'])){
          echo "<tr id = '".$row['rfapartoneid']."'; style= 'background-color:#D3D3D3;'>";
        }else{
          echo "<tr id = '".$row['rfapartoneid']."' style= 'background-color:#D3D3D3;'>";
          /*if($model->getDataRFAPartThree($row['rfapartoneid'], "")!=""){
            echo "<tr id = '".$row['rfapartoneid']."' style= 'background-color:#94DIC7;'>";
          }else{
            if($model->getDataRfaPartTwo($row['rfapartoneid'], "rfapartoneid")!=""){
              echo "<tr id = '".$row['rfapartoneid']."' style= 'background-color:#94BDD1;'>";
            }else{
              echo "<tr id = '".$row['rfapartoneid']."' style= 'background-color:#9E94D1;'>";
            }
          }*/
        
        echo "<tr id = '".$row['rfapartoneid']."'>";
          echo "<td class='center aligned'>".$row['rfacontrolno']."</td>";
          echo "<td class='center aligned'>".$date_format->format("M j, Y")."</td>";
          if($row['rfaRequestSource']!= ""){
            echo "<td class='center aligned'>".$row['rfaRequestSource']."</td>";
          }else{
            echo "<td class='center aligned'>".$row['rfaRequestSourceSpecified']."</td>";
          }
          echo "<td>".$row['rfaNonConformity']."</td>";
          echo "<td>".$model->getRootCause($row['rfapartoneid'])."</td>";
          echo "<td class='center aligned'>".$row['rfaTypeOfAction']."</td>";
          echo "<td class='center aligned'>".$row['rfaRiskLevel']."</td>";
          if($model->checkIfRFAClosed($row['rfapartoneid'])){
            echo "<td class='center aligned' style='background-color:#3498db;'>Closed</td>";
          }else{
            if($model->getDataRFAPartThree($row['rfapartoneid'], "")!=""){
              echo "<td class='center aligned' style= 'background-color:#e74c3c;'>Verified</td>";
            }else{
              if($model->getDataRfaPartTwo($row['rfapartoneid'], "rfapartoneid")!=""){
                echo "<td class='center aligned' style= 'background-color:#e74c3c;'>For Verification</td>";
              }else{
                  echo "<td class='center aligned' style= 'background-color:#e74c3c;'>Pending Action Plan</td>";
              }
            }
          }
            echo "<td><div class='ui icon button editRfaPartOne' id = 'editPartOne-".$row['rfapartoneid']."'>Edit</div></td>";
            echo "<td><div class='ui icon button viewRFA' id = 'view-".$row['rfapartoneid']."'><i class='search icon'></i></div></td>";
          if($usertype=="Admin"){
            //echo "<td><div class='ui icon button editRFAAdmin' id = 'edit-".$row['rfapartoneid']."'><i class='edit icon'></i></div></td>";
            echo "<td><div class='ui icon button editRFAAuditee' id = 'edit-".$row['rfapartoneid']."' data-content='Action Plan'><i class='write square icon'></i></div>";
            echo "<div class='ui icon button editRFAAuditor' id = 'edit-".$row['rfapartoneid']."' data-content='Verification'><i class='share icon'></i></div>";
            echo "<div class='ui icon button editRFAAdmin' id = 'edit-".$row['rfapartoneid']."' data-content='Close Out'><i class='closed captioning icon'></i></div></td>";
          }elseif ($usertype=="Auditor") {
            echo "<td><div class='ui icon button editRFAAuditee' id = 'edit-".$row['rfapartoneid']."' data-content='Action Plan'><i class='write square icon'></i></div>
            <div class='ui icon button editRFAAuditor' id = 'edit-".$row['rfapartoneid']."' data-content='Verification'><i class='share icon'></i></div></td>";
          }elseif ($usertype=="Auditee") {
            echo "<td><div class='ui icon button editRFAAuditee' id = 'edit-".$row['rfapartoneid']."' data-content='Verification'><i class='share icon'></i></div></td>";
          }
        echo "</tr>";
      }
    echo "</tbody>";
  echo "</table>";
?>
