<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  echo "<div class='ui segment'>
          <table class='ui celled table'>
            <thead>
              <tr>
                <th width='100'>Date</th>
                <th width='200'>Details</th>
                <th>Satisfactory</th>
                <th>Not Satisfactory</th>
                <th width='100'>Verified By</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>";
    $model->displayTablePartThree($_POST['rfaid']);
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
?>
