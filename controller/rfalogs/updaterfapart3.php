<?php
  session_start();
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $model->updateTimeLogged($_SESSION['user']);
  if($_SESSION['user']!=$model->getIssuedBy($_POST['partoneid']) && $model->getUserType($_SESSION['user']) != 'Admin'){
    echo "This rfa is not assigned to you for processing!";
    return;
  }
  if($model->getDataRFAPartFour($_POST['partoneid'], 'rfapartfourid')!=""){
    echo "This rfa have already been processed in part 4 and cannot be editted!";
    return;
  }
  $sql = "UPDATE tblrfapartthree SET details = :details, followupdate = :mydate,
        satisfaction = :satisfaction, verifiedby = :verifiedby WHERE rfppartthreeid = '".$_POST['partthreeid']."'";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":details"=>$_POST['details'],
      ":mydate"=>$_POST['date'],
      ":satisfaction"=>$_POST['satisfactory'],
      ":verifiedby"=>$_POST['verifiedby']
    ))){
      $html= "<td>".$_POST['date']."</td><td>".$_POST['details']."</td>";
      if($_POST['satisfactory']=="1"){
        $html.="<td>*</td><td></td>";
      }else{
        $html.="<td></td><td>*</td>";
      }
      $html.="<td>".$_POST['verifiedby']."</td><td><div id ='divparthree-".$_POST['partthreeid']."' class='ui icon button'><i class='delete icon'></i></div></td>";
      echo $html;
    }else{
      echo "Something went wrong. Please try again later!";
    }
  } catch (PDOException $e) {
    echo "Something went wrong. Please try again later!";
  }

?>
