<?php
  session_start();
  include "../../model/model.php"; 
  $model = new model();
  $model->connectDatabase();
  $model->updateTimeLogged($_SESSION['user']);
  $usertype = $model->getUserType($_SESSION['user']);
  $sql = "";
    if($usertype == "Admin"){
      //$sql = "SELECT * FROM tblrfapartone WHERE rfaAcknowledgeBy = '".$_SESSION['user']."' ORDER BY rfapartoneid DESC";
      $sql = "SELECT tblrfapartone.* FROM tblrfapartone INNER JOIN tblcoreunit ON tblcoreunit.coreunitid = tblrfapartone.coureunitid
              INNER JOIN tblprocessunit ON tblprocessunit.processunitid = tblrfapartone.processunitid INNER JOIN tblsubunit ON
              tblsubunit.subunitid = tblrfapartone.subunitid WHERE (tblrfapartone.rfacontrolno LIKE '%".$_POST['search']."%' OR
              tblcoreunit.cuname LIKE '%".$_POST['search']."%' OR tblsubunit.suname LIKE '%".$_POST['search']."%' OR tblprocessunit.puname LIKE '%".$_POST['search']."%'
              OR tblrfapartone.rfaRiskLevel LIKE '%".$_POST['search']."%' OR tblrfapartone.rfaRequestSource LIKE '%".$_POST['search']."%' OR
              tblrfapartone.rfaRequestSourceSpecified LIKE '%".$_POST['search']."%' OR tblrfapartone.rfaTypeOfAction LIKE '%".$_POST['search']."%') AND
              tblrfapartone.rfaAcknowledgeBy = '".$_SESSION['user']."' ORDER BY tblrfapartone.rfapartoneid DESC";

    }elseif ($usertype == "Auditee") {
      //$sql = "SELECT * FROM tblrfapartone WHERE rfaConformedBy = '".$_SESSION['user']."' ORDER BY rfapartoneid DESC";
      $sql = "SELECT tblrfapartone.* FROM tblrfapartone INNER JOIN tblcoreunit ON tblcoreunit.coreunitid = tblrfapartone.coureunitid
              INNER JOIN tblprocessunit ON tblprocessunit.processunitid = tblrfapartone.processunitid INNER JOIN tblsubunit ON
              tblsubunit.subunitid = tblrfapartone.subunitid WHERE (tblrfapartone.rfacontrolno LIKE '%".$_POST['search']."%' OR
              tblcoreunit.cuname LIKE '%".$_POST['search']."%' OR tblsubunit.suname LIKE '%".$_POST['search']."%' OR tblprocessunit.puname LIKE '%".$_POST['search']."%'
              OR tblrfapartone.rfaRiskLevel LIKE '%".$_POST['search']."%' OR tblrfapartone.rfaRequestSource LIKE '%".$_POST['search']."%' OR
              tblrfapartone.rfaRequestSourceSpecified LIKE '%".$_POST['search']."%' OR tblrfapartone.rfaTypeOfAction LIKE '%".$_POST['search']."%') AND
              tblrfapartone.rfaConformedBy = '".$_SESSION['user']."' ORDER BY tblrfapartone.rfapartoneid DESC";
    }elseif ($usertype == "Auditor") {
      //$sql = "SELECT * FROM tblrfapartone WHERE rfaIssuedBy = '".$_SESSION['user']."' ORDER BY rfapartoneid DESC";
      $sql = "SELECT tblrfapartone.* FROM tblrfapartone INNER JOIN tblcoreunit ON tblcoreunit.coreunitid = tblrfapartone.coureunitid
              INNER JOIN tblprocessunit ON tblprocessunit.processunitid = tblrfapartone.processunitid INNER JOIN tblsubunit ON
              tblsubunit.subunitid = tblrfapartone.subunitid WHERE (tblrfapartone.rfacontrolno LIKE '%".$_POST['search']."%' OR
              tblcoreunit.cuname LIKE '%".$_POST['search']."%' OR tblsubunit.suname LIKE '%".$_POST['search']."%' OR tblprocessunit.puname LIKE '%".$_POST['search']."%'
              OR tblrfapartone.rfaRiskLevel LIKE '%".$_POST['search']."%' OR tblrfapartone.rfaRequestSource LIKE '%".$_POST['search']."%' OR
              tblrfapartone.rfaRequestSourceSpecified LIKE '%".$_POST['search']."%' OR tblrfapartone.rfaTypeOfAction LIKE '%".$_POST['search']."%') AND
              tblrfapartone.rfaIssuedBy = '".$_SESSION['user']."' ORDER BY tblrfapartone.rfapartoneid DESC";
    }
    if($_POST['search']=='closed'){
      $sql = "SELECT tblrfapartone.* FROM tblrfapartone INNER JOIN tblrfaparttwo 
            ON tblrfapartone.rfapartoneid = tblrfaparttwo.rfapartoneid INNER JOIN 
            tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
            INNER JOIN tblrfapartfour ON tblrfapartfour.rfapartthreeid = 
            tblrfapartthree.rfppartthreeid
            WHERE tblrfapartone.rfaAcknowledgeBy = '".$_SESSION['user']."' ORDER 
            BY tblrfapartone.rfapartoneid DESC";
    }elseif($_POST['search']=='forverification'){
      $sql = "SELECT a.* FROM tblrfapartone a INNER JOIN tblrfaparttwo 
      ON a.rfapartoneid = tblrfaparttwo.rfapartoneid
      WHERE NOT EXISTS (SELECT b.* FROM tblrfapartone b INNER JOIN tblrfaparttwo 
      ON b.rfapartoneid = tblrfaparttwo.rfapartoneid INNER JOIN 
      tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
                       WHERE a.rfapartoneid = b.rfapartoneid)
      AND 
      a.rfaAcknowledgeBy = '".$_SESSION['user']."' ORDER 
            BY a.rfapartoneid DESC";
    }elseif($_POST['search']=='verified'){
      $sql = "SELECT a.* FROM tblrfapartone a INNER JOIN tblrfaparttwo 
      ON a.rfapartoneid = tblrfaparttwo.rfapartoneid
      INNER JOIN tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
      WHERE NOT EXISTS (SELECT b.* FROM tblrfapartone b INNER JOIN tblrfaparttwo 
      ON b.rfapartoneid = tblrfaparttwo.rfapartoneid INNER JOIN 
      tblrfapartthree ON tblrfapartthree.rfaparttwoid = tblrfaparttwo.parttwoid
       INNER JOIN tblrfapartfour ON tblrfapartfour.rfapartthreeid = tblrfapartthree.rfppartthreeid
                       WHERE a.rfapartoneid = b.rfapartoneid)
      AND 
      a.rfaAcknowledgeBy = '".$_SESSION['user']."' ORDER 
            BY a.rfapartoneid DESC";
    }elseif($_POST['search']=='pendingactionplan'){
      $sql = "SELECT a.* FROM tblrfapartone a 
      WHERE NOT EXISTS (SELECT b.* FROM tblrfapartone b INNER JOIN tblrfaparttwo 
      ON b.rfapartoneid = tblrfaparttwo.rfapartoneid
                       WHERE a.rfapartoneid = b.rfapartoneid)
      AND 
      a.rfaAcknowledgeBy = '".$_SESSION['user']."' ORDER 
            BY a.rfapartoneid DESC";
    }
    $query = $model->handler->query($sql);

    echo "<table class='ui celled table'>";
     echo "<thead>";
       echo "<th>RFA Control No</th>
             <th>Date</th>
             <th>Request</th>
             <th>Noncomformity</th>
             <th>Root Cause</th>
             <th>Type Of Action</th>
             <th>Risk Level</th>
             <th>Status</th>
             <th>View Full log</th>";
             if($usertype=="Admin"){
               echo "<th>RFA part 2/3/4</th>";
             }elseif ($usertype=="Auditor") {
               echo "<th>RFA part 3</th>";
             }elseif ($usertype=="Auditee") {
               echo "<th>RFA part 2</th>";
             }
     echo "</thead>";
     echo "<tbody>";

     while($row = $query->fetch()){
      $date_format = new DateTime($row['rfaDateCreated']);
      //if($model->checkIfRFAClosed($row['rfapartoneid'])){
        echo "<tr id = '".$row['rfapartoneid']."'>";
      //}else{
        //echo "<tr id = '".$row['rfapartoneid']."' style= 'background-color:#e74c3c;'>";
        /*if($model->getDataRFAPartThree($row['rfapartoneid'], "")!=""){
          echo "<tr id = '".$row['rfapartoneid']."' style= 'background-color:#94DIC7;'>";
        }else{
          if($model->getDataRfaPartTwo($row['rfapartoneid'], "rfapartoneid")!=""){
            echo "<tr id = '".$row['rfapartoneid']."' style= 'background-color:#94BDD1;'>";
          }else{
            echo "<tr id = '".$row['rfapartoneid']."' style= 'background-color:#9E94D1;'>";
          }
        }*/
      //}
      //echo "<tr id = '".$row['rfapartoneid']."'>";
        echo "<td class='center aligned'>".$row['rfacontrolno']."</td>";
        echo "<td class='center aligned'>".$date_format->format("M j, Y")."</td>";
        if($row['rfaRequestSource']!= ""){
          echo "<td class='center aligned'>".$row['rfaRequestSource']."</td>";
        }else{
          echo "<td class='center aligned'>".$row['rfaRequestSourceSpecified']."</td>";
        }
        echo "<td class='center aligned'>".$row['rfaNonConformity']."</td>";
        echo "<td class='center aligned'>".$model->getRootCause($row['rfapartoneid'])."</td>";
        echo "<td class='center aligned'>".$row['rfaTypeOfAction']."</td>";
        echo "<td class='center aligned'>".$row['rfaRiskLevel']."</td>";
        if($model->checkIfRFAClosed($row['rfapartoneid'])){
          echo "<td class='center aligned' style = 'background-color: #3498db;'>Closed</td>";
        }else{
          if($model->getDataRFAPartThree($row['rfapartoneid'], "")!=""){
            echo "<td class='center aligned' style = 'background-color: #e74c3c;'>Verified</td>";
          }else{
            if($model->getDataRfaPartTwo($row['rfapartoneid'], "rfapartoneid")!=""){
              echo "<td class='center aligned' style = 'background-color: #e74c3c;'>For Verification</td>";
            }else{
                echo "<td class='center aligned' style = 'background-color: #e74c3c;'>Pending Action Plan</td>";
            }
          }
        }
          echo "<td><div class='ui icon button editRfaPartOne' id = 'editPartOne-".$row['rfapartoneid']."'>Edit</div></td>";
          echo "<td><div class='ui icon button viewRFA' id = 'view-".$row['rfapartoneid']."'><i class='search icon'></i></div></td>";
        if($usertype=="Admin"){
          //echo "<td><div class='ui icon button editRFAAdmin' id = 'edit-".$row['rfapartoneid']."'><i class='edit icon'></i></div></td>";
          echo "<td><div class='ui icon button editRFAAuditee' id = 'edit-".$row['rfapartoneid']."' data-content='Action Plan'><i class='write square icon'></i></div>";
          echo "<div class='ui icon button editRFAAuditor' id = 'edit-".$row['rfapartoneid']."' data-content='Verification'><i class='share icon'></i></div>";
          echo "<div class='ui icon button editRFAAdmin' id = 'edit-".$row['rfapartoneid']."' data-content='Close Out'><i class='closed captioning icon'></i></div></td>";
        }elseif ($usertype=="Auditor") {
          echo "<td><div class='ui icon button editRFAAuditee' id = 'edit-".$row['rfapartoneid']."' data-content='Action Plan'><i class='write square icon'></i></div>
          <div class='ui icon button editRFAAuditor' id = 'edit-".$row['rfapartoneid']."' data-content='Verification'><i class='share icon'></i></div></td>";
        }elseif ($usertype=="Auditee") {
          echo "<td><div class='ui icon button editRFAAuditee' id = 'edit-".$row['rfapartoneid']."' data-content='Verification'><i class='share icon'></i></div></td>";
        }
      echo "<tr>";
    }
     echo "</tbody>";
   echo "</table>";

?>
