<?php
  session_start();
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $model->updateTimeLogged($_SESSION['user']);
  $rfaparttwoid = $model->getDataRfaPartTwo($_POST['partoneid'], 'parttwoid');
  if($_SESSION['user']!=$model->getIssuedBy($_POST['partoneid']) && $model->getUserType($_SESSION['user']) != 'Admin'){
    echo "This rfa is not assigned to you for processing!";
    return;
  }
  if($model->getRowsPartTwoInPartThree($rfaparttwoid)>=5){
    echo "This rfa have already had 5 max reviews for verification!";
    return;
  }
  if($model->checkIfSatisfactory($_POST['partoneid'])){
    echo "This have already been approved in for verification!";
    return;
  }
  if($model->checkIfDueDate($_POST['partoneid'])== true){
    echo "This rfa have already exceeded due date!";
    return;
  }
  $sql = "INSERT INTO tblrfapartthree(details, followupdate, rfaparttwoid, satisfaction, verifiedby)
          VALUES(:details, :followupdate, :rfaparttwoid, :satisfaction, :verifiedby)";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":details"=>$_POST['details'],
      ":followupdate"=>$_POST['date'],
      ":rfaparttwoid"=>$rfaparttwoid,
      ":satisfaction"=>$_POST['satisfactory'],
      ":verifiedby"=>$_POST['verifiedby']
    ))){
      $satisfactory = false;
      $html= "<tr id = 'parththree-".$model->handler->lastInsertId()."'><td>".$_POST['date']."</td><td>".$_POST['details']."</td>";
      if($_POST['satisfactory']=="1"){
        $html.="<td>*</td><td></td>";
        $satisfactory = true;
      }else{
        $html.="<td></td><td>*</td>";
        //$model->insertNotification("An RFA with Control no: ".$model->getControlNumberRFA($_POST['partoneid'])." have been issued to you for close out by ".$model->getAccNameById($_SESSION['user']), $model->getAcknowledgedBy($_POST['partoneid']), $_POST['partoneid']);
        $model->insertNotification("The RFA you filed with CONTROL NO: ".$model->getControlNumberRFA($_POST['partoneid'])." have been rejected for verification by ".$model->getAccNameById($_SESSION['user'])." please revise it.", $model-> getConformedBy($_POST['partoneid']), $_POST['partoneid']);
      }
      $html.="<td>".$_POST['verifiedby']."</td><td><div id ='divparthree-".$model->handler->lastInsertId()."' class='ui icon button'><i class='delete icon'></i></div></td></tr>";
      if($model->getRowsPartTwoInPartThree($rfaparttwoid)==5){
        $model->insertNotification("An RFA with Control no: ".$model->getControlNumberRFA($_POST['partoneid'])." have been issued to you for close out by ".$model->getAccNameById($_SESSION['user']), $model->getAcknowledgedBy($_POST['partoneid']), $_POST['partoneid']);
      }elseif ($satisfactory ==  true) {
        $model->insertNotification("An RFA with Control no: ".$model->getControlNumberRFA($_POST['partoneid'])." have been issued to you for close out by ".$model->getAccNameById($_SESSION['user']), $model->getAcknowledgedBy($_POST['partoneid']), $_POST['partoneid']);
      }
      echo $html;
    }else{
      echo "Something went wrong. Please try again later!";
    }
  } catch (PDOException $e) {
    echo "This RFA is not yet processed for Action Plan!";
  }

?>
