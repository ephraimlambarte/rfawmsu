<?php
  session_start();
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $model->updateTimeLogged($_SESSION['user']);
  $rfaparttwoid = $model->getDataRfaPartTwo($_POST['rfaid'], 'parttwoid');
  if($_SESSION['user']!=$model->getConformedBy($_POST['rfaid']) && $model->getUserType($_SESSION['user']) != 'Admin'){
    echo "This rfa is not assigned to you for processing!";
    return;
  }
  $sql = "UPDATE tblrfaparttwo SET immediateaction = :immediateaction, implementedby1 = :implementedby1,
          implementedby2 = :implementedby2, date1= :date1, date2= :date2, preventiveaction = :preventiveaction,
          rootcauses = :rootcauses WHERE parttwoid = '".$rfaparttwoid."'";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":immediateaction"=> $_POST['immediateaction'],
      ":implementedby1" => $_POST['implementedby'],
      ":implementedby2" => $_POST['implementedby2'],
      ":date1" => $_POST['date1'],
      ":date2" => $_POST['date2'],
      ":preventiveaction" => $_POST['preventiveaction'],
      ":rootcauses" => $_POST['rootcauses']
    ))){
      echo "<h3>Succesfully updated!</h3>";
    }else{
      echo "<h3>Something went wrong, please try again later!</h3>";
    }
  } catch (PDOException $e) {
    echo "<h3>Something went wrong, please try again later!</h3>";
  }

?>
