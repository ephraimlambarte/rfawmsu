<?php
  session_start();
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $model->updateTimeLogged($_SESSION['user']);
  if($model->getDataRfaPartTwo($_POST['rfaid'], 'rfapartoneid')!= ""){
    echo "<h4>This RFA is already on process and cannot be editted!</h4>";
    return;
  }
  echo "<div class='ui grid'>

      <div class='row'>
        <div class='four wide column' id='rfa_cudiv'>

          <label>Core Unit</label>
          <div class='ui selection dropdown rfaModalDropDown' id='rfa_cu'>

            <input type='hidden' value = '".$model->getRfaPartOneData('coureunitid', $_POST['rfaid'])."' placeholder ='".$model->getRFAPartOneCoreUnit($_POST['rfaid'])."'>
            <i class='dropdown icon'></i>
            <div class='default text'>Core Unit</div>

            <div class='menu' id='loadrfa_cu'>";
              $model->loadCoreUnitToDropBox();
            echo "</div>

          </div>

        </div>
        <div class='four wide column' id='rfa_pudiv'>

          <label>Process Unit</label>
          <div class='ui selection dropdown rfaModalDropDown' id='rfa_pu'>

            <input type='hidden' value = '".$model->getRfaPartOneData('processunitid', $_POST['rfaid'])."' placeholder = '".$model->getProcessUnitIDByRFAID($_POST['rfaid'])."'>
            <i class='dropdown icon'></i>
            <div class='default text'>Process Unit</div>

            <div class='menu' id='loadrfa_pu'>";
            $model->loadPuDropDown($model->getRfaPartOneData('coureunitid', $_POST['rfaid']));
            echo "</div>

          </div>

        </div>
        <div class='four wide column' id='rfa_sudiv'>

          <label>Sub Unit</label>
          <div class='ui selection dropdown rfaModalDropDown' id='rfa_su'>

            <input type='hidden' value = '".$model->getRfaPartOneData('subunitid', $_POST['rfaid'])."' placeholder = '".$model->getSubUnitIDByRFAID($_POST['rfaid'])."'>
            <i class='dropdown icon'></i>
            <div class='default text'>Sub Unit</div>

            <div class='menu' id='loadrfa_su'>";
              $model->loadSuDropdown($model->getRfaPartOneData('processunitid', $_POST['rfaid']));
            echo "</div>

          </div>

        </div>
        <div class='four wide column' id='rfa_Date'>
          <div class='four wide field'>
            <div class='row'>
            <label>Date:</label>
            </div>
            <div class='ui input'>
              <input type='date' id='rfa_dateissued' value='".date('Y-m-d')."' readonly>
            </div>
          </div>
        </div>

    </div>
    <div class='ui inverted divider'></div>

      <div class='row'>
        <div class='five wide column'>
          <label>
            Type Of Action:
          </label>
        </div>
        <div class='five wide column'>
          <div class='ui radio'>
            <input type='radio' name='Type_Of_Action' id='at_ca'
                    value = 'Corrective Action' class= 'checkBoxActionType'";
            if($model->getRfaPartOneData('rfaTypeOfAction', $_POST['rfaid'])=='Corrective Action')
                echo "checked";
            echo ">
            <label for = 'at_ca'>Corrective Action</label>
          </div>
        </div>
        <div class='five wide column'>
          <div class='ui radio'>
            <input type='radio' name='Type_Of_Action' id='at_pa'
                    value = 'Preventive Action' class= 'checkBoxActionType'";
            if($model->getRfaPartOneData('rfaTypeOfAction', $_POST['rfaid'])=='Preventive Action')
                echo "checked";
            echo ">
            <label for = 'at_pa'>Preventive Action</label>
          </div>
        </div>
      </div>
        <div class='ui inverted divider'></div>
      <div class='row'>
        <div class='three wide column'>
          <label>
            Request Source:
          </label>
        </div>
        <div class='three wide column'>
          <div class='row'>
            <div class='ui radio'>
              <input type='radio' name='requestSource' id='internalAuditRadio'
                      value = 'Internal Audit' class= 'radioActionType'";
              if($model->getRfaPartOneData('rfaRequestSource', $_POST['rfaid'])=='Internal Audit')
                  echo "checked";
              echo ">
              <label for = 'internalAuditRadio'>Internal Audit</label>
            </div>
          </div>
          <div class='row'>
            <div class='ui radio'>
              <input type='radio' name='requestSource' id='externalAuditRadio'
                      value = 'External Audit' class= 'radioActionType'";
              if($model->getRfaPartOneData('rfaRequestSource', $_POST['rfaid'])=='External Audit')
                  echo "checked";
              echo ">
              <label for = 'externalAuditRadio'>External Audit</label>
            </div>
          </div>
        </div>
        <div class='three wide column'>
          <div class='row'>
            <div class='ui radio'>
              <input type='radio' name='requestSource' id='clientFeedBackRadio'
                      value = 'Client Feedbacks' class= 'radioActionType'";
              if($model->getRfaPartOneData('rfaRequestSource', $_POST['rfaid'])=='Client Feedbacks')
                  echo "checked";
              echo ">
              <label for = 'clientFeedBackRadio'>Client Feedback</label>
            </div>
          </div>
          <div class='row'>
            <div class='ui radio'>
              <input type='radio' name='requestSource' id='managementReviewRadio'
                      value = 'Management Review' class= 'radioActionType'";
              if($model->getRfaPartOneData('rfaRequestSource', $_POST['rfaid'])=='Management Review')
                  echo "checked";
              echo ">
              <label for = 'managementReviewRadio'>Management Review</label>
            </div>
          </div>
        </div>
        <div class='three wide column'>
          <div class='row'>
            <div class='ui radio'>
              <input type='radio' name='requestSource' id='specifyRadio'
                      value = 'specify' class= 'radioActionType'";
              if($model->getRfaPartOneData('rfaRequestSource', $_POST['rfaid'])=='specify')
                  echo "checked";
              echo ">
              <label for = 'specifyRadio'>Others(specify)</label>
            </div>
          </div>
        </div>
        <div class='four wide column'>
          <label>Specify</label>
          <div class='ui input'>
            <input type = 'text' id = 'specifyText' placeholder='Specify' value = '".$model->getRfaPartOneData('rfaRequestSourceSpecified', $_POST['rfaid'])."'>
          </div>
        </div>
      </div>
      <div class='ui inverted divider'></div>
      <div class='row'>
        <div class='ui form sixteen wide column'>
          <div class='field'>
              <label>Nonconformance / Potential Nonconformance</label>
              <textarea rows='3' type='text' id='rfa_nonconformity' style = 'width:100%;'>".$model->getRfaPartOneData('rfaNonConformity', $_POST['rfaid'])."</textarea>
          </div>
        </div>
      </div>
      <div class='ui inverted divider'></div>
      <div class='row'>
        <div class='ui form sixteen wide column'>
          <div class='field'>
              <label>Description</label>
              <textarea rows='3' type='text' id='rfa_desc' style = 'width:100%;'>".$model->getRfaPartOneData('rfaDescription', $_POST['rfaid'])."</textarea>
          </div>
        </div>
      </div>
      <div class='row'>
        <div class='eight wide column'>
          <div class='row'>
            <label style='font-weight: 600'>Issued By:</label>
          </div>
          <div class='row'>
            <div class='ui input' style = 'width:100%;'>
              <input type = 'text' id = 'rfa_issuedbyText' placeholder='Signature over printed name' readonly>
            </div>
          </div>
          <div class='row'>
            <label style='margin-top: 20px; font-weight: 600' id='rfa_issuedby'>".$model->getAccNameById($model->getRfaPartOneData('rfaIssuedBy', $_POST['rfaid']))."</label>
          </div>
          <input type = 'hidden' id = 'issuedby1' value='".$model->getRfaPartOneData('rfaIssuedBy', $_POST['rfaid'])."'>
          <div class='ui inverted divider'></div>
          <div class='row'>
            <div class='sixteen wide column'>
              <div class='row'>
                <label style='font-weight: 600'>Conformed By</label>
              </div>
              <div class='row'>
                <div class = 'ui input' style = 'width:100%;'>
                  <input type  = 'text' readonly placeholder='Signature over printed name'>
                </div>
              </div>
            </div>
            <div class='sixteen wide column'>
              <div class='ui selection dropdown rfaModalDropDown' id='rfa_conformedby' style = 'width:100%;'>

                <input type='hidden' value='".$model->getRfaPartOneData('rfaConformedBy', $_POST['rfaid'])."' placeholder='".$model->getAccNameById($model->getRfaPartOneData('rfaConformedBy', $_POST['rfaid']))."'>
                <i class='dropdown icon'></i>
                <div class='default text'>Conformed By</div>

                <div class='menu' id='loadrfa_conformedby'>";
                  $model->loadConformedBy();
                echo "</div>

              </div>
            </div>
          </div>
        </div>
        <div class='eight wide column'>
          <div class='row'>
            <div class='eight wide column'>
              <div class='sixteen wide column'>
                <h5>Risk Level</h5>
              </div>
              <div class='ui inverted divider'></div>
              <div class='sixteen wide column'>
                <div class='row'>
                  <div class='ui radio'>
                    <input type='radio' name='riskLevel' id='highRiskRadio'
                            value = 'High Risk' class= 'radioActionType'";
                    if($model->getRfaPartOneData('rfaRiskLevel', $_POST['rfaid'])=='High Risk')
                        echo "checked";
                    echo ">
                    <label for = 'highRiskRadio'>High Risk</label>
                  </div>
                  <div class='ui radio'>
                    <input type='radio' name='riskLevel' id='mediumRiskRadio'
                            value = 'Medium Risk' class= 'radioActionType'";
                    if($model->getRfaPartOneData('rfaRiskLevel', $_POST['rfaid'])=='Medium Risk')
                        echo "checked";
                    echo ">
                    <label for = 'mediumRiskRadio'>Medium Risk</label>
                  </div>
                </div>
                <div class='row'>
                  <div class='ui radio'>
                    <input type='radio' name='riskLevel' id='lowRiskRadio'
                            value = 'Low Risk' class= 'radioActionType'";
                    if($model->getRfaPartOneData('rfaRiskLevel', $_POST['rfaid'])=='Low Risk')
                        echo "checked";
                    echo ">
                    <label for = 'lowRiskRadio'>Low Risk</label>
                  </div>
                </div>
              </div>
            </div>
            <div class='ui inverted divider'></div>
            <div class='eight wide column'>
              <label>Due Date:</label>
              <div class='ui input'>
                <input type='date' id='rfa_duedate' value='".date('Y-m-d', strtotime('+30 days'))."'>
              </div>
            </div>
          </div>
            <div class='ui inverted divider'></div>
          <div class='row'>
            <div class='row'>
              <label style='font-weight: 600'>Acknowledged By:</label>
            </div>
            <div class='row'>
              <div class='ui input' style = 'width:100%;'>
                <input type = 'text' id = 'acknowledgedBy' placeholder='Signature over printed name' readonly>
              </div>
            </div>
            <div class='row'>
              <label style='margin-top: 20px; font-weight: 600' id='rfa_issuedby'>".$model->getAccNameById($model->getAdminID())."</label>
            </div>
            <input type = 'hidden' id = 'acknowledgedByRfa'>
          </div>
        </div>
      </div>
    </div>
  </div>";
?>
