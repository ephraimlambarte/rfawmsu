<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "SELECT security_Question FROM tblaccount WHERE username = :username";
  $query = $model->handler->prepare($sql);
  $query->execute(array(
    ':username' =>$_POST['username']
  ));
  $row = $query->fetch();
  if($row['security_Question']==$_POST['question']){
    $sql = "SELECT question_Answer FROM tblaccount WHERE username = :username";
    $query = $model->handler->prepare($sql);
    $query->execute(array(
        ':username' =>$_POST['username']
    ));
    $row = $query->fetch();
    if($row['question_Answer']== $_POST['answer']){
        echo "Correct answer!";
    }else{
        echo 'Wrong answer!';
    }
  }else{
      echo "Questions doesnt match!";
  }
?>