<?php
  session_start();
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "DELETE FROM tblcoreunit WHERE coreunitid = :cuid";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ':cuid'=>$_POST['cuid']
    ))){
      echo "Success!";
    }else{
      echo "Something went wrong. please try again later!";
    }
  } catch (PDOException $e) {
    echo "Something went wrong. please try again later!";
  }

?>
