<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $tdID = 1;
  $sql = "SELECT coreunitid as ID, cuname,
          (SELECT COUNT(coreunitid) FROM tblprocessunit
           WHERE coreunitid = tblcoreunit.coreunitid) as puCounter
          FROM tblcoreunit
          ORDER BY cuname ASC";

  $result = $model->handler->query($sql);

  if($result->rowCount() > 0){

    while($row = $result->fetch()){

      echo "<tr id = 'coreunit-".$row['ID']."'>";
        echo "<td>".$row['cuname']."</td>";
        echo "<td>".$row['puCounter']."</td>";
        echo "<td>
                <div class='ui icon button' id = '".$row['ID']."'><i class='delete icon'></i></div>
              </td>";
      echo "</tr>";

      $tdID++;

    }

  }
?>
