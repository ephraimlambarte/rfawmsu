<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "INSERT INTO tblcoreunit (cuname) VALUES(:cuname)";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":cuname" => $_POST['cuname']
    ))){
      echo "<tr id = 'coreunit-".$model->handler->lastInsertId()."'><td>".$_POST['cuname']."</td>
      <td>".$model->getNoProcessUnitOfCoreUnit($model->handler->lastInsertId())."</td>
      <td><div class='ui icon button' id = '".$model->handler->lastInsertId()."'><i class='delete icon'></i></div></td></tr>";
    }else{
      echo "Something went wrong please try again later!";
    }
  } catch (PDOException $e) {
    echo "Something went wrong please try again later!";
  }

?>
