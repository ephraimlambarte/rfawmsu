<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "INSERT INTO designationstable (designationName) VALUES(:designationname)";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":designationname"=> $_POST['designationName']
    ))){
      echo "<tr id = 'designation-".$model->handler->lastInsertId()."'>
              <td>".$_POST['designationName']."</td>
              <td><div class='ui icon button' id = '".$model->handler->lastInsertId()."'><i class='delete icon'></i></div></td>
            </tr>";
    }else{
      echo "Something went wrong. please try again later!";
    }
  } catch (PDOException $e) {
    echo "Something went wrong. please try again later!";
  }

?>