<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "UPDATE designationstable SET designationName = :designationName WHERE
          designationID = :designationid";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":designationName" => $_POST['designationName'],
      ":designationid" => $_POST['rowid']
    ))){
      echo "<td>".$_POST['designationName']."</td>
      <td><div class='ui icon button'><i class='delete icon'></i></div></td>";
    }else{
      echo "Something went wrong. please try again later!";
    }
  } catch (PDOException $e) {
    echo "Something went wrong. please try again later!";
  }

?>
