<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $tdID = 1;
  $sql = "SELECT designationID, designationName FROM designationstable ORDER BY designationName ASC";

  $result = $model->handler->query($sql);

  if($result->rowCount() > 0){

    while($row = $result->fetch()){

      echo "<tr id = 'designation-".$row['designationID']."'>";
        echo "<td>".$row['designationName']."</td>";
        echo "<td>
                <div class='ui icon button' id = '".$row['ID']."'><i class='delete icon'></i></div>
              </td>";
      echo "</tr>";

      $tdID++;

    }

  }
?>