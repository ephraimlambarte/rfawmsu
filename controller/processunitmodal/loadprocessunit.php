<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "  SELECT processunitid AS ID, coreunitid AS fkID, puname,

              (SELECT COUNT(subunitid) FROM tblsubunit
               WHERE processunitid = ID) AS suCounter,

              (SELECT cuname FROM tblcoreunit
               WHERE coreunitid = fkID) AS cuName

              FROM tblprocessunit
              ORDER BY puname ASC";
  $query = $model->handler->query($sql);
  while($row = $query->fetch()){
    echo "<tr id = 'pumodal-".$row['ID']."'>";
      echo "<td>".$row['puname']."</td>";
      echo "<td>".$row['cuName']."</td>";
      echo "<td>".$row['suCounter']."</td>";
      echo "<td><div class='ui icon button' id = '".$row['ID']."'><i class='delete icon'></i></div></td>";
    echo "</tr>";
  }
?>
