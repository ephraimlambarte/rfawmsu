<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "INSERT INTO tblprocessunit (puname, coreunitid) VALUES(:puname, :coreunitid)";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":puname"=> $_POST['pu'],
      ":coreunitid" => $_POST['cu']
    ))){
      echo "<tr id = 'pumodal-".$model->handler->lastInsertId()."'>
              <td>".$_POST['pu']."</td>
              <td>".$model->getCoreUnitName($_POST['cu'])."</td>
              <td>0</td>
              <td><div class='ui icon button' id = '".$model->handler->lastInsertId()."'><i class='delete icon'></i></div></td>
            </tr>";
    }else{
      echo "Something went wrong. please try again later!";
    }
  } catch (PDOException $e) {
    echo "Something went wrong. please try again later!";
  }

?>
