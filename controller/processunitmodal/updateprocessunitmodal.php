<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "UPDATE tblprocessunit SET puname = :puname, coreunitid = :coreunitid WHERE
          processunitid = :processunitid";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":puname" => $_POST['pu'],
      ":coreunitid" => $_POST['cu'],
      ":processunitid" =>$_POST['puid']
    ))){
      echo "<td>".$_POST['pu']."</td>
      <td>".$model->getCoreUnitName($_POST['cu'])."</td>
      <td>".$model->getNoSubUnitProcessUnit($_POST['puid'])."</td>
      <td><div class='ui icon button' id = '".$_POST['puid']."'><i class='delete icon'></i></div></td>";
    }else{
      echo "Something went wrong. please try again later!";
    }
  } catch (PDOException $e) {
    echo "Something went wrong. please try again later!";
  }

?>
