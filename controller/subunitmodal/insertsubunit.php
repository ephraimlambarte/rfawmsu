<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $sql = "INSERT INTO tblsubunit(processunitid, suname) VALUES(:puid, :suname)";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
      ":puid" => $_POST['puname'],
      ":suname" => $_POST['suname']
    ))){
      echo "<tr id = 'subunit-".$model->handler->lastInsertId()."'>
              <td>".$_POST['suname']."</td>
              <td>".$model->getProcessUnitName($model->handler->lastInsertId())."</td>
              <td><div class='ui icon button' id = '".$model->handler->lastInsertId()."'><i class='delete icon'></i></div></td>
            </tr>";
    }else{
      echo "Something went wrong. Please try again later!";
    }
  } catch (PDOException $e) {
      echo "Something went wrong. Please try again later!";
  }


?>
