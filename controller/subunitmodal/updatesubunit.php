<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $sql = "UPDATE tblsubunit SET processunitid = :puid, suname = :suname WHERE subunitid = :subid";
  $query = $model->handler->prepare($sql);
  try {
      if($query->execute(array(
        ":puid" => $_POST['puname'],
        ":suname" => $_POST['suname'],
        ":subid" => $_POST['rowid']
      ))){
        echo "<td>".$_POST['suname']."</td>
              <td>".$model->getProcessUnitName($_POST['puname'])."</td>
              <td><div class='ui icon button' id = '".$_POST['rowid']."'><i class='delete icon'></i></div>></td>";
      }else{
        echo "Something went wrong. Please try again later!";
      }
  } catch (PDOException $e) {
    echo "Something went wrong. Please try again later!";
  }

?>
