<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $sql = "SELECT subunitid AS ID, processunitid AS fkID, suName,

              (SELECT puname FROM tblprocessunit
               WHERE processunitid = fkID) AS puName

              FROM tblsubunit
              ORDER BY suName ASC";
  $query = $model->handler->query($sql);
  while($row = $query->fetch()){
    echo "<tr id = 'subunit-".$row['ID']."'>";
      echo "<td>".$row['suName']."</td>";
      echo "<td>".$row['puName']."</td>";
      echo "<td>
              <div class='ui icon button' id = '".$row['ID']."'><i class='delete icon'></i></div>
            </td>";
    echo "</tr>";
  }
?>
