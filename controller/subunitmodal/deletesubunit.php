<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  $sql = "DELETE FROM tblsubunit WHERE subunitid = :suid";
  $query = $model->handler->prepare($sql);
  try {
    if($query->execute(array(
        ":suid" => $_POST['rowid']
    ))){
      echo "Success!";
    }else{
      echo "Something went wrong. please try again later!";
    }
  } catch (PDOException $e) {
    echo "Something went wrong. please try again later!";
  }

?>
