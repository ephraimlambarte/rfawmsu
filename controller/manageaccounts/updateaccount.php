<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  if($_POST['prevusername']!=$_POST['username']){
    if ($model->checkIfUsernameExist($_POST['username'])){
      echo "Username already exists!";
      return;
    }
  }
  $userid = $model->getUserIDByUsername($_POST['prevusername']);
  $sql = "UPDATE tblaccount SET username = :username, firstname= :firstname, lastname= :lastname,
          usertype = :usertype, password= :password, security_Question = :question, question_Answer = :answer,
          email = :email WHERE accountid = '".$userid."'";
  try {
    $query = $model->handler->prepare($sql);
    if($query->execute(array(
      ':username' => $_POST['username'],
      ':firstname' => $_POST['firstname'],
      ':lastname' => $_POST['lastname'],
      ':usertype' => $_POST['type'],
      ':password' => $_POST['password'],
      ':question' => $_POST['question'],
      ':answer' => $_POST['answer'],
      ':email' => $_POST['email']
    ))){
      $model->deleteUserInDesignation($userid);
      $model->insertUserDesignation($userid, $_POST['designation']);
        echo "<td>".$_POST['lastname'].", ".$_POST['firstname']."</td><td>".$_POST['username']."</td><td>".$_POST['type']."</td><td>Active</td><td><div class='ui icon button'><i class='delete icon'></i></div></td>*Successfully Saved!";
    }else {
      echo "Something went wrong. please try again later!";
    }
  } catch (PDOException $e) {
    echo "Something went wrong. please try again later!";
    //echo $e->getMessage();
  }

?>
