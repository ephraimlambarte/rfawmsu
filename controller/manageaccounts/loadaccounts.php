<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "SELECT accountid, username, firstname, lastname, usertype, status FROM tblaccount";
  $query = $model->handler->query($sql);
  while($row = $query->fetch()){
    echo "<tr id='".$row['accountid']."'>";
      echo "<td>".$row['lastname'].", ".$row['firstname']."</td>";
      echo "<td>".$row['username']."</td>";
      echo "<td>".$row['usertype']."</td>";
      if ($row['status'] == '1'){
          echo "<td>Active</td>";
      }else{
          echo "<td>Deactivated</td>";
      }
      echo "<td><div class='ui icon button'><i class='delete icon'></i></div></td>";
    echo "</tr>";
  }
?>
