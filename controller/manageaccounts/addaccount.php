<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  if ($model->checkIfUsernameExist($_POST['username'])){
    echo "Username already exists!";
    return;
  }
  $sql = "INSERT INTO tblaccount(username, firstname, lastname, usertype, status, password, security_Question, question_Answer, email)
          VALUES(:username, :firstname, :lastname, :usertype, :status, :password, :question, :answer, :email)";
  $query = $model->handler->prepare($sql);
  if($query->execute(array(
    ':username' => $_POST['username'],
    ':firstname' => $_POST['firstname'],
    ':lastname' => $_POST['lastname'],
    ':usertype' => $_POST['type'],
    ':status' => '1',
    ':password' => $_POST['password'],
    ':question' => $_POST['question'],
    ':answer' => $_POST['answer'],
    ':email' => $_POST['email']
  ))){
    $model->insertUserDesignation($model->handler->lastInsertId(), $_POST['designation']);
    echo "<tr id = '".$model->handler->lastInsertId()."'><td>".$_POST['lastname'].", ".$_POST['firstname']."</td><td>".$_POST['username']."</td><td>".$_POST['type']."</td><td>Active</td><td><div class='ui icon button'><i class='delete icon'></i></div></td></tr>*Successfully Added!";
  }else{
    echo "Something went wrong. try again later!";
  }

?>
