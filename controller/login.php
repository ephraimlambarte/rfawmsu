<?php
  include "../model/model.php";
  $model = new model();
  $model->connectDatabase();
  try {
    $sql = "SELECT * FROM tblaccount WHERE username = :username AND password = :password AND status = '1'";
    $query =  $model->handler->prepare($sql);
    $query->execute(array(
      ':username' => $_POST['username'],
      ':password' => $_POST['password']
    ));
    $rows = $query->rowCount();
    if($rows>0){
      $userid = $model->getUserIDByUsername($_POST['username']);
      $sql = "SELECT * FROM session_table WHERE sessionid = '".$userid."'";
      $query = $model->handler->query($sql);
      if($query->rowCount()==0){
        $sql = "INSERT INTO session_table(sessionid, datelogged, timelogged) VALUES('".$userid."',CURDATE(), NOW())";
        if($model->handler->query($sql)){
          session_start();
          $_SESSION['user'] = $userid;
          echo "login.".$model->getUserType($_SESSION['user']);
        }else{
          echo "cantlogin";
        }
      }else{
        $row = $query->fetch();
        $curdate = $model->getactualdate();
        if($curdate == $row['datelogged']){
          $curtime = $model->getMilitaryTime();
          $interval = $model->get_time_difference($row['timelogged'], $curtime);
          if($interval<30){
            echo "This User is logged in elsewhere!";
          }else{
            $sql = "DELETE FROM session_table WHERE sessionid = '".$userid."'";
            if($model->handler->query($sql)){
              $sql = "INSERT INTO session_table(sessionid, datelogged, timelogged) VALUES('".$userid."',CURDATE(), NOW())";
              if($model->handler->query($sql)){
                session_start();
                $_SESSION['user'] = $userid;
                echo "login.".$model->getUserType($_SESSION['user']);
              }else{
                echo "cantlogin";
              }
            }else{
              echo "cantlogin";
            }
          }
        }else{
          $sql = "DELETE FROM session_table WHERE sessionid = '".$userid."'";
          if($model->handler->query($sql)){
            $sql = "INSERT INTO session_table(sessionid, datelogged, timelogged) VALUES('".$userid."',CURDATE(), NOW())";
            if($model->handler->query($sql)){
              session_start();
              $_SESSION['user'] = $userid;
              echo "login.".$model->getUserType($_SESSION['user']);
            }else{
              echo "cantlogin";
            }
          }else{
            echo "cantlogin";
          }
        }
      }
    }else{
      echo "cantlogin";
    }
  } catch (PDOException $e) {
    echo $e->getMessage();
  }

?>
