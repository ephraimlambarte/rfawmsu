<?php

  session_start();

  include("./rfapartoneclass.php");

  switch ($_POST['func']) {

    case 1:

      $loadCoreUnitToDropBox = new RFAPartOneClass();
      echo $loadCoreUnitToDropBox->loadCoreUnitToDropBox();

      break;

    case 2:

      $loadProcessUnitToDropBox = new RFAPartOneClass();
      echo $loadProcessUnitToDropBox->loadProcessUnitToDropBox($_POST['id']);

      break;

    case 3:

      $loadSubUnitToDropBox = new RFAPartOneClass();
      echo $loadSubUnitToDropBox->loadSubUnitToDropBox($_POST['id']);

      break;

    case 4:

      $generateRFAControlNumber = new RFAPartOneClass();
      echo $generateRFAControlNumber->generateRFAControlNumber();

      break;

    case 5:

      $loadConformedBy = new RFAPartOneClass();
      echo $loadConformedBy->loadConformedBy();

      break;

    case 6:

      $loadIssuedBy = new RFAPartOneClass();
      echo $loadIssuedBy->loadIssuedBy('1');

      break;

    case 7:

      $saveRFA = new RFAPartOneClass();
      echo $saveRFA->saveNewRFA($_POST['ac'],$_POST['rs'],$_POST['rl'],$_POST['cu'],
           $_POST['pu'],$_POST['su'],$_POST['dc'],$_POST['dd'],$_POST['rss'],
           $_POST['nc'],$_POST['d'],$_POST['cf'],$_POST['ab'],$_POST['ib']);

      break;



  }



 ?>
