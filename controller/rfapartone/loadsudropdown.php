<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "SELECT suname, subunitid FROM tblsubunit WHERE processunitid = :puid";
  $query = $model->handler->prepare($sql);
  $query->execute(array(
    ":puid"=>$_POST['puid']
  ));
  while($row = $query->fetch()){
    echo "<div class='item' data-value=\"".$row['subunitid']."\">".$row['suname']."</div>";
  }
?>
