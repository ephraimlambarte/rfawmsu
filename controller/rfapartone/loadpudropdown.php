<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
  session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $sql = "SELECT puname, processunitid FROM tblprocessunit WHERE coreunitid = :cuid";
  $query = $model->handler->prepare($sql);
  $query->execute(array(
    ":cuid"=>$_POST['cuid']
  ));
  while($row = $query->fetch()){
    echo "<div class='item' data-value=\"".$row['processunitid']."\">".$row['puname']."</div>";
  }
?>
