<?php
  include "../../model/model.php";
  $model = new model();
  $model->connectDatabase();
   // or your date as well
   session_start();
  $model->updateTimeLogged($_SESSION['user']);
  $duedate = strtotime($_POST['rfaDateDue']);
  $currentdate = strtotime($_POST['rfaDateCreated']);
  $sql = "INSERT INTO tblrfapartone (coureunitid, processunitid,
          subunitid, rfaTypeOfAction, rfaRequestSource, rfaRequestSourceSpecified,
          rfaNonConformity, rfaDescription, rfaRiskLevel, rfaDateCreated, rfaDateDue,
          rfaIssuedBy, rfaConformedBy, rfaAcknowledgeBy,issued, acknowledged, conformed)
          VALUES (:coureunitid, :processunitid, :subunitid, :rfaTypeOfAction,
          :rfaRequestSource, :rfaRequestSourceSpecified, :rfaNonComformity, :rfaDescription,
          :rfaRiskLevel, :rfaDateCreated, :rfaDateDue, :rfaIssuedBy, :rfaConformedBy,
          :rfaAcknowledgeBy, TRUE, FALSE, FALSE)";
          ;
  $query = $model->handler->prepare($sql);
  try {
    if ($_POST['ispecified'] == "false"){
      if($query->execute(array(
                    ':coureunitid'=>$_POST['coreunit'],
                    ':processunitid'=>$_POST['processunit'],
                    ':subunitid'=>$_POST['subunit'],
                    ':rfaTypeOfAction'=>$_POST['actionType'],
                    ':rfaRequestSource'=>$_POST['requestSource'],
                    ':rfaRequestSourceSpecified'=>"",
                    ':rfaNonComformity'=>$_POST['nonComformity'],
                    ':rfaDescription'=>$_POST['description'],
                    ':rfaRiskLevel'=>$_POST['riskLevel'],
                    ':rfaDateCreated'=>$_POST['rfaDateCreated'],
                    ':rfaDateDue'=>$_POST['rfaDateDue'],
                    ':rfaIssuedBy'=>$_POST['issuedby'],
                    ':rfaConformedBy'=>$_POST['rfaConformedBy'],
                    ':rfaAcknowledgeBy'=>$_POST['rfaAcknowledgeBy']
                  )) == TRUE){
                  $myid = $model->handler->lastInsertId();
                  $model->updateControlNo($model->generateControlNumber($model->handler->lastInsertId()), $model->handler->lastInsertId());
                  $model->insertNotification("An RFA with Control no: ".$model->getControlNumberRFA($model->handler->lastInsertId())." have been issued to you for part 2 by ".$model->getAccNameById($_SESSION['user']), $_POST['rfaConformedBy'], $myid);
                  echo "Successfully issued RFA!";
                }else{
                  echo "Something went wrong. please try again later!";
                }
    }else{
      if($query->execute(array(
                    ':coureunitid'=>$_POST['coreunit'],
                    ':processunitid'=>$_POST['processunit'],
                    ':subunitid'=>$_POST['subunit'],
                    ':rfaTypeOfAction'=>$_POST['actionType'],
                    ':rfaRequestSource'=>"",
                    ':rfaRequestSourceSpecified'=>$_POST['specifiedRequestSource'],
                    ':rfaNonComformity'=>$_POST['nonComformity'],
                    ':rfaDescription'=>$_POST['description'],
                    ':rfaRiskLevel'=>$_POST['riskLevel'],
                    ':rfaDateCreated'=>$_POST['rfaDateCreated'],
                    ':rfaDateDue'=>$_POST['rfaDateDue'],
                    ':rfaIssuedBy'=>$_POST['issuedby'],
                    ':rfaConformedBy'=>$_POST['rfaConformedBy'],
                    ':rfaAcknowledgeBy'=>$_POST['rfaAcknowledgeBy']
                  )) == TRUE){
                  $myid = $model->handler->lastInsertId();
                  $model->updateControlNo($model->generateControlNumber($model->handler->lastInsertId()), $model->handler->lastInsertId());
                  $model->insertNotification("An RFA with Control no: ".$model->getControlNumberRFA($model->handler->lastInsertId())." have been issued to you for part 2 by ".$model->getAccNameById($_SESSION['user']), $_POST['rfaConformedBy'], $myid);
                  echo "Successfully issued RFA!";
                }else{
                  echo "Something went wrong. please try again later!";
                }
    }
  } catch (PDOException $e) {
    echo $model->generateControlNumber($model->handler->lastInsertId());
    echo $e->getMessage();
  }
?>
