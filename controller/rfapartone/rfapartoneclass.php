<?php
    include "../../model/model.php";
  class RFAPartOneClass{
    var $model;

    public function loadCoreUnitToDropBox(){

      $this->model =  new model();
      $this->model->connectDatabase();

      $sql = "SELECT cuname,coreunitid FROM tblcoreunit

              ORDER BY cuname ASC";

      $result = $this->model->handler->query($sql);

      if($result->rowCount() > 0){

        while($row = $result->fetch()){

          echo "<div class='item' data-value=\"".$row['coreunitid']."\">".$row['cuname']."</div>";

        }

      }

    }

    public function loadProcessUnitToDropBox($id){

      $this->model =  new model();
      $this->model->connectDatabase();

      $sql = "SELECT puname,processunitid FROM tblprocessunit

              WHERE coreunitid = '$id'
              ORDER BY puname ASC";

      $result = $this->model->handler->query($sql);

      if($result->rowCount() > 0){

        while($row = $result->fetch()){

          echo "<div class='item' data-value=\"".$row['processunitid']."\">".$row['puname']."</div>";

        }

      }

    }

    public function loadSubUnitToDropBox($id){

      $this->model =  new model();
      $this->model->connectDatabase();

      $sql = "SELECT suName,suID FROM tblsubunit
              WHERE suStatus = 1
              AND puID = '$id'
              ORDER BY suName ASC";

      $result = $this->model->handler->query($sql);

      if($result->rowCount() > 0){

        while($row = $result->fetch()){

          echo "<div class='item' data-value=\"".$row['suID']."\">".$row['suName']."</div>";

        }

      }

    }

    /*public function generateRFAControlNumber(){

      $this->model =  new model();
      $this->model->connectDatabase();

      $sql = "SELECT rfapartoneid FROM tblrfapartone
              ORDER BY rfapartoneid DESC LIMIT 1";

      $result = $this->model->handler->query($sql);

      if($result->rowCount() > 0){

        while($row = $result->fetch()){

          $hold = explode("-",$row['rfaID']);
          $val = $hold[1]+1;
          return date("Y") . "-" . $val;

        }

      }else {

        return date("Y")."-1000";

      }

    }*/

    public function loadConformedBy(){

      $this->model =  new model();
      $this->model->connectDatabase();

      $sql = "SELECT firstname,lastname,accountid FROM tblaccount
              WHERE status = '1'
              AND (usertype = 'Auditee' OR usertype='Admin')";

      $result = $this->model->handler->query($sql);

      if($result->rowCount() > 0){

        while($row = $result->fetch()){

          echo "<div class='item' data-value=\"".$row['accountid']."\">".$row['lastname'].", ".$row['firstname']."</div>";

        }

      }

    }

    public function loadIssuedBy($id){

      $this->model =  new model();
      $this->model->connectDatabase();

      $sql = "SELECT accountName FROM tblaccount
              WHERE accountID = '$id'";

      $result = $this->model->handler->query($sql);

      if($result->rowCount() > 0){

        while($row = $result->fetch()){

          return $row['accountName'];

        }

      }

    }

    public function saveNewRFA($controlno,$coreunit,$processunit,$subunit,$typeofaction,$requestsource,$rfaRequestSourceSpecified,
                              $rfaNonComformity,$description,$risklevel,$datecreated,$datedue,$issuedby,$conformedby, $acknowledgeby){

      $this->model =  new model();
      $this->model->connectDatabase();


      try {
        $sql = $this->model->handler->prepare("INSERT INTO tblrfapartone (coureunitid, processunitid,
                                                subunitid, rfaTypeOfAction, rfaRequestSource, rfaRequestSourceSpecified,
                                                rfaNonComformity, rfaDescription, rfaRiskLevel, rfaDateCreated, rfaDateDue,
                                                rfaIssuedBy, rfaConformedBy, rfaAcknowledgeBy,issued, acknowledged, conformed)
                                                VALUES (:coureunitid, :processunitid, :subunitid, :rfaTypeOfAction,
                                                  :rfaRequestSource, :rfaRequestSourceSpecified, :rfaNonComformity, :rfaDescription,
                                                  :rfaRiskLevel, :rfaDateCreated, :rfaDateDue, :rfaIssuedBy, :rfaConformedBy,
                                                  :rfaAcknowledgeBy, TRUE, FALSE, FALSE)");



        if($sql->execute(
          array(

            ':coureunitid'=>$coreunit,
            ':processunitid'=>$processunit,
            ':subunitid'=>$subunit,
            ':rfaTypeOfAction'=>$typeofaction,
            ':rfaRequestSource'=>$requestsource,
            ':rfaRequestSourceSpecified'=>$rfaRequestSourceSpecified,
            ':rfaNonComformity'=>$rfaNonComformity,
            ':rfaDescription'=>$description,
            ':rfaRiskLevel'=>$risklevel,
            ':rfaDateCreated'=>$datecreated,
            ':rfaDateDue'=>$datedue,
            ':rfaIssuedBy'=>$issuedby,
            ':rfaConformedBy'=>$conformedby,
            ':rfaAcknowledgeBy'=>$acknowledgeby
          )) == TRUE){
          $model->updateControlNo($model->generateRFAControlNumber($model->handler->lastInsertId()), $model->handler->lastInsertId());
          return 1;

        }

        else {
          return 0;
        }

      } catch (PDOException $e) {
        echo $e->getMessage();
      }


    }

  }

 ?>
